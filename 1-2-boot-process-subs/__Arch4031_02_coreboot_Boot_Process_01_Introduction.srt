1
00:00:00,000 --> 00:00:01,124
Okay so let's start

2
00:00:02,100 --> 00:00:05,600
In order to understand what the boot process is for,

3
00:00:06,050 --> 00:00:08,100
first we have to answer the question:

4
00:00:08,100 --> 00:00:11,100
"What does one wish to happen when powering up a machine?"

5
00:00:11,850 --> 00:00:14,150
So typically, when we press the power button

6
00:00:14,400 --> 00:00:17,550
we just wait, until the operating system loads

7
00:00:18,000 --> 00:00:20,400
and we get our things done on the machine.

8
00:00:21,050 --> 00:00:22,500
But, the operating system

9
00:00:22,750 --> 00:00:24,350
cannot be launched just like that.

10
00:00:25,250 --> 00:00:27,850
We have to, first prepare the hardware and,

11
00:00:27,850 --> 00:00:30,700
pass certain information about our platform

12
00:00:30,700 --> 00:00:32,450
to the operate-- operating system.

13
00:00:33,850 --> 00:00:35,500
If you remember Boromir from

14
00:00:35,500 --> 00:00:37,400
"The Lord of the Rings", he said:

15
00:00:37,700 --> 00:00:39,900
"One does not simply walk into Mordor"

16
00:00:40,700 --> 00:00:44,150
The same goes for the firmware and operating system.

17
00:00:45,950 --> 00:00:49,350
This is why we need a piece of persistent software

18
00:00:49,350 --> 00:00:50,700
which is executed after

19
00:00:50,700 --> 00:00:53,100
pressing the power button and prepares the hardware

20
00:00:53,300 --> 00:00:54,700
to launch the operating system.

21
00:00:55,500 --> 00:00:58,050
This piece of software is called: the firmware

22
00:01:00,500 --> 00:01:01,650
So in this chapter,

23
00:01:01,650 --> 00:01:03,850
you will be introduced to the tasks performed

24
00:01:03,850 --> 00:01:05,100
by the firmware

25
00:01:05,100 --> 00:01:06,700
on the example of coreboot.

26
00:01:07,350 --> 00:01:08,350
I will explain

27
00:01:08,350 --> 00:01:11,400
what coreboot boot process looks like in detail

28
00:01:12,000 --> 00:01:13,500
You will also get to know

29
00:01:14,100 --> 00:01:16,950
the coreboot components, also called stages,

30
00:01:17,750 --> 00:01:20,150
what are their responsibilities and tasks.

31
00:01:20,450 --> 00:01:22,550
Each will be described in detail.
