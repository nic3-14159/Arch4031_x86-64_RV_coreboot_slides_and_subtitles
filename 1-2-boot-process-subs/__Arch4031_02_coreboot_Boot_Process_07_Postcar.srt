1
00:00:00,960 --> 00:00:06,399
the next stage after the rom stage

2
00:00:03,439 --> 00:00:07,680
is called post core this stage has been

3
00:00:06,399 --> 00:00:09,679
designed to create a

4
00:00:07,680 --> 00:00:11,280
clear program boundary between the roam

5
00:00:09,679 --> 00:00:13,599
station drum stage

6
00:00:11,280 --> 00:00:15,120
before postcard has been introduced

7
00:00:13,599 --> 00:00:18,000
there were so-called

8
00:00:15,120 --> 00:00:18,800
car globals which were variables that

9
00:00:18,000 --> 00:00:21,119
had to be

10
00:00:18,800 --> 00:00:22,480
stored in a special section of ram stage

11
00:00:21,119 --> 00:00:26,000
and copied over

12
00:00:22,480 --> 00:00:28,800
when the car has been teared down

13
00:00:26,000 --> 00:00:30,800
it was quite a burden for developers to

14
00:00:28,800 --> 00:00:33,120
maintain that solution

15
00:00:30,800 --> 00:00:36,000
so car tier down has been moved to a

16
00:00:33,120 --> 00:00:37,960
separate stage

17
00:00:36,000 --> 00:00:39,120
this stage has a very few

18
00:00:37,960 --> 00:00:42,000
responsibilities

19
00:00:39,120 --> 00:00:44,559
and has takes a very short amount of

20
00:00:42,000 --> 00:00:47,120
time during the boot process

21
00:00:44,559 --> 00:00:49,200
it is still executed in temporary memory

22
00:00:47,120 --> 00:00:51,760
environment the cache's ram

23
00:00:49,200 --> 00:00:53,520
and mainly uses c code with certain

24
00:00:51,760 --> 00:00:57,360
elements of assembly during the

25
00:00:53,520 --> 00:00:59,680
teardown the main tasks of postcard

26
00:00:57,360 --> 00:01:00,480
are to tip the cache's ram environment

27
00:00:59,680 --> 00:01:03,840
down

28
00:01:00,480 --> 00:01:11,040
switch to the ram the main memory

29
00:01:03,840 --> 00:01:11,040
and to load and execute the ram stage

