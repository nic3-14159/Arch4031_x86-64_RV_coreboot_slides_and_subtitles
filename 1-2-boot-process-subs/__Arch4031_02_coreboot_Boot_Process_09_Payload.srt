1
00:00:00,960 --> 00:00:04,080
right before the ram stage ends its last

2
00:00:03,679 --> 00:00:07,680
job

3
00:00:04,080 --> 00:00:10,000
is to load and execute the payload

4
00:00:07,680 --> 00:00:10,960
the payload is an external to core boots

5
00:00:10,000 --> 00:00:13,120
software

6
00:00:10,960 --> 00:00:14,799
which is possibilities to load the

7
00:00:13,120 --> 00:00:18,080
target software

8
00:00:14,799 --> 00:00:19,039
the machine typically some operating

9
00:00:18,080 --> 00:00:23,039
system

10
00:00:19,039 --> 00:00:25,039
or it may be an operating system itself

11
00:00:23,039 --> 00:00:26,800
corbot offers a wide range of payloads

12
00:00:25,039 --> 00:00:29,359
to be launched after ram stage

13
00:00:26,800 --> 00:00:31,279
i will briefly explain what each of them

14
00:00:29,359 --> 00:00:34,880
can do

15
00:00:31,279 --> 00:00:36,640
see bios it provides the old fashioned

16
00:00:34,880 --> 00:00:39,440
the kc bios services

17
00:00:36,640 --> 00:00:42,879
which were used back in 90s to launch

18
00:00:39,440 --> 00:00:46,000
operating system like microsoft does

19
00:00:42,879 --> 00:00:48,640
diana provides ufi compatible interface

20
00:00:46,000 --> 00:00:49,760
to launch an operating system it is

21
00:00:48,640 --> 00:00:52,480
based on

22
00:00:49,760 --> 00:00:55,199
fe development kit 2 repository which is

23
00:00:52,480 --> 00:00:58,640
a reference implementation of ufi

24
00:00:55,199 --> 00:01:00,559
specification 2

25
00:00:58,640 --> 00:01:02,480
one of the most popular boot loaders in

26
00:01:00,559 --> 00:01:05,360
many linux distributions

27
00:01:02,480 --> 00:01:07,760
they can load linux windows as well as

28
00:01:05,360 --> 00:01:11,119
other executables

29
00:01:07,760 --> 00:01:12,000
linux linux kernel with initial round

30
00:01:11,119 --> 00:01:14,240
disk can also

31
00:01:12,000 --> 00:01:17,360
be integrated as a corbot payload and

32
00:01:14,240 --> 00:01:20,400
directly launched after round stage

33
00:01:17,360 --> 00:01:24,000
linux boot linux boot is a

34
00:01:20,400 --> 00:01:26,560
linux kernel with eurozoland

35
00:01:24,000 --> 00:01:28,720
eurot is an initial round disc written

36
00:01:26,560 --> 00:01:31,439
in gold language

37
00:01:28,720 --> 00:01:34,000
depth charge dev charge is a google

38
00:01:31,439 --> 00:01:37,360
chromebook specific payload designed to

39
00:01:34,000 --> 00:01:40,400
launch chromevoice in a secure manner

40
00:01:37,360 --> 00:01:42,560
ubud it is a well-known bootloader in an

41
00:01:40,400 --> 00:01:45,119
embedded ecosystem

42
00:01:42,560 --> 00:01:45,920
it is more adequate for arm rather than

43
00:01:45,119 --> 00:01:49,840
x86

44
00:01:45,920 --> 00:01:50,399
architecture phylo a bootloader which

45
00:01:49,840 --> 00:01:52,720
loads

46
00:01:50,399 --> 00:01:55,920
boot images from a local file system

47
00:01:52,720 --> 00:02:00,000
without help from legacy buyer services

48
00:01:55,920 --> 00:02:01,920
written with libperiod custom payloads

49
00:02:00,000 --> 00:02:04,240
corporate offers also launching custom

50
00:02:01,920 --> 00:02:04,799
payloads and it offers a library for

51
00:02:04,240 --> 00:02:09,440
writing

52
00:02:04,799 --> 00:02:11,840
on payloads called leap payload

53
00:02:09,440 --> 00:02:12,640
so the secondary payrolls are payloads

54
00:02:11,840 --> 00:02:15,280
that can be run

55
00:02:12,640 --> 00:02:16,319
by the main payload the idea of

56
00:02:15,280 --> 00:02:18,480
secondary payloads

57
00:02:16,319 --> 00:02:20,080
became available with the integration of

58
00:02:18,480 --> 00:02:23,120
cbfs executable

59
00:02:20,080 --> 00:02:25,440
images into c bios

60
00:02:23,120 --> 00:02:26,160
c bios can load another payload from

61
00:02:25,440 --> 00:02:29,280
cbfs

62
00:02:26,160 --> 00:02:30,000
and execute it some secondary payloads

63
00:02:29,280 --> 00:02:33,760
available

64
00:02:30,000 --> 00:02:37,840
in coreboot are memtest

65
00:02:33,760 --> 00:02:40,000
86 plus ram testing application

66
00:02:37,840 --> 00:02:41,680
also available in for example ubuntu

67
00:02:40,000 --> 00:02:44,720
distribution

68
00:02:41,680 --> 00:02:46,560
apixey which is a open source network

69
00:02:44,720 --> 00:02:49,440
boot environment

70
00:02:46,560 --> 00:02:50,400
operational version of preboot execution

71
00:02:49,440 --> 00:02:54,640
environment

72
00:02:50,400 --> 00:02:57,360
pxe core info which is a simple

73
00:02:54,640 --> 00:02:58,239
gui application to parse core boot

74
00:02:57,360 --> 00:03:00,080
tables

75
00:02:58,239 --> 00:03:03,599
and display information about hardware

76
00:03:00,080 --> 00:03:06,560
and corporate console and timestamps

77
00:03:03,599 --> 00:03:07,760
and if you run cui a simple gui

78
00:03:06,560 --> 00:03:10,640
application for

79
00:03:07,760 --> 00:03:12,879
thomas content manipulation it

80
00:03:10,640 --> 00:03:16,000
integrates very well with core boot and

81
00:03:12,879 --> 00:03:17,680
the option table which is implemented

82
00:03:16,000 --> 00:03:19,200
the option terrible stores runtime

83
00:03:17,680 --> 00:03:29,680
configuration options in the

84
00:03:19,200 --> 00:03:29,680
cmos non-volatile memory

