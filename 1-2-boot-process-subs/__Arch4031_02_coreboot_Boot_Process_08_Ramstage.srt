1
00:00:01,360 --> 00:00:05,920
so one of the last stages of chord boot

2
00:00:03,840 --> 00:00:07,919
is called ram stage

3
00:00:05,920 --> 00:00:12,080
it is the first stage that executes

4
00:00:07,919 --> 00:00:12,080
entirely from the main memory

5
00:00:12,480 --> 00:00:16,960
it is responsible mainly for pci devices

6
00:00:15,440 --> 00:00:21,039
installation

7
00:00:16,960 --> 00:00:23,119
on-chip devices trusted platform modules

8
00:00:21,039 --> 00:00:26,400
if not yet done by first stage

9
00:00:23,119 --> 00:00:27,199
the graphics cpu and other components

10
00:00:26,400 --> 00:00:30,320
like

11
00:00:27,199 --> 00:00:33,119
super i o and embedded control but

12
00:00:30,320 --> 00:00:33,119
that's not all

13
00:00:35,680 --> 00:00:40,800
so our ram stage does not perform the

14
00:00:38,079 --> 00:00:42,960
installation in a random order

15
00:00:40,800 --> 00:00:44,160
this process is controlled in an

16
00:00:42,960 --> 00:00:47,280
appropriate manner

17
00:00:44,160 --> 00:00:47,840
via the boot state machine the boosting

18
00:00:47,280 --> 00:00:50,079
machine

19
00:00:47,840 --> 00:00:52,480
consists of various machine state

20
00:00:50,079 --> 00:00:54,719
machine substages

21
00:00:52,480 --> 00:00:56,800
each of the substages can have

22
00:00:54,719 --> 00:00:59,359
particular action to be performed

23
00:00:56,800 --> 00:01:00,559
on the entry to the substance and on the

24
00:00:59,359 --> 00:01:02,239
exit

25
00:01:00,559 --> 00:01:03,600
it is tightly coupled with the core

26
00:01:02,239 --> 00:01:05,760
boots device tree

27
00:01:03,600 --> 00:01:08,240
please do not mistake it with the device

28
00:01:05,760 --> 00:01:10,400
tree used for kernel booting

29
00:01:08,240 --> 00:01:11,840
this is a tree structure defined for

30
00:01:10,400 --> 00:01:12,960
each platform that describes the

31
00:01:11,840 --> 00:01:14,479
hardware devices

32
00:01:12,960 --> 00:01:16,799
present on the board and their

33
00:01:14,479 --> 00:01:20,240
configuration address

34
00:01:16,799 --> 00:01:22,479
location etc first

35
00:01:20,240 --> 00:01:24,080
there are actions performed before pci

36
00:01:22,479 --> 00:01:27,360
devices initialization

37
00:01:24,080 --> 00:01:30,479
this is pre-device substage for example

38
00:01:27,360 --> 00:01:32,240
pci devices may not be at ready and they

39
00:01:30,479 --> 00:01:33,840
need some special treatment before

40
00:01:32,240 --> 00:01:37,119
standard animation procedure

41
00:01:33,840 --> 00:01:40,320
it kicks in

42
00:01:37,119 --> 00:01:41,520
at this point for example intel fsp or

43
00:01:40,320 --> 00:01:44,240
amd agiza

44
00:01:41,520 --> 00:01:46,000
is called to prepare the hardware

45
00:01:44,240 --> 00:01:48,240
definite chip substage

46
00:01:46,000 --> 00:01:49,280
initializes various chips in corbett's

47
00:01:48,240 --> 00:01:53,040
device tree

48
00:01:49,280 --> 00:01:56,079
for example cpus the enumerate

49
00:01:53,040 --> 00:01:59,520
substage is visible is responsible for

50
00:01:56,079 --> 00:02:02,640
standard pci enumeration

51
00:01:59,520 --> 00:02:04,159
the resources substage assigns resources

52
00:02:02,640 --> 00:02:06,479
to the devices

53
00:02:04,159 --> 00:02:10,160
pca devices may require certain memory

54
00:02:06,479 --> 00:02:13,360
to map the registers and arrow ports

55
00:02:10,160 --> 00:02:15,680
def enabled substage is responsible for

56
00:02:13,360 --> 00:02:20,000
disabling or enabling the devices

57
00:02:15,680 --> 00:02:23,200
as defined in the core boots device tree

58
00:02:20,000 --> 00:02:25,599
dev init substate is responsible for

59
00:02:23,200 --> 00:02:27,760
performing device-specific installation

60
00:02:25,599 --> 00:02:29,920
that is not always common for all

61
00:02:27,760 --> 00:02:32,959
devices

62
00:02:29,920 --> 00:02:34,840
the pos device substage happens after

63
00:02:32,959 --> 00:02:36,879
the devices are enumerated and

64
00:02:34,840 --> 00:02:39,440
initialized initialized

65
00:02:36,879 --> 00:02:40,959
it gives some room for customization and

66
00:02:39,440 --> 00:02:45,120
adjustments for main

67
00:02:40,959 --> 00:02:47,680
specific settings for devices

68
00:02:45,120 --> 00:02:48,879
the os resume check and os resume

69
00:02:47,680 --> 00:02:50,560
substages

70
00:02:48,879 --> 00:02:52,160
check whether the machine has been put

71
00:02:50,560 --> 00:02:54,879
to the slip with

72
00:02:52,160 --> 00:02:58,480
suspend to run and eventually resumes

73
00:02:54,879 --> 00:03:01,040
directly to the operating system

74
00:02:58,480 --> 00:03:02,800
the right tables substage is responsible

75
00:03:01,040 --> 00:03:04,000
for preparing various structures and

76
00:03:02,800 --> 00:03:07,040
teams in the ram for

77
00:03:04,000 --> 00:03:07,680
operating system consumption it is done

78
00:03:07,040 --> 00:03:10,000
after

79
00:03:07,680 --> 00:03:10,879
the os resume check because it is not

80
00:03:10,000 --> 00:03:13,840
necessary to

81
00:03:10,879 --> 00:03:14,319
do it once again these tables are for

82
00:03:13,840 --> 00:03:18,560
example

83
00:03:14,319 --> 00:03:19,680
scpi tables sm bios tables some memory

84
00:03:18,560 --> 00:03:24,400
tables

85
00:03:19,680 --> 00:03:26,879
etc the payload load and payload boot

86
00:03:24,400 --> 00:03:27,680
are sub stages which prepare the payload

87
00:03:26,879 --> 00:03:30,400
defining

88
00:03:27,680 --> 00:03:30,959
so the final final application not part

89
00:03:30,400 --> 00:03:34,000
of

90
00:03:30,959 --> 00:03:36,159
coreboot to be launched and execute

91
00:03:34,000 --> 00:03:40,159
at this point round stage ends and

92
00:03:36,159 --> 00:03:40,159
control is handed over to the payload

93
00:03:41,040 --> 00:03:44,080
as i have mentioned at the beginning in

94
00:03:42,799 --> 00:03:45,840
the introduction

95
00:03:44,080 --> 00:03:48,319
the operating system requires certain

96
00:03:45,840 --> 00:03:48,799
information about the hardware it runs

97
00:03:48,319 --> 00:03:52,239
on

98
00:03:48,799 --> 00:03:55,200
etc this information are passed

99
00:03:52,239 --> 00:03:56,319
via standardized structures and tables

100
00:03:55,200 --> 00:03:59,599
to these tables

101
00:03:56,319 --> 00:04:00,560
we can include ncpi tables asm bios

102
00:03:59,599 --> 00:04:02,879
tables

103
00:04:00,560 --> 00:04:05,519
various core boot tables or device tree

104
00:04:02,879 --> 00:04:05,519
updates

105
00:04:06,319 --> 00:04:11,040
the acpi stands for advanced

106
00:04:08,640 --> 00:04:13,360
configuration and power interface

107
00:04:11,040 --> 00:04:14,159
it is an industrious standard which was

108
00:04:13,360 --> 00:04:16,479
developed

109
00:04:14,159 --> 00:04:18,639
to enable os directed configuration

110
00:04:16,479 --> 00:04:21,440
power and thermal management

111
00:04:18,639 --> 00:04:22,960
and reliability availability and

112
00:04:21,440 --> 00:04:26,880
survivability features

113
00:04:22,960 --> 00:04:29,280
of mobile desktop and server platforms

114
00:04:26,880 --> 00:04:30,880
the cpi specification describes the

115
00:04:29,280 --> 00:04:33,360
structures and mechanisms

116
00:04:30,880 --> 00:04:35,199
necessary to design operating system

117
00:04:33,360 --> 00:04:36,880
directed power management

118
00:04:35,199 --> 00:04:39,919
and make advanced configuration

119
00:04:36,880 --> 00:04:42,560
architectures possible

120
00:04:39,919 --> 00:04:43,520
a cpi applies to all classes of

121
00:04:42,560 --> 00:04:45,600
computers

122
00:04:43,520 --> 00:04:46,720
and is the key element in operating

123
00:04:45,600 --> 00:04:48,800
system directed

124
00:04:46,720 --> 00:04:51,280
configuration and power management or

125
00:04:48,800 --> 00:04:51,280
spm

126
00:04:51,520 --> 00:04:55,120
acpi tables may contain informational

127
00:04:54,400 --> 00:04:57,759
content

128
00:04:55,120 --> 00:04:58,320
or executable code written in a cpa

129
00:04:57,759 --> 00:05:00,720
specific

130
00:04:58,320 --> 00:05:00,720
language

131
00:05:01,440 --> 00:05:05,280
sm bios stands for system management

132
00:05:04,160 --> 00:05:07,360
bios

133
00:05:05,280 --> 00:05:10,160
it is their primary standard for

134
00:05:07,360 --> 00:05:14,039
delivering management information data

135
00:05:10,160 --> 00:05:15,520
via system firmware since its release in

136
00:05:14,039 --> 00:05:17,919
1995

137
00:05:15,520 --> 00:05:19,600
the widely implemented sm bio standard

138
00:05:17,919 --> 00:05:20,000
has simplified the management of more

139
00:05:19,600 --> 00:05:22,960
than

140
00:05:20,000 --> 00:05:24,000
2 billion client and server systems it

141
00:05:22,960 --> 00:05:27,039
exposes information

142
00:05:24,000 --> 00:05:31,280
about the hardware the details of cpus

143
00:05:27,039 --> 00:05:33,680
use memory external devices etc

144
00:05:31,280 --> 00:05:35,039
core boot tables are occurred with

145
00:05:33,680 --> 00:05:37,440
specific structures

146
00:05:35,039 --> 00:05:38,800
placed in reserved memory they hold

147
00:05:37,440 --> 00:05:41,120
various information like

148
00:05:38,800 --> 00:05:42,160
main board vendor model corporate

149
00:05:41,120 --> 00:05:45,440
version

150
00:05:42,160 --> 00:05:49,440
build data the console boot verb forum

151
00:05:45,440 --> 00:05:51,759
boot performance timestamps etc

152
00:05:49,440 --> 00:05:53,120
they can be viewed and parsed with

153
00:05:51,759 --> 00:05:56,560
another corbot tool

154
00:05:53,120 --> 00:05:59,919
called cbmem it is also available

155
00:05:56,560 --> 00:06:02,720
as a part of the corporate repository

156
00:05:59,919 --> 00:06:04,960
so device trees are rather unspecific

157
00:06:02,720 --> 00:06:08,080
and are required to put linux kernel

158
00:06:04,960 --> 00:06:11,039
the device trees are constructed by the

159
00:06:08,080 --> 00:06:11,039
round stage as well

160
00:06:12,479 --> 00:06:16,160
before the ram stage hands over the

161
00:06:14,479 --> 00:06:18,720
control to payload

162
00:06:16,160 --> 00:06:20,639
it also needs to do some harder and

163
00:06:18,720 --> 00:06:24,000
thermal lockdown

164
00:06:20,639 --> 00:06:25,840
for example it might be some right

165
00:06:24,000 --> 00:06:28,880
protection of the boot media

166
00:06:25,840 --> 00:06:31,280
like some security related registers

167
00:06:28,880 --> 00:06:32,479
for example like lock system management

168
00:06:31,280 --> 00:06:35,919
mode which is

169
00:06:32,479 --> 00:06:38,880
x86 specific not doing

170
00:06:35,919 --> 00:06:41,520
it may have a huge security impact on

171
00:06:38,880 --> 00:06:44,240
the software that runs on the machine

172
00:06:41,520 --> 00:06:46,080
for example system management mode

173
00:06:44,240 --> 00:06:47,039
contains firmware code that handles

174
00:06:46,080 --> 00:06:49,919
various action

175
00:06:47,039 --> 00:06:50,479
created to slip and power transitions

176
00:06:49,919 --> 00:06:55,919
dust

177
00:06:50,479 --> 00:06:55,919
must be protected from operating system

