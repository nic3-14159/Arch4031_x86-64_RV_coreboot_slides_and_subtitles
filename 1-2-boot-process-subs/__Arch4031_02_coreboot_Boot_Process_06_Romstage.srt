1
00:00:00,719 --> 00:00:05,120
let's move now to the next stage which

2
00:00:03,040 --> 00:00:07,600
is called rom stage

3
00:00:05,120 --> 00:00:08,480
it is the next stage exited either by

4
00:00:07,600 --> 00:00:11,440
boot block

5
00:00:08,480 --> 00:00:14,400
or by first stage depending on the

6
00:00:11,440 --> 00:00:17,680
viewport enablement state

7
00:00:14,400 --> 00:00:20,720
since there is no main memory yet

8
00:00:17,680 --> 00:00:22,960
it still executes from the cache's ram

9
00:00:20,720 --> 00:00:25,039
it is mainly written in c code except

10
00:00:22,960 --> 00:00:27,840
the entry point

11
00:00:25,039 --> 00:00:29,920
the main purpose of the rom stage is to

12
00:00:27,840 --> 00:00:31,679
initialize the hardware necessary for

13
00:00:29,920 --> 00:00:34,079
ram training

14
00:00:31,679 --> 00:00:36,320
change their main memory and then

15
00:00:34,079 --> 00:00:39,760
initialize the cpu man and prepare a

16
00:00:36,320 --> 00:00:39,760
handoff to postcard

17
00:00:40,399 --> 00:00:46,480
the cpu mam is a core boot memory

18
00:00:43,680 --> 00:00:49,200
is a reserved car boot memory to hold

19
00:00:46,480 --> 00:00:52,160
various cordless specific information

20
00:00:49,200 --> 00:00:54,000
and the hand of the postcard is a

21
00:00:52,160 --> 00:00:55,120
structure that is passed to the next

22
00:00:54,000 --> 00:00:57,199
stage

23
00:00:55,120 --> 00:01:01,120
containing some various information

24
00:00:57,199 --> 00:01:01,120
necessary for postcard to operate

25
00:01:01,199 --> 00:01:06,080
few years back the memory training was

26
00:01:03,120 --> 00:01:08,880
so open and not so complicated

27
00:01:06,080 --> 00:01:10,320
at that time platforms used coreboot's

28
00:01:08,880 --> 00:01:11,680
native implementation of memory

29
00:01:10,320 --> 00:01:14,479
insistation

30
00:01:11,680 --> 00:01:15,840
however over time the silicon vendors

31
00:01:14,479 --> 00:01:16,720
began to close their silicon

32
00:01:15,840 --> 00:01:19,200
initialization

33
00:01:16,720 --> 00:01:22,560
inside so-called binary blobs in the

34
00:01:19,200 --> 00:01:24,400
fear of leaking intellectual property

35
00:01:22,560 --> 00:01:26,479
these blobs are for example

36
00:01:24,400 --> 00:01:28,880
interferometer support package

37
00:01:26,479 --> 00:01:29,759
and amd generic encapsulated software

38
00:01:28,880 --> 00:01:33,600
architecture

39
00:01:29,759 --> 00:01:34,479
called ajisa at the beginning amd has

40
00:01:33,600 --> 00:01:37,680
been releasing the

41
00:01:34,479 --> 00:01:39,280
egisa source code to coreboot but later

42
00:01:37,680 --> 00:01:40,079
it began to follow us into internal

43
00:01:39,280 --> 00:01:43,280
strength

44
00:01:40,079 --> 00:01:45,680
and started to deliver it in panic fall

45
00:01:43,280 --> 00:01:47,680
still there are some micro architectures

46
00:01:45,680 --> 00:01:50,880
that have fully open code

47
00:01:47,680 --> 00:01:54,240
like intel iv bridge third generation

48
00:01:50,880 --> 00:01:59,119
and older amd family 16h

49
00:01:54,240 --> 00:02:01,920
kabini 15h trinity or 14h

50
00:01:59,119 --> 00:02:02,719
nowadays x86 architecture is full of

51
00:02:01,920 --> 00:02:08,320
secrets

52
00:02:02,719 --> 00:02:08,320
and likely will never be open again

