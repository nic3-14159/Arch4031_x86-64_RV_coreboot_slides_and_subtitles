1
00:00:00,74 --> 00:00:03,35
so I have mentioned the carbon stages,

2
00:00:03,74 --> 00:00:06,56
but I haven't yet explained what these are and how

3
00:00:06,56 --> 00:00:07,85
they are named

4
00:00:08,54 --> 00:00:09,68
So basically,

5
00:00:09,73 --> 00:00:15,16
car boot consists of 5 to 6 main stages,

6
00:00:15,64 --> 00:00:17,39
and the names are as false

7
00:00:17,4 --> 00:00:18,7
The boot block

8
00:00:18,71 --> 00:00:23,06
It is the earliest stage that is executed right after

9
00:00:23,07 --> 00:00:26,76
waking up the machine from sleep

10
00:00:27,74 --> 00:00:29,8
Then there is an optional fire

11
00:00:29,8 --> 00:00:34,46
Stage four Stage is responsible for early film or verification

12
00:00:35,25 --> 00:00:38,65
and it's tied to the verified but implementation

13
00:00:39,54 --> 00:00:41,67
Then we have rum stage

14
00:00:41,68 --> 00:00:46,05
Which main task is to prepare the main memory

15
00:00:47,64 --> 00:00:53,14
Then we have a postcard which tears down the temporary

16
00:00:53,14 --> 00:00:58,02
memory environment and prepares the next stage that we run

17
00:00:58,02 --> 00:01:01,16
from the main memory

18
00:01:01,84 --> 00:01:05,43
And then we have the last last Corbyn stage,

19
00:01:05,43 --> 00:01:06,85
which is called the REM Stage

20
00:01:07,24 --> 00:01:14,2
And it is responsible for overall harboring sensation writing very

21
00:01:14,2 --> 00:01:17,7
stables that are passed the operating system and hand over

22
00:01:17,7 --> 00:01:19,42
to the payout pay

23
00:01:19,42 --> 00:01:21,73
A lot is not really a part of core boot

24
00:01:22,44 --> 00:01:27,76
but this is the last stage that Corbett hands over

25
00:01:27,76 --> 00:01:31,62
the control to the pilot is anything we wanted to

26
00:01:31,63 --> 00:01:31,86
be

27
00:01:31,87 --> 00:01:35,19
It can be a bootloader on an application

28
00:01:35,2 --> 00:01:37,58
It may load target operating system,

29
00:01:37,59 --> 00:01:38,26
etcetera

30
00:01:39,44 --> 00:01:43,12
So each of these stages has its own purpose and

31
00:01:43,13 --> 00:01:46,25
one should not perform tasks of one station another

32
00:01:47,13 --> 00:01:50,52
It is enforced by Korbut booting model and to keep

33
00:01:50,52 --> 00:01:53,66
behavioral consistency across various platforms

34
00:01:54,44 --> 00:01:58,25
I have mentioned that car but consists of multiple stages

35
00:01:58,94 --> 00:02:01,28
But how does it really work?

36
00:02:01,29 --> 00:02:02,06
Typically,

37
00:02:02,54 --> 00:02:04,09
firmware is a single binary,

38
00:02:04,09 --> 00:02:04,46
right

39
00:02:05,34 --> 00:02:08,72
So each of the status are indeed and separate executable

40
00:02:08,72 --> 00:02:09,29
file,

41
00:02:09,3 --> 00:02:14,63
but they are packed into a CPF s CBF SSR

42
00:02:14,64 --> 00:02:18,3
color both file system that allows to pack the firmware

43
00:02:18,3 --> 00:02:21,23
components to create a sort of sort of argument

44
00:02:22,34 --> 00:02:22,75
Of course,

45
00:02:22,75 --> 00:02:25,94
there is a tool that allows manipulating the CBF contents

46
00:02:25,95 --> 00:02:27,26
called CBF festival,

47
00:02:27,84 --> 00:02:29,96
it is a part of the corporate industry

48
00:02:31,24 --> 00:02:34,1
It is able to put and remove files from severe

49
00:02:34,1 --> 00:02:34,66
fest,

50
00:02:35,24 --> 00:02:39,09
apply compression to defiance and force their alignment or base

51
00:02:39,09 --> 00:02:41,26
address in the main memory

52
00:02:41,64 --> 00:02:45,56
It is also capable of manipulating the Flashman partition contents

53
00:02:45,94 --> 00:02:46,65
What is flash?

54
00:02:46,65 --> 00:02:46,84
Um,

55
00:02:46,85 --> 00:02:48,25
I will describe soon

56
00:02:49,54 --> 00:02:51,26
Besides these features,

57
00:02:51,35 --> 00:02:54,36
it can also integrate virus five types like bus pass

58
00:02:54,36 --> 00:02:55,16
images,

59
00:02:55,47 --> 00:02:56,57
profiles,

60
00:02:56,66 --> 00:02:58,49
64 bit integers,

61
00:02:58,64 --> 00:02:59,35
etcetera

62
00:03:00,24 --> 00:03:00,45
Okay,

