# About QEMU

* Open source emulator for various platforms.
    * *Nix philosophy.
    * Full software implementation of environment
    * Virtualization support using VT-x & KVM (Kernel-based
    Virtual Machine) on Linux
    * Virtualization support using VT-x & HAXM (Intel®
    Hardware Accelerated Execution Manager) on Windows
* Many devices and architectures  available, VT-d support
  - support for RISC-V
  - support for OpenPOWER
* Very versatile tool with many options. This presentation
covers only basic features useful for firmware development.

???

QEMU follows *NIX philosophy, thus it's designed to be easily
integrated with other tools rather than creating monolithic
application. All VM control can be done using Monitor mode
(which lets using it in slave mode), all configuration can
be done using command line.

Because of this philosophy, there are many switches and stuff,
so probably you want to write a shell script that type them
for you. You could try one of GUI frontends as well. Minimal
solution is to set alias that enables at least KVM, 512M RAM
and coreboot.

Unlike many popular solutions (VirtualPC, VirtualBox) support
variety of architectures. It can take advantage of hardware
assisted virtualization (VT-x, VT-d).



---

# Practice #4: environment verification

* Pull recent version of 3mdeb training container
```shell
docker pull \
registry.gitlab.com/opensecuritytraining/arch4031_x86-64_rv_coreboot_code_for_class:4.13
```

* Close coreboot repository, if you don't have it already
```shell
git clone --recurse-submodules --branch 4.13 https://review.coreboot.org/coreboot.git
```

* Run container:
```shell
alias cb-qemu='qemu-system-x86_64 -M q35 -bios build/coreboot.rom \
  -serial stdio -display none'
cb-qemu
```

---

# Practice #4: environment verification #2

* Run `menuconfig`
```shell
make distclean
make menuconfig
```

* As usual, before building on new platform, `make distclean` is recommended to
  make sure we starting from scratch and there no outstanding artifacts.


???

* You may want to copy long commands from PDF or even do some side notes and
  save those for further reference

---

# Building coreboot for QEMU

* QEMU support turned on in `Mainboard` section:
```
Mainboard vendor (Emulation)  --->
Mainboard model (QEMU x86 q35/ich9 (aka qemu -M q35, since v1.4))  --->
```

* This is generic x86 option. More options (models):
```
( ) QEMU AArch64 (virt)
( ) QEMU armv7 (vexpress-a9)
( ) QEMU x86 i440fx/piix4 (aka qemu -M pc)
( ) QEMU power8
(X) QEMU x86 q35/ich9 (aka qemu -M q35, since v1.4)
( ) QEMU RISC-V rv64
```

* let's exit, save and build image

???

* On x86 OS usually only PC and q35 are installed by default:

```shell
qemu -M help # list all available platforms
apt-get install qemu-system # install all platforms
apt-get install qemu-system-arm # install arm
```

---

# Practice #5: Running coreboot in QEMU

* Command to run `qemu`:

```shell
qemu-system-x86_64 -M q35 -bios build/coreboot.rom \
  -serial stdio -display none
```

* `Ctrl-C` exit QEMU
* please create alias for above command:

```shell
alias cb-qemu='qemu-system-x86_64 -M q35 -bios build/coreboot.rom \
  -serial stdio -display none'
```

---

# Practice #6: QEMU debug console

* You may want to enable isa-debugcon for convenient logging
to file, this option is in coreboot menuconfig section `Console`:
```
[*] QEMU debug console output
(0x402) QEMU debug console port
```

* This options should be already enabled by default, so let's try it out
* Then to enable in QEMU:

```shell
cb-qemu -chardev file,id=debugcon,path=log \
    -device isa-debugcon,iobase=0x402,chardev=debugcon
```

```shell
cat log | head
coreboot-4.12-2077-gb18b459661-dirty Mon Aug 10 23:47:21 UTC 2020 bootblock...
FMAP: Found "FLASH" version 1.1 at 0x0.
FMAP: base = 0xffe00000 size = 0x200000 #areas = 3
FMAP: area COREBOOT found @ 200 (2096640 bytes)
CBFS: Locating 'fallback/romstage'
CBFS: Found @ offset 80 size 3b94
BS: bootblock times (exec / console): total (unknown) / 10 ms


coreboot-4.12-2077-gb18b459661-dirty Mon Aug 10 23:47:21 UTC 2020 romstage...
```

???

Note that in `-chardev parameter` log file path is specified
(`path=log`) and in `-device` IO base address (`iobase=0x402`)

---

# Practice #7: QEMU monitor

* monitor is very powerful feature of QEMU - it gives direct control over
  emulated hardware
* it can be enabled in various ways - for details please check configuration
* documentation: https://qemu-project.gitlab.io/qemu/system/monitor.html
* to enable it in our configuration:
```shell
cb-qemu -monitor unix:/tmp/qemu-monitor,server,nowait
```

* our console is busy, so we have to attach in different way, use new terminal:
```shell
$ docker ps
CONTAINER ID        IMAGE                          COMMAND    (...)
1ad3c57be738        3mdeb/coreboot-training-sdk   "/bin/bash"(...)
$ docker exec -it 1ad3c57be738 bash
$ socat - UNIX-CONNECT:/tmp/qemu-monitor
```

---

# Practice #7: QEMU monitor #2

* Check help and try various command
* Make sure to explore `info` command

.image-99[.center[![](images/qemu-monitor.png)]]

---

# Practice #7: QEMU monitor #3

For example let's dump the architectural registers with `info registers`:

```
(qemu) info registers
info registers
EAX=00000000 EBX=00000024 ECX=f8261c34 EDX=00000a46
ESI=000f120b EDI=00000024 EBP=000000c0 ESP=000a3478
EIP=00013f8d EFL=00000246 [---Z-P-] CPL=0 II=0 A20=1 SMM=0 HLT=1
ES =0010 07e3b000 ffffffff 00c09300 DPL=0 DS   [-WA]
CS =0008 07e3b000 ffffffff 00c09f00 DPL=0 CS32 [CRA]
SS =0010 07e3b000 ffffffff 00c09300 DPL=0 DS   [-WA]
DS =0010 07e3b000 ffffffff 00c09300 DPL=0 DS   [-WA]
FS =0010 07e3b000 ffffffff 00c09300 DPL=0 DS   [-WA]
GS =0010 07e3b000 ffffffff 00c09300 DPL=0 DS   [-WA]
LDT=0000 00000000 0000ffff 00008200 DPL=0 LDT
TR =0000 00000000 0000ffff 00008b00 DPL=0 TSS32-busy
GDT=     0009cd30 00000047
IDT=     07e5c560 000007ff
CR0=00000011 CR2=00000000 CR3=00000000 CR4=00000000
...
```

---

# Practice #8: Booting Debian in QEMU

* We can use previously used Debian image (~300MB):
```shell
wget https://cloud.3mdeb.com/index.php/s/8jWopRwpmAZskwk/download \
    -O mb_legacy_debian_sd_8gbv2.img.tar.gz
tar xvf mb_legacy_debian_sd_8gbv2.img.tar.gz
cp mb_legacy_debian_sd_8gbv2.img /path/to/coreboot
```

* Let's boot:
```shell
cb-qemu -hda mb_legacy_debian_sd_8gbv2.img
```

* There is no password for `root`

---

# More QEMU features

* Many more options can be found in documentation, like:
* Debugging using GDB, execution logs
* QEMU has huge potential for debugging and Agile development shift left
  strategy, since we can model hardware behavior and interactions before
  physical components are available to us
* Availability of QEMU for RISC-V and OpenPOWER was very important in being
able to port coreboot

---

# Quiz #1

---

# Demo #1

???

In this demo I will show you:
1. How to add networking, we will prove it installing packages (removing and adding dmidecode)
2. Then we will learn about dmidecode and importance of SMBIOS tables

In short specification defines data structures (and access methods) that can be
used to read management information produced by the BIOS.

DMI vs SMBIOS

SMBIOS was originally known as Desktop Management BIOS (DMIBIOS), since it
interacted with the Desktop Management Interface (DMI)

alias cb-qemu='qemu-system-x86_64 -M q35 -bios build/coreboot.rom \
  -serial stdio -display none'

-hda mb_legacy_debian_sd_8gbv2.img

-netdev user,id=n1 -device virtio-net-pci,netdev=n1


---

# Assignment #1

---

# CBMEM

* CBMEM is special region in DRAM reserved for logs, performance measuring, etc.
* In `utils/cbmem` we find tool utility to get its contents.
* It can be accessed with `libpayload` too.
* Such kind of logging has almost no overhead, but due to static allocation
  we can easily run out of space (rest of logs would be truncated).
* It may be also hard to access those information if we cannot boot OS/payload
  (unless we use JTAG for example).
* Despite its limitations it's used as a standard tool in bug/issue reporting
  for coreboot.

---

# Practice #9: CBMEM usage

* To compile this tool in coreboot tree:
```shell
cd utils/cbmem
make
```

* Let's transfer `cbmem` to Debian image
```shell
fdisk -l mb_legacy_debian_sd_8gbv2.img
losetup -o 1048576 /dev/loop0 mb_legacy_debian_sd_8gbv2.img
mkdir loop0
mount /dev/loop0 loop0
cp util/cbmem/cbmem loop0
umount loop0
losetup -D
```

* Let's boot and use `cbmem`
```shell
cbmem -c | less
cbmem -h
```

---

# Debugging using GDB

* QEMU can be told to open a `gdbserver` on given port with `-gdb tcp::<port>`,
  or `-s` as a shorthand for using port 1234
* Another switch, `-S`, tells QEMU to pause at reset vector and wait until GDB
  connects
* There is a [bug in QEMU](https://bugs.launchpad.net/qemu/+bug/1686170) that
  makes it impossible to debug non-64b code on `qemu-system-x86_64`, which
  hangs for more then 3 years
* Debug build of coreboot stages are located in `build/cbfs/fallback` directory,
  starting GDB from there while pointing to the coreboot's root directory saves
  a lot of typing
* From main coreboot directory:
```shell
qemu-system-i386 -M q35 -bios build/coreboot.rom \
    -serial stdio -display none -s -S
```
* From `build/cbfs/fallback` in another terminal:
```shell
gdb -ex "target remote :1234" -ex "set arch i386" \
    -ex "symbol-file bootblock.debug" -d ../../..
```

???

https://bugs.launchpad.net/qemu/+bug/1686170

QEMU monitor gives a simple debugging environment, which is enough to quickly
check on which instruction the platform hangs. For more complex situations, it
is useful to use full potential of GDB.

`-ex` executes command at startup, `-d` specifies directory for source files.
We need to set architecture, otherwise x64 is assumed.

---

# Debugging using GDB

* Do not try to debug 16b code at the reset vector, GDB can't do that properly,
  even with `set arch i8086`
* We have access to all of the bootblock symbols, we can debug it freely
* We can add other stages with `symbol-file filename` or
  `add-symbol-file filename address`, but...
* Symbols in `*.debug` files for next stages use different base address than
  they are loaded to! Most of the stages are dynamically relocated at runtime
* We are in debugger, we can find the proper address ourselves

---

# Debugging using GDB

* The next stage is started in `prog_run(struct prog*)` function, set a
  breakpoint and run until that function:

```
(gdb) b prog_run
Breakpoint 1 at 0xffffdb10: file src/lib/prog_ops.c, line 26.
(gdb) c
Continuing.

Breakpoint 1, prog_run (prog=<unavailable>) at src/lib/prog_ops.c:26
26		platform_prog_run(prog);
(gdb)
```

???

`symbol-file` deletes old symbols and adds new ones, while `add-symbol-file`
just adds and leaves old ones intact.

romstage is not relocated, it is XIP, but the symbols in `romstage.debug` still
use different base address. All further stages are relocated when they are
loaded into RAM.

`(prog=<unavailable>)` means that GDB cannot automatically obtain the value of
that parameter, most likely due to code optimization. We need to get it
manually.

---

# Debugging using GDB

```
(gdb) list prog_run
24	void prog_run(struct prog *prog)
25	{
26		platform_prog_run(prog);
27		arch_prog_run(prog);
28	}
(gdb) ptype /o struct prog
/* offset    |  size */  type = struct prog {
/*    0      |     4 */    enum prog_type type;
/*    4      |     4 */    uint32_t cbfs_type;
/*    8      |     4 */    const char *name;
/*   12      |    16 */    struct region_device {
/*   12      |     4 */        const struct region_device *root;
/*   16      |     4 */        const struct region_device_ops *ops;
/*   20      |     8 */        struct region {
/*   20      |     4 */            size_t offset;
/*   24      |     4 */            size_t size;
                                   /* total size (bytes):    8 */
                               } region;
                               /* total size (bytes):   16 */
                           } rdev;
/*   28      |     4 */    void (*entry)(void *);
/*   32      |     4 */    void *arg;
                           /* total size (bytes):   36 */
                         }
```

???

`list prog_run` prints a couple lines more, removed here for clarity.

`ptype` prints type, with `/o` it prints offsets of fields, as well as includes
all substructs in the listing. This way we don't have to go through multiple
source files.

`prog.rdev.region.offset` holds the address of `.text` section of a stage - this
is exactly the address we need for `add-symbol-file`.

---

# Debugging using GDB

* On x86, parameters are passed on the stack (usually) from right to left
  (usually), a call to the function pushes also the return address to the stack
* Immediately after the entry, the first parameter can be accessed as `$esp + 4`
* This example sets a _convenience variable_ pointing to that parameter:

```
(gdb) set $my_prog = {struct prog*} ($esp + 4)
```

---

# Debugging using GDB

* Read the value of `offset` with `print`/`p` and use it as a base address for
  next stage:

```
(gdb) p /x $my_prog->rdev.region.offset
$3 = 0xffe00300
(gdb) add-symbol-file romstage.debug 0xffe00300
add symbol table from file "romstage.debug" at
	.text_addr = 0xffe00300
(y or n) y
Reading symbols from romstage.debug...done.
(gdb)
```

* It may be a good idea to `remove-symbol-file` before continuing - if the same
  symbol name is used in romstage, commands like `list` would still print the
  one defined by bootblock

???

`/x` uses hexadecimal format, this way it's easier to see if this is proper,
aligned address.

If old symbols are not removed, `b` and `hb` will try to add breakpoints for all
functions with specified name. It is possible to run out of possible `hb`s
quickly.

---

# Debugging using GDB

* Now we can use symbols from romstage
* Every next stage also requires the load address to be passed
* We can follow the same route, in which case we need to repeat everything from
  `b prog_run` - every new stage has different function, even though they are
  all named `prog_run()`
* Alternatively, if debug level is high enough we can redo just the breakpoint
  and observe the QEMU serial output:

```
Decompressing stage fallback/postcar @ 0x07fd3fc0 (32432 bytes)
Loading module at 0x07fd4000 with entry 0x07fd4000. filesize: 0x3b90 memsize: 0x7e70
Processing 124 relocs. Offset value of 0x05fd4000
```

* We are interested in `Loading module at X`, other values may differ
* Such line is not printed for bootblock->romstage transition because romstage
  isn't compressed and it executes in place (XIP)

???

Because some symbols are re-defined, commands like `list` may show wrong
functions.

Notice the destination is near the top of RAM (128M, QEMU default). This is why
those addresses are not known ahead.

---

# Quiz #2

---

# Assignment #2

---

class: center, middle

# Q&A

## Thank you
