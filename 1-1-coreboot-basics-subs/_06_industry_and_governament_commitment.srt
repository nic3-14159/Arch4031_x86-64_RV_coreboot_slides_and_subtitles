1
00:00:00,74 --> 00:00:01,14
Okay,

2
00:00:01,14 --> 00:00:03,03
so there is this chord with stuff,

3
00:00:03,03 --> 00:00:05,17
but who really do this,

4
00:00:05,17 --> 00:00:05,44
like,

5
00:00:05,45 --> 00:00:07,25
who cares about this project?

6
00:00:07,64 --> 00:00:11,0
So there are many components that are involved,

7
00:00:11,01 --> 00:00:12,09
some of them very,

8
00:00:12,09 --> 00:00:12,87
very serious,

9
00:00:12,87 --> 00:00:15,76
like Google's IMAs and the Intel MediaTek

10
00:00:16,14 --> 00:00:22,73
And we can also find some cool information about open

11
00:00:22,73 --> 00:00:26,04
compute project with kind of which is a place where

12
00:00:26,04 --> 00:00:27,76
Facebook and Google

13
00:00:27,77 --> 00:00:32,27
And right now many other organizations try to create open

14
00:00:32,27 --> 00:00:32,83
server,

15
00:00:32,83 --> 00:00:34,72
which gives well,

16
00:00:34,73 --> 00:00:38,05
like well maintained and film were well maintained,

17
00:00:38,05 --> 00:00:39,45
it harder for a long time

18
00:00:39,84 --> 00:00:44,15
You can see even that official tests like Hiccup account

19
00:00:44,16 --> 00:00:45,6
contained corporate fork,

20
00:00:45,61 --> 00:00:47,96
which means maybe they using it

21
00:00:47,97 --> 00:00:49,5
For some reasons,

22
00:00:49,51 --> 00:00:54,7
there are situations where some core boot the Census Service

23
00:00:54,7 --> 00:00:59,27
providers are hired by some some customers in,

24
00:00:59,28 --> 00:01:00,05
for example,

25
00:01:00,05 --> 00:01:02,66
in case of Port of Super Micro

26
00:01:02,74 --> 00:01:04,67
Uh,

27
00:01:04,68 --> 00:01:07,48
one company was hired by VPN provider

28
00:01:07,49 --> 00:01:07,81
Uh,

29
00:01:07,82 --> 00:01:10,26
it's very interesting that,

30
00:01:10,94 --> 00:01:12,24
for example,

31
00:01:12,24 --> 00:01:15,48
VPN providers see the value in open source firmware

32
00:01:15,49 --> 00:01:16,7
So why,

33
00:01:16,71 --> 00:01:17,96
Why core boot?

34
00:01:17,97 --> 00:01:18,96
It's free

35
00:01:18,97 --> 00:01:21,22
It's free as free beer

36
00:01:21,23 --> 00:01:23,14
It's very simple design

37
00:01:23,15 --> 00:01:27,95
It does minimum booting to a minimum

38
00:01:28,44 --> 00:01:30,45
What is needed to boot or s,

39
00:01:30,46 --> 00:01:30,75
uh,

40
00:01:30,76 --> 00:01:32,24
it's vendor independent

41
00:01:32,25 --> 00:01:33,6
It's cross platform

42
00:01:33,61 --> 00:01:35,05
It's very fast

43
00:01:35,06 --> 00:01:38,18
It's way easier than than you were

44
00:01:38,18 --> 00:01:44,56
Five in comparison to to legacy bios written in assembly

45
00:01:44,57 --> 00:01:45,34
Um,

46
00:01:45,35 --> 00:01:47,18
the court quality is much better,

47
00:01:47,18 --> 00:01:48,99
is much more flexible

48
00:01:49,0 --> 00:01:49,36
Um,

49
00:01:49,37 --> 00:01:51,05
it's very important because,

50
00:01:51,06 --> 00:01:51,58
uh,

51
00:01:51,59 --> 00:01:54,44
many companies want to customize film where,

52
00:01:54,45 --> 00:01:54,87
um,

53
00:01:54,88 --> 00:01:58,86
in in ways that it will work in special way

54
00:01:58,86 --> 00:01:59,96
on their hardware

55
00:02:00,04 --> 00:02:00,81
Um,

56
00:02:00,82 --> 00:02:01,69
and typically,

57
00:02:01,69 --> 00:02:04,87
this is not supported by bios or,

58
00:02:04,87 --> 00:02:05,09
um,

59
00:02:05,09 --> 00:02:06,06
500 hours

60
00:02:06,74 --> 00:02:07,46
Of course,

61
00:02:07,47 --> 00:02:09,75
you may pay them like Big Buck,

62
00:02:09,75 --> 00:02:10,96
but also,

63
00:02:10,96 --> 00:02:13,66
you can go to open source filmer and take open

64
00:02:13,66 --> 00:02:16,96
source film and modify optimized performance

65
00:02:16,97 --> 00:02:17,56
Um,

66
00:02:17,57 --> 00:02:18,02
like,

67
00:02:18,02 --> 00:02:20,09
do some more power saving art,

68
00:02:20,09 --> 00:02:21,21
security,

69
00:02:21,22 --> 00:02:22,6
disabled some features

70
00:02:22,6 --> 00:02:23,16
For example

71
00:02:23,16 --> 00:02:28,31
There was story from Simmons that they had CNC machine

72
00:02:28,31 --> 00:02:31,06
that they wanted to disable SMS,

73
00:02:31,06 --> 00:02:32,86
and it was impossible in u F I,

74
00:02:33,24 --> 00:02:37,1
um because those semis cause some delays in in CNC

75
00:02:37,1 --> 00:02:40,95
machines would make make it less precise

76
00:02:41,34 --> 00:02:42,06
Um,

77
00:02:42,54 --> 00:02:45,75
and Corbett is cross platform

78
00:02:46,74 --> 00:02:49,17
It's not only possible to run on one machine,

79
00:02:49,18 --> 00:02:50,69
many machines can be run

80
00:02:50,7 --> 00:02:53,17
It's easy to port relatively

81
00:02:53,18 --> 00:02:54,52
If you will go through training,

82
00:02:54,52 --> 00:02:57,66
definitely you can understand how to do that

83
00:02:58,14 --> 00:03:02,42
And we recently we see even more commitment so you

84
00:03:02,42 --> 00:03:07,8
can see that purism Laptop producer execute laptop producer Uh

85
00:03:07,81 --> 00:03:12,96
System 76 another provider of laptops loves open source,

86
00:03:12,96 --> 00:03:14,46
filmer and core

87
00:03:14,46 --> 00:03:18,26
But we also see companies like in Chicago,

88
00:03:18,27 --> 00:03:22,68
which provide cubes well certified laptops

89
00:03:22,69 --> 00:03:23,35
Um,

90
00:03:23,74 --> 00:03:30,06
look at car boot as valuable boot system and and

91
00:03:30,06 --> 00:03:34,61
also firewall providers like Protect Lee PC engines

92
00:03:34,62 --> 00:03:40,17
Those companies see value and their customers value in cold

93
00:03:40,17 --> 00:03:43,26
But as a bias for those hungry,

94
00:03:43,84 --> 00:03:45,43
if that is not enough,

95
00:03:45,44 --> 00:03:48,07
there is even a government commitment

96
00:03:48,08 --> 00:03:48,38
So,

97
00:03:48,38 --> 00:03:49,35
for example,

98
00:03:49,35 --> 00:03:53,3
there is a federal office for information security in Germany

99
00:03:53,31 --> 00:03:54,05
Um,

100
00:03:54,06 --> 00:03:55,56
it's good example

101
00:03:55,57 --> 00:03:57,83
And like representatives of,

102
00:03:57,84 --> 00:03:58,4
uh,

103
00:03:58,41 --> 00:04:05,26
this agency frequently attended Car boot conference officer spindle conference

104
00:04:05,44 --> 00:04:06,03
um,

105
00:04:06,04 --> 00:04:09,73
saying that they value corporate because they can fork fork

106
00:04:09,73 --> 00:04:10,19
it

107
00:04:10,2 --> 00:04:10,7
Um,

108
00:04:10,71 --> 00:04:12,15
it's secure

109
00:04:12,15 --> 00:04:14,19
They are not depending on anyone

110
00:04:14,2 --> 00:04:15,75
It's easy to out it

111
00:04:15,75 --> 00:04:20,11
They can check the code that runs on government administration

112
00:04:20,12 --> 00:04:21,41
machines,

113
00:04:21,42 --> 00:04:22,22
and that,

114
00:04:22,23 --> 00:04:23,05
and that is very,

115
00:04:23,05 --> 00:04:24,55
very important for them

116
00:04:24,74 --> 00:04:29,6
There is even an NSA contribution to car boot

117
00:04:29,61 --> 00:04:36,15
Very interesting contribution about SME transfer monitor for Intel

118
00:04:36,16 --> 00:04:37,58
Interestingly,

119
00:04:37,59 --> 00:04:42,67
there are some presentation where a person who imported STM

120
00:04:42,68 --> 00:04:47,1
said that someone asked what is needed to support a

121
00:04:47,1 --> 00:04:48,13
semi transfer monitor,

122
00:04:48,13 --> 00:04:51,45
which is advanced security feature of Intel platforms

123
00:04:51,46 --> 00:04:55,43
He say you need SDM friendly bias

124
00:04:55,44 --> 00:04:58,99
So Colbert is definitely SDM friendly bias,

125
00:04:59,0 --> 00:05:01,49
not u F I implementations,

126
00:05:01,49 --> 00:05:04,25
not some old bios implementations

