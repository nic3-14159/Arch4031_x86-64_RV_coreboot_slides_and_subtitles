1
00:00:00,94 --> 00:00:01,57
committing

2
00:00:01,57 --> 00:00:05,42
So if you are committing any call to corporate repository

3
00:00:05,42 --> 00:00:09,99
you should follow Windows Kernel submitting purchase documentation

4
00:00:10,0 --> 00:00:14,05
The most important rules are that there should be only

5
00:00:14,05 --> 00:00:15,68
one logical change per commit

6
00:00:15,69 --> 00:00:18,55
It's very it is very important because if you mix

7
00:00:18,64 --> 00:00:20,49
couple of fixes in one community,

8
00:00:20,49 --> 00:00:23,84
then it's hard to separate that and hard to understand

9
00:00:23,85 --> 00:00:25,48
If there will be any back,

10
00:00:25,48 --> 00:00:26,04
introduce it

11
00:00:26,04 --> 00:00:28,63
It's hard to understand what what caused that back

12
00:00:28,63 --> 00:00:31,48
What does not does not cause that back

13
00:00:31,49 --> 00:00:32,16
Um,

14
00:00:32,64 --> 00:00:37,14
all comments should contain three sections separated by empty lines

15
00:00:37,14 --> 00:00:41,09
So first section is titled as second tax section is

16
00:00:41,09 --> 00:00:45,66
description and third section is sign off Region

17
00:00:46,24 --> 00:00:50,15
The title typically should not be bigger than 64 for

18
00:00:50,16 --> 00:00:52,23
65 characters

19
00:00:52,24 --> 00:00:55,28
Typical typical method is that,

20
00:00:55,29 --> 00:00:55,79
First,

21
00:00:55,79 --> 00:00:57,57
to play some module

22
00:00:57,58 --> 00:01:01,99
This is used in various ways

23
00:01:02,0 --> 00:01:05,99
The best way to comply with current rules is just

24
00:01:06,0 --> 00:01:07,44
check the okay,

25
00:01:07,44 --> 00:01:07,65
well,

26
00:01:07,66 --> 00:01:09,16
how the files,

27
00:01:09,16 --> 00:01:12,21
which I modify or how the directories,

28
00:01:12,22 --> 00:01:16,04
which are near the location that I do the modification

29
00:01:16,05 --> 00:01:18,68
were documented in commit messages

30
00:01:18,68 --> 00:01:21,84
So it's good to look at git log and see

31
00:01:21,84 --> 00:01:23,87
how those changes were committed

32
00:01:23,87 --> 00:01:25,46
Then you can follow the same pattern,

33
00:01:25,74 --> 00:01:29,08
but typically it is like a component

34
00:01:29,09 --> 00:01:29,97
Uh,

35
00:01:29,98 --> 00:01:31,42
and then we have,

36
00:01:31,43 --> 00:01:31,92
um,

37
00:01:31,93 --> 00:01:35,06
title associated with this component

38
00:01:36,54 --> 00:01:37,61
Yeah

39
00:01:37,62 --> 00:01:39,88
So then we have this explanation explanation

40
00:01:39,88 --> 00:01:40,56
Should be,

41
00:01:41,34 --> 00:01:42,15
uh,

42
00:01:42,16 --> 00:01:47,21
mostly why we do this change what that change really

43
00:01:47,21 --> 00:01:48,08
does

44
00:01:48,09 --> 00:01:50,48
What's the benefit of it?

45
00:01:50,49 --> 00:01:51,44
Um,

46
00:01:51,45 --> 00:01:52,39
And like,

47
00:01:52,4 --> 00:01:55,7
you have to do kind of marketing peach

48
00:01:55,71 --> 00:01:59,51
Why this is needed and why this improved improves the

49
00:01:59,51 --> 00:02:00,08
situation

50
00:02:00,08 --> 00:02:03,41
So if some reviewer will read that he will,

51
00:02:03,41 --> 00:02:06,68
he she will be convinced that this has to be

52
00:02:06,68 --> 00:02:07,26
merged

