1
00:00:00,54 --> 00:00:04,09
working with open source projects like Lennox Corner or Corporate

2
00:00:04,09 --> 00:00:11,52
Project requires my studying GID and preferably get command line

3
00:00:11,53 --> 00:00:12,46
interface

4
00:00:13,24 --> 00:00:13,62
Um,

5
00:00:13,63 --> 00:00:16,51
so why Why did come online?

6
00:00:16,52 --> 00:00:19,2
This is because the user interface,

7
00:00:19,21 --> 00:00:21,62
like all the rappers around,

8
00:00:21,62 --> 00:00:21,96
kid,

9
00:00:22,34 --> 00:00:29,15
typically because a lot of problems are they hiding something

10
00:00:29,15 --> 00:00:30,17
They kind of,

11
00:00:30,18 --> 00:00:30,87
uh,

12
00:00:30,88 --> 00:00:31,68
cause,

13
00:00:31,69 --> 00:00:32,32
um,

14
00:00:32,33 --> 00:00:37,48
situation in which we don't precisely understand what's happening behind

15
00:00:37,48 --> 00:00:38,05
the scene

16
00:00:38,44 --> 00:00:42,53
Especially when someone during the review asked us to replace

17
00:00:42,53 --> 00:00:44,67
patches or change the order

18
00:00:44,68 --> 00:00:49,84
It requires my mastering the command line Get interface to

19
00:00:49,84 --> 00:00:54,61
simplify that I'm using for more than 10 years,

20
00:00:54,62 --> 00:00:55,12
Um,

21
00:00:55,13 --> 00:00:58,36
following a set of aliases

22
00:00:58,84 --> 00:01:00,55
So Ali is for,

23
00:01:00,94 --> 00:01:01,42
um,

24
00:01:01,43 --> 00:01:05,85
Comet and signing for checking out for branching,

25
00:01:05,86 --> 00:01:09,64
for checking status for checking the difference

26
00:01:09,65 --> 00:01:10,47
Um,

27
00:01:10,48 --> 00:01:14,04
for checking the differences which are already in staging,

28
00:01:14,05 --> 00:01:15,4
um,

29
00:01:15,41 --> 00:01:20,44
also like there is very useful stuff around local command

30
00:01:20,45 --> 00:01:26,27
which gives you the ability to draw cool graphs in

31
00:01:26,28 --> 00:01:27,26
council

32
00:01:28,04 --> 00:01:30,48
So let me present those,

33
00:01:30,49 --> 00:01:31,16
um,

34
00:01:31,54 --> 00:01:33,55
those aliases to you

