1
00:00:01,280 --> 00:00:06,640
so let's start with a little bit

2
00:00:03,199 --> 00:00:10,719
of the history of uh core boot

3
00:00:06,640 --> 00:00:11,440
so version one was created in 1999 as i

4
00:00:10,719 --> 00:00:15,519
said

5
00:00:11,440 --> 00:00:19,359
uh linux bios created by ron minich

6
00:00:15,519 --> 00:00:22,640
that time he worked in los alamos um

7
00:00:19,359 --> 00:00:25,279
laboratory the goal was to boot linux

8
00:00:22,640 --> 00:00:27,920
linux as fast as possible and if

9
00:00:25,279 --> 00:00:31,679
possible to put linux inside

10
00:00:27,920 --> 00:00:36,000
spy flash when normally bios

11
00:00:31,679 --> 00:00:37,440
was placed so he tried to replace bios

12
00:00:36,000 --> 00:00:40,000
with linux

13
00:00:37,440 --> 00:00:41,280
this project was called linux bios and

14
00:00:40,000 --> 00:00:44,079
it was

15
00:00:41,280 --> 00:00:46,320
many operated in version one mainly

16
00:00:44,079 --> 00:00:50,800
operated in 1999

17
00:00:46,320 --> 00:00:53,840
and 2000 then we have another stage of

18
00:00:50,800 --> 00:00:57,360
linux bios back then and future future

19
00:00:53,840 --> 00:01:00,160
core boot uh 2000 to 2005.

20
00:00:57,360 --> 00:01:02,719
then we started to gain some additional

21
00:01:00,160 --> 00:01:06,240
support for various architectures

22
00:01:02,719 --> 00:01:09,600
x86 alpha power pc and

23
00:01:06,240 --> 00:01:11,439
then there was um there were some

24
00:01:09,600 --> 00:01:14,640
problems with projects like

25
00:01:11,439 --> 00:01:17,840
the memory initialization uh causes some

26
00:01:14,640 --> 00:01:20,799
significant issues because the memory

27
00:01:17,840 --> 00:01:21,920
technology rise in complexity because of

28
00:01:20,799 --> 00:01:25,360
that

29
00:01:21,920 --> 00:01:28,720
rom city project was created to

30
00:01:25,360 --> 00:01:32,079
to change c uh into stackless up

31
00:01:28,720 --> 00:01:35,360
assembly and rom cc kind of compiled c

32
00:01:32,079 --> 00:01:39,280
code in a way that um it use

33
00:01:35,360 --> 00:01:42,640
um registers for uh storing values

34
00:01:39,280 --> 00:01:45,439
as a stack then uh

35
00:01:42,640 --> 00:01:45,759
there was device 3 concept is introduced

36
00:01:45,439 --> 00:01:48,720
by

37
00:01:45,759 --> 00:01:49,920
back in the days uh probably even before

38
00:01:48,720 --> 00:01:53,119
linux

39
00:01:49,920 --> 00:01:56,000
device 3 concept then

40
00:01:53,119 --> 00:01:56,479
uh first payloads were introduced to

41
00:01:56,000 --> 00:01:59,280
make

42
00:01:56,479 --> 00:02:00,479
booting more flexible in this period of

43
00:01:59,280 --> 00:02:02,479
time there was big

44
00:02:00,479 --> 00:02:04,799
contribution from various vendors from

45
00:02:02,479 --> 00:02:06,000
inter a lot of contribution and open

46
00:02:04,799 --> 00:02:09,759
source ajisa

47
00:02:06,000 --> 00:02:11,200
from amd via um cs

48
00:02:09,759 --> 00:02:13,280
uh there were many components

49
00:02:11,200 --> 00:02:15,520
contributing to that project back in the

50
00:02:13,280 --> 00:02:15,520
days

51
00:02:15,920 --> 00:02:19,680
then we had another stage um we can call

52
00:02:19,440 --> 00:02:23,680
it

53
00:02:19,680 --> 00:02:25,040
uh version two plus uh it was 2005 to

54
00:02:23,680 --> 00:02:27,440
2008

55
00:02:25,040 --> 00:02:28,720
uh where where there was introduction of

56
00:02:27,440 --> 00:02:32,400
cash as ram

57
00:02:28,720 --> 00:02:35,200
um amd 64 port um

58
00:02:32,400 --> 00:02:37,840
acpi and smm support was added there was

59
00:02:35,200 --> 00:02:41,040
flash on project created which helped in

60
00:02:37,840 --> 00:02:42,560
flashing uh linux bios binaries or

61
00:02:41,040 --> 00:02:45,599
carbon binaries

62
00:02:42,560 --> 00:02:46,239
um free software foundation was

63
00:02:45,599 --> 00:02:49,360
interested

64
00:02:46,239 --> 00:02:51,519
in core boot and started use

65
00:02:49,360 --> 00:02:53,599
the linux bios core boot on their own

66
00:02:51,519 --> 00:02:57,120
servers

67
00:02:53,599 --> 00:03:01,200
in version 3 2006 um

68
00:02:57,120 --> 00:03:05,280
2008 there was more than 100

69
00:03:01,200 --> 00:03:09,280
main boards supported but version 3 was

70
00:03:05,280 --> 00:03:13,519
a kind of experiment where some

71
00:03:09,280 --> 00:03:16,959
things were tried for example cbfs um

72
00:03:13,519 --> 00:03:20,239
just to um so it was like uh

73
00:03:16,959 --> 00:03:22,720
um it was like a

74
00:03:20,239 --> 00:03:25,120
wrong part of evolution or part of

75
00:03:22,720 --> 00:03:28,159
evolution that gave some ideas

76
00:03:25,120 --> 00:03:31,040
which were moved forward but but the v3

77
00:03:28,159 --> 00:03:32,000
branch was killed they get back to v2

78
00:03:31,040 --> 00:03:35,519
and ported

79
00:03:32,000 --> 00:03:38,959
some um some most impo important

80
00:03:35,519 --> 00:03:42,879
improvements uh to v2 and then progress

81
00:03:38,959 --> 00:03:44,239
to v4 in 2008 linux bios was renamed to

82
00:03:42,879 --> 00:03:46,159
coreboot

83
00:03:44,239 --> 00:03:47,920
there was also change of the maintainer

84
00:03:46,159 --> 00:03:51,200
back then uh ron

85
00:03:47,920 --> 00:03:52,480
kind of um give place for stefan

86
00:03:51,200 --> 00:03:57,599
raynauer

87
00:03:52,480 --> 00:04:01,040
and in v4 uh from 2009 to 2012

88
00:03:57,599 --> 00:04:04,640
um they moved from svn to git

89
00:04:01,040 --> 00:04:08,319
there was also big contribution from amd

90
00:04:04,640 --> 00:04:11,920
more against a staff and more um

91
00:04:08,319 --> 00:04:12,239
hardware support and yeah and finally we

92
00:04:11,920 --> 00:04:16,560
are

93
00:04:12,239 --> 00:04:20,560
in version 4 and evolving

94
00:04:16,560 --> 00:04:21,280
since 2012 uh most important thing that

95
00:04:20,560 --> 00:04:24,560
kind of

96
00:04:21,280 --> 00:04:28,000
um gave me a lot of

97
00:04:24,560 --> 00:04:31,040
thinking about core boot was that

98
00:04:28,000 --> 00:04:31,919
google decided to use core boot as our

99
00:04:31,040 --> 00:04:34,960
chromebook

100
00:04:31,919 --> 00:04:38,240
default bios this was huge uh

101
00:04:34,960 --> 00:04:41,280
bust for boost for coreboot project

102
00:04:38,240 --> 00:04:44,160
um intel created fsp

103
00:04:41,280 --> 00:04:44,720
this was most probably because google

104
00:04:44,160 --> 00:04:47,280
pushes

105
00:04:44,720 --> 00:04:48,160
he pushed intel that they don't want to

106
00:04:47,280 --> 00:04:52,320
use

107
00:04:48,160 --> 00:04:55,520
um this ami inside phoenix code

108
00:04:52,320 --> 00:04:55,520
so they decide to

109
00:04:55,600 --> 00:05:00,639
ask uh intel to provide like smaller

110
00:04:58,639 --> 00:05:02,000
possible initialization code that could

111
00:05:00,639 --> 00:05:05,360
be integrated

112
00:05:02,000 --> 00:05:08,800
in uh in in corporate

113
00:05:05,360 --> 00:05:11,600
um so at that point i worked in intel

114
00:05:08,800 --> 00:05:12,560
um and it was like blasting information

115
00:05:11,600 --> 00:05:15,680
for me

116
00:05:12,560 --> 00:05:17,759
uh also the like um couple additional

117
00:05:15,680 --> 00:05:21,360
things happened at that time

118
00:05:17,759 --> 00:05:22,639
um there was wikileaks uh vault seven

119
00:05:21,360 --> 00:05:25,840
publications

120
00:05:22,639 --> 00:05:29,360
there was snowden uh revelations

121
00:05:25,840 --> 00:05:31,120
and many um major security failures from

122
00:05:29,360 --> 00:05:34,240
big city conventors

123
00:05:31,120 --> 00:05:36,240
and ibvs and probably because of that

124
00:05:34,240 --> 00:05:38,240
core boot was more and more and more

125
00:05:36,240 --> 00:05:41,120
more popular and right now

126
00:05:38,240 --> 00:05:43,039
uh definitely we're going into stage of

127
00:05:41,120 --> 00:05:45,919
renaissance when

128
00:05:43,039 --> 00:05:48,639
major hardware vendors understand what

129
00:05:45,919 --> 00:05:52,320
core boot is what value it provides

130
00:05:48,639 --> 00:05:56,080
so um as you can see on this

131
00:05:52,320 --> 00:05:57,600
graphics um on on core boot website you

132
00:05:56,080 --> 00:06:00,720
can find

133
00:05:57,600 --> 00:06:01,199
all the releases and the dates recently

134
00:06:00,720 --> 00:06:04,800
12

135
00:06:01,199 --> 00:06:08,639
may um there was release of version

136
00:06:04,800 --> 00:06:12,160
four 412 the cadence of corbett release

137
00:06:08,639 --> 00:06:18,960
is 6 months and yeah of course we should

138
00:06:12,160 --> 00:06:18,960
expect in november another release

