1
00:00:00,960 --> 00:00:07,440
so this is not filmer history lecture

2
00:00:04,000 --> 00:00:10,480
or um or some extensive

3
00:00:07,440 --> 00:00:11,360
um course about firmware history and

4
00:00:10,480 --> 00:00:14,480
explanation

5
00:00:11,360 --> 00:00:17,359
of all possible terms but we

6
00:00:14,480 --> 00:00:18,880
at least briefly have to go into what i

7
00:00:17,359 --> 00:00:21,680
have on the slide

8
00:00:18,880 --> 00:00:22,800
so um let's start with bios bios means

9
00:00:21,680 --> 00:00:25,760
bios

10
00:00:22,800 --> 00:00:26,720
basic input output system is a low level

11
00:00:25,760 --> 00:00:28,960
software

12
00:00:26,720 --> 00:00:30,000
also some people call it firmware i

13
00:00:28,960 --> 00:00:32,640
believe more correct

14
00:00:30,000 --> 00:00:33,760
term would be boot firmware or host

15
00:00:32,640 --> 00:00:36,719
firmware

16
00:00:33,760 --> 00:00:38,559
um which is responsible for hardware

17
00:00:36,719 --> 00:00:42,320
hardware initialization

18
00:00:38,559 --> 00:00:45,360
after a week we power on um

19
00:00:42,320 --> 00:00:49,440
the platform um since 2010

20
00:00:45,360 --> 00:00:52,719
um bios migrates from like legacy bios

21
00:00:49,440 --> 00:00:56,879
um interrupt driven typically assemble

22
00:00:52,719 --> 00:00:59,520
written um code to ufi which

23
00:00:56,879 --> 00:01:00,719
is more extensible got like well

24
00:00:59,520 --> 00:01:03,600
designed

25
00:01:00,719 --> 00:01:04,799
interface but it's way more complex and

26
00:01:03,600 --> 00:01:08,320
caused a lot of other

27
00:01:04,799 --> 00:01:10,720
issues but the problem is that the usage

28
00:01:08,320 --> 00:01:13,920
of bios world is very confusing

29
00:01:10,720 --> 00:01:16,000
um even hardware vendors or like oems

30
00:01:13,920 --> 00:01:18,960
odms

31
00:01:16,000 --> 00:01:20,720
just provide bios in their manuals in

32
00:01:18,960 --> 00:01:23,280
their data sheets

33
00:01:20,720 --> 00:01:24,400
give give this word different meaning

34
00:01:23,280 --> 00:01:26,560
sometimes they mean

35
00:01:24,400 --> 00:01:28,320
type of firmware or software or

36
00:01:26,560 --> 00:01:30,320
sometimes they mean

37
00:01:28,320 --> 00:01:31,920
yearly hardware initialization software

38
00:01:30,320 --> 00:01:34,560
or simply

39
00:01:31,920 --> 00:01:35,520
they just like glue everything together

40
00:01:34,560 --> 00:01:37,119
and mix and

41
00:01:35,520 --> 00:01:39,360
do not understand what's the difference

42
00:01:37,119 --> 00:01:43,040
between one and the other

43
00:01:39,360 --> 00:01:46,079
ufi is your unified extensible firmware

44
00:01:43,040 --> 00:01:46,560
specification this is a specification

45
00:01:46,079 --> 00:01:49,600
which is

46
00:01:46,560 --> 00:01:52,880
managed by ufi forum it was first

47
00:01:49,600 --> 00:01:56,640
published in 2007 but its origin is

48
00:01:52,880 --> 00:02:00,000
in mid 90s associated with

49
00:01:56,640 --> 00:02:01,200
the creation of itanium intel italian

50
00:02:00,000 --> 00:02:03,680
platform

51
00:02:01,200 --> 00:02:04,640
where they realized that legacy bios

52
00:02:03,680 --> 00:02:08,160
would have

53
00:02:04,640 --> 00:02:11,520
problem booting 64-bit italian

54
00:02:08,160 --> 00:02:14,640
processor so they started to create efi

55
00:02:11,520 --> 00:02:18,800
and then this um evolved into

56
00:02:14,640 --> 00:02:21,680
ufi published in 2007.

57
00:02:18,800 --> 00:02:23,280
of course ufi is also confused con

58
00:02:21,680 --> 00:02:26,319
confusing

59
00:02:23,280 --> 00:02:26,879
many people mix ufi they kind of mean

60
00:02:26,319 --> 00:02:29,280
ufi

61
00:02:26,879 --> 00:02:30,080
as a bios and of course bios is also not

62
00:02:29,280 --> 00:02:33,599
clear

63
00:02:30,080 --> 00:02:36,400
um but ufi is just a specification

64
00:02:33,599 --> 00:02:38,879
it's not an implementation there is

65
00:02:36,400 --> 00:02:42,720
something called edk

66
00:02:38,879 --> 00:02:44,480
2 there was previously edk1 so edk2 is a

67
00:02:42,720 --> 00:02:47,599
publicly available

68
00:02:44,480 --> 00:02:51,200
implementation of ufi specification

69
00:02:47,599 --> 00:02:53,120
the code is on on github and

70
00:02:51,200 --> 00:02:54,959
to move on with the terminology there is

71
00:02:53,120 --> 00:02:58,080
something called fsp

72
00:02:54,959 --> 00:03:00,400
or more correctly intel fsp it's

73
00:02:58,080 --> 00:03:01,680
it's intel firmware support package it's

74
00:03:00,400 --> 00:03:04,720
a binary

75
00:03:01,680 --> 00:03:06,640
release which is done by intel uh

76
00:03:04,720 --> 00:03:08,080
right now they're pushing binaries to

77
00:03:06,640 --> 00:03:11,120
github and

78
00:03:08,080 --> 00:03:12,560
and it is for this is targeted for given

79
00:03:11,120 --> 00:03:16,159
marco architecture and

80
00:03:12,560 --> 00:03:18,800
the goal of fsp of the binary package

81
00:03:16,159 --> 00:03:20,560
uh which should can be included in

82
00:03:18,800 --> 00:03:22,770
various firmwares

83
00:03:20,560 --> 00:03:24,080
is to perform um

84
00:03:22,770 --> 00:03:27,280
[Music]

85
00:03:24,080 --> 00:03:28,720
like intellectual property uh critical

86
00:03:27,280 --> 00:03:32,560
initialization of the

87
00:03:28,720 --> 00:03:33,840
chipset and processor so this is just to

88
00:03:32,560 --> 00:03:36,159
protect

89
00:03:33,840 --> 00:03:37,040
some intellectual property which intel

90
00:03:36,159 --> 00:03:40,640
climbs

91
00:03:37,040 --> 00:03:42,400
it would be bad to inform competition

92
00:03:40,640 --> 00:03:44,640
about what's inside

93
00:03:42,400 --> 00:03:46,319
this is of course questionable and and

94
00:03:44,640 --> 00:03:50,080
of open source firmware

95
00:03:46,319 --> 00:03:53,360
um community do not agree with those

96
00:03:50,080 --> 00:03:57,120
uh crimes but uh this is this is the

97
00:03:53,360 --> 00:03:57,120
official statement from winter that we

98
00:03:58,840 --> 00:04:03,519
have

99
00:04:00,080 --> 00:04:06,640
then we have something called ajisa

100
00:04:03,519 --> 00:04:09,760
this is like fsp which coming from

101
00:04:06,640 --> 00:04:12,560
um from from amd

102
00:04:09,760 --> 00:04:14,480
uh it means amd generic and encapsulated

103
00:04:12,560 --> 00:04:17,680
software architecture

104
00:04:14,480 --> 00:04:20,880
so it is for

105
00:04:17,680 --> 00:04:24,000
early initialization of the amd

106
00:04:20,880 --> 00:04:27,120
processors the next

107
00:04:24,000 --> 00:04:29,199
firmware jargon term is binary blob

108
00:04:27,120 --> 00:04:30,880
it's very often used in open source

109
00:04:29,199 --> 00:04:33,280
firmware community

110
00:04:30,880 --> 00:04:34,479
and it is good to know what does what

111
00:04:33,280 --> 00:04:37,600
does it mean

112
00:04:34,479 --> 00:04:40,160
so this is official term for software

113
00:04:37,600 --> 00:04:42,560
distributed in binary form only

114
00:04:40,160 --> 00:04:43,680
so we have some binary component which

115
00:04:42,560 --> 00:04:46,479
we don't know

116
00:04:43,680 --> 00:04:48,639
what is inside we it may poison our

117
00:04:46,479 --> 00:04:50,720
platform it may deliver some malware

118
00:04:48,639 --> 00:04:52,479
in it may deliver some vulnerabilities

119
00:04:50,720 --> 00:04:54,560
that we cannot fix and we cannot do

120
00:04:52,479 --> 00:04:57,280
anything with that

121
00:04:54,560 --> 00:04:58,960
the term blob came from really cool

122
00:04:57,280 --> 00:05:02,160
movie you should watch that

123
00:04:58,960 --> 00:05:05,120
uh it's 1958 uh science

124
00:05:02,160 --> 00:05:06,000
science fiction horror about growing

125
00:05:05,120 --> 00:05:09,120
corrosive

126
00:05:06,000 --> 00:05:11,039
alien ammo boy doll

127
00:05:09,120 --> 00:05:12,160
entity that they're just eating the

128
00:05:11,039 --> 00:05:14,639
earth uh

129
00:05:12,160 --> 00:05:15,600
and they're just growing and growing

130
00:05:14,639 --> 00:05:18,000
because

131
00:05:15,600 --> 00:05:19,039
blobs are like that are we don't know

132
00:05:18,000 --> 00:05:22,160
what it is

133
00:05:19,039 --> 00:05:24,160
it it it's something that grows in our

134
00:05:22,160 --> 00:05:26,479
system we don't like blobs we want

135
00:05:24,160 --> 00:05:27,840
to eliminate it because if we will leave

136
00:05:26,479 --> 00:05:31,199
even one small piece

137
00:05:27,840 --> 00:05:34,240
it will grow again so

138
00:05:31,199 --> 00:05:35,440
fsp and aegis are considered binary

139
00:05:34,240 --> 00:05:38,800
blobs because

140
00:05:35,440 --> 00:05:39,199
those are delivered in that form and um

141
00:05:38,800 --> 00:05:41,039
yeah

142
00:05:39,199 --> 00:05:43,039
and and open source firmware community

143
00:05:41,039 --> 00:05:44,479
believes that this is damaging to

144
00:05:43,039 --> 00:05:47,280
firmware industry

145
00:05:44,479 --> 00:05:49,680
because like you know it's closed and we

146
00:05:47,280 --> 00:05:53,600
cannot do anything with that

147
00:05:49,680 --> 00:05:54,160
um next term that may be important and

148
00:05:53,600 --> 00:05:55,600
maybe

149
00:05:54,160 --> 00:05:57,600
confuse it like what's the difference

150
00:05:55,600 --> 00:06:00,400
between core boot and libre boot

151
00:05:57,600 --> 00:06:00,720
so libra boot is a fork or of core boot

152
00:06:00,400 --> 00:06:02,720
or

153
00:06:00,720 --> 00:06:04,080
if we can call that fork because not it

154
00:06:02,720 --> 00:06:07,120
is not exactly co

155
00:06:04,080 --> 00:06:10,720
fork it is just coreboot plus

156
00:06:07,120 --> 00:06:13,919
um additional scripts which can develop

157
00:06:10,720 --> 00:06:17,360
uh coreboot the motivation is that

158
00:06:13,919 --> 00:06:20,960
open source fails to promote freedom and

159
00:06:17,360 --> 00:06:23,039
that the open source projects just um

160
00:06:20,960 --> 00:06:24,260
like like core booth didn't provide

161
00:06:23,039 --> 00:06:25,840
enough um

162
00:06:24,260 --> 00:06:30,000
[Music]

163
00:06:25,840 --> 00:06:32,960
protection for freedom uh defined by the

164
00:06:30,000 --> 00:06:34,800
free software foundation and defined in

165
00:06:32,960 --> 00:06:37,919
day in their terms

166
00:06:34,800 --> 00:06:41,360
and because corbot accepted um

167
00:06:37,919 --> 00:06:44,800
blobs uh to be included in the

168
00:06:41,360 --> 00:06:45,360
in the final components just to make

169
00:06:44,800 --> 00:06:48,240
sure that

170
00:06:45,360 --> 00:06:48,800
more platforms will boot with core boot

171
00:06:48,240 --> 00:06:51,360
and

172
00:06:48,800 --> 00:06:53,440
and libre boots questioned that practice

173
00:06:51,360 --> 00:06:55,599
and they just wanted to fork

174
00:06:53,440 --> 00:06:56,639
but of course the project didn't came

175
00:06:55,599 --> 00:06:59,039
very well

176
00:06:56,639 --> 00:07:01,759
um we will see what will happen with it

177
00:06:59,039 --> 00:07:01,759
in the future

