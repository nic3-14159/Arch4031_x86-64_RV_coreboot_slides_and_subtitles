1
00:00:00,84 --> 00:00:01,49
Okay,

2
00:00:01,49 --> 00:00:02,85
let's talk about code review

3
00:00:04,34 --> 00:00:07,46
So during contribute can your changes are graded

4
00:00:08,24 --> 00:00:09,55
Bye Bye,

5
00:00:09,55 --> 00:00:10,36
Reviewers

6
00:00:10,74 --> 00:00:11,11
Uh,

7
00:00:11,12 --> 00:00:15,86
minus two is reserved at great for developers

8
00:00:15,87 --> 00:00:17,9
It means definitely don't merge

9
00:00:17,91 --> 00:00:20,62
So it means that if there is some serious problem

10
00:00:20,62 --> 00:00:23,42
with departure patch set,

11
00:00:23,43 --> 00:00:26,78
either things are very low quality

12
00:00:26,78 --> 00:00:29,84
Either are made in wrong way either

13
00:00:29,85 --> 00:00:32,51
Maybe they are incorrect

14
00:00:32,52 --> 00:00:37,16
And not according to Corbett Project vision

15
00:00:37,17 --> 00:00:37,98
Uh,

16
00:00:37,99 --> 00:00:43,37
so those are like serious programs which probably require a

17
00:00:43,37 --> 00:00:48,45
lot of work to be improved or unblocking something on

18
00:00:48,45 --> 00:00:50,3
the vision

19
00:00:50,3 --> 00:00:54,17
Strategy level minus one means I prefer you want to

20
00:00:54,17 --> 00:00:54,82
merge it

21
00:00:54,83 --> 00:00:55,46
That means,

22
00:00:55,46 --> 00:00:58,14
like there are some improvements needed

23
00:00:58,15 --> 00:01:00,26
Uh,

24
00:01:00,27 --> 00:01:01,17
this is like,

25
00:01:01,18 --> 00:01:04,86
I just don't want this to be managed because this

26
00:01:04,86 --> 00:01:05,65
is low quality

27
00:01:06,04 --> 00:01:07,4
Zero is neutral,

28
00:01:07,4 --> 00:01:09,21
plus one means looks good

29
00:01:09,22 --> 00:01:11,97
But someone else should check it

30
00:01:11,98 --> 00:01:12,95
For example,

31
00:01:12,95 --> 00:01:17,59
we use plus one if we review changes of our

32
00:01:17,6 --> 00:01:18,81
company members

33
00:01:18,82 --> 00:01:22,77
So we never give plus two for departures

34
00:01:22,78 --> 00:01:24,46
That coming from three in depth,

35
00:01:25,04 --> 00:01:25,77
um,

36
00:01:25,78 --> 00:01:28,8
plus two means please murdered

37
00:01:28,81 --> 00:01:34,31
And when the maintainers of Corbett will go through the

38
00:01:34,32 --> 00:01:35,73
changes and we'll see,

39
00:01:35,73 --> 00:01:36,57
plus two,

40
00:01:36,58 --> 00:01:36,85
uh,

41
00:01:36,86 --> 00:01:40,56
this will be would be merged There are some rules

42
00:01:40,56 --> 00:01:42,76
related to plus two Great

43
00:01:43,24 --> 00:01:48,02
Which the plus two should be never gave to your

44
00:01:48,03 --> 00:01:49,28
own change

45
00:01:49,29 --> 00:01:50,77
If you give plus two,

46
00:01:50,78 --> 00:01:52,81
then you are responsible for those changes

47
00:01:52,81 --> 00:01:53,72
So if something,

48
00:01:53,73 --> 00:01:56,07
if this change will break something,

49
00:01:56,08 --> 00:01:58,83
then you are expected to be involved in fixing the

50
00:01:58,83 --> 00:01:59,35
things

51
00:02:00,64 --> 00:02:01,45
Um,

52
00:02:02,34 --> 00:02:03,11
of course

53
00:02:03,12 --> 00:02:04,05
Uh,

54
00:02:04,44 --> 00:02:05,24
uh huh

55
00:02:05,94 --> 00:02:06,45
You know,

56
00:02:06,45 --> 00:02:08,62
like you should wait for for the review

57
00:02:08,62 --> 00:02:11,58
It's not like you will get us to like in

58
00:02:11,58 --> 00:02:13,03
24 hours

59
00:02:13,04 --> 00:02:14,35
Typically,

60
00:02:14,36 --> 00:02:17,94
developers from all around the world should see the change

61
00:02:17,95 --> 00:02:18,93
If it is simple,

62
00:02:18,93 --> 00:02:21,71
it's probably easier if it is long and complex

63
00:02:21,71 --> 00:02:23,06
It may take a lot,

64
00:02:23,44 --> 00:02:23,91
uh,

65
00:02:23,92 --> 00:02:28,41
and the waiting period and time that you wait to

66
00:02:28,42 --> 00:02:32,34
pink to ask when I can get answer should be

67
00:02:32,34 --> 00:02:34,19
longer for bigger changes

68
00:02:34,2 --> 00:02:37,36
This is similar to any open source product,

69
00:02:37,94 --> 00:02:43,26
and you should not expect anyone to review your changes

70
00:02:43,35 --> 00:02:44,16
Um,

71
00:02:44,84 --> 00:02:47,05
you should ask for this review

72
00:02:47,06 --> 00:02:50,01
It's there is a script

73
00:02:50,01 --> 00:02:51,23
Get maintainers

74
00:02:51,23 --> 00:02:53,06
You can easily also check who,

75
00:02:53,44 --> 00:02:54,45
uh,

76
00:02:54,46 --> 00:02:56,68
modified the same region of code

77
00:02:56,69 --> 00:02:57,58
Uh,

78
00:02:57,59 --> 00:03:01,45
in the past month there is in maintenance file

79
00:03:01,46 --> 00:03:06,04
You can also check who's responsible for given section for

80
00:03:06,05 --> 00:03:13,63
Given Assoc Forgiven module Forgiven platform That's that's the basic

81
00:03:13,63 --> 00:03:14,76
way to verify

82
00:03:15,24 --> 00:03:15,73
Um,

83
00:03:15,74 --> 00:03:18,11
if you look for reviewers,

84
00:03:18,11 --> 00:03:20,94
you can always ask on IRC on slack

85
00:03:20,95 --> 00:03:21,45
Um,

86
00:03:21,46 --> 00:03:23,7
and then if you for a long time not getting

87
00:03:23,7 --> 00:03:24,36
any response,

88
00:03:24,36 --> 00:03:26,85
you can go to my linguist and ask,

89
00:03:27,24 --> 00:03:28,45
Hey,

90
00:03:28,46 --> 00:03:30,58
can you take a look at these changes?

91
00:03:30,59 --> 00:03:32,72
It's here for a long time,

92
00:03:32,73 --> 00:03:33,45
um,

93
00:03:33,93 --> 00:03:37,31
and and explain why it would be great to marriage

94
00:03:37,31 --> 00:03:37,86
disco

