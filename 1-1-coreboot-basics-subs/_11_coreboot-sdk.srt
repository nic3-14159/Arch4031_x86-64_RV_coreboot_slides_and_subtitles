1
00:00:00,34 --> 00:00:02,12
in next practice exercise

2
00:00:02,12 --> 00:00:02,78
We will,

3
00:00:02,79 --> 00:00:04,3
um first of all,

4
00:00:04,3 --> 00:00:06,37
check if doctor is configured

5
00:00:06,38 --> 00:00:09,21
We will check what kind of images we already have

6
00:00:09,22 --> 00:00:10,32
downloaded

7
00:00:10,33 --> 00:00:13,01
We should have at least the young if we had

8
00:00:13,02 --> 00:00:14,95
clear Dr Installation,

9
00:00:15,64 --> 00:00:20,16
we may pull core boot sdk,

10
00:00:20,17 --> 00:00:24,64
which is official car boot docker container with the tulle

11
00:00:24,64 --> 00:00:25,46
train

12
00:00:25,47 --> 00:00:26,88
There are some problems with it

13
00:00:26,89 --> 00:00:29,46
I will describe that in the next slide

14
00:00:29,94 --> 00:00:33,45
But for this training we will use three and Corr

15
00:00:33,45 --> 00:00:34,89
book training SDK,

16
00:00:34,9 --> 00:00:40,86
which is already published inversion for 12.1 on the Docker

17
00:00:40,86 --> 00:00:41,25
hub

18
00:00:41,64 --> 00:00:43,69
What's the versioning scheme?

19
00:00:43,7 --> 00:00:49,19
So the problem is a corporate sdk lost versioning scheme

20
00:00:49,19 --> 00:00:50,69
scheme at some point

21
00:00:50,7 --> 00:00:51,25
Uh,

22
00:00:51,26 --> 00:00:54,53
this is probably some in our maintain ability problem,

23
00:00:54,54 --> 00:00:55,78
but for us,

24
00:00:55,78 --> 00:00:57,49
we would like to keep,

25
00:00:57,5 --> 00:00:58,14
uh,

26
00:00:58,15 --> 00:01:03,99
the versioning scheme of our training sdk as containing three

27
00:01:03,99 --> 00:01:06,21
numbers a major version of core,

28
00:01:06,21 --> 00:01:08,05
but as a first number,

29
00:01:08,24 --> 00:01:12,2
second number minor version of core boot and third number

30
00:01:12,2 --> 00:01:14,07
corporate training sdk patch,

31
00:01:14,08 --> 00:01:18,35
which we will increase with the changes to the docker

32
00:01:18,35 --> 00:01:19,06
container

33
00:01:19,64 --> 00:01:22,36
All the versions will be available on,

34
00:01:22,74 --> 00:01:23,23
um,

35
00:01:23,24 --> 00:01:24,41
Dr Hub,

36
00:01:24,42 --> 00:01:27,53
linked to the docker hub you may find under this

37
00:01:27,54 --> 00:01:32,4
video and linked to official Corbett sdk also will be

38
00:01:32,4 --> 00:01:32,76
there

