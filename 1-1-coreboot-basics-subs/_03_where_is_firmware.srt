1
00:00:00,64 --> 00:00:02,28
then we can ask ourselves

2
00:00:02,29 --> 00:00:02,73
Okay,

3
00:00:02,73 --> 00:00:05,33
so where is where is former like?

4
00:00:05,34 --> 00:00:05,62
OK,

5
00:00:05,62 --> 00:00:09,0
we know all these farmers or these terms where this

6
00:00:09,0 --> 00:00:11,03
film is to be honest

7
00:00:11,04 --> 00:00:11,64
Uh,

8
00:00:11,65 --> 00:00:13,53
here on this picture we have,

9
00:00:13,54 --> 00:00:18,19
um we have a modern architecture of computer

10
00:00:18,19 --> 00:00:20,28
We have two processors of intel

11
00:00:20,28 --> 00:00:21,12
Haswell,

12
00:00:21,13 --> 00:00:22,74
We have PC

13
00:00:22,74 --> 00:00:24,31
I devices

14
00:00:24,31 --> 00:00:26,15
We have many things here we have,

15
00:00:26,16 --> 00:00:30,24
um we have sold bridge or PCH

16
00:00:30,25 --> 00:00:31,22
Um,

17
00:00:31,23 --> 00:00:33,05
collecting These days,

18
00:00:33,06 --> 00:00:39,96
we have BMC from a speed here and and we

19
00:00:39,96 --> 00:00:40,65
ask ourselves,

20
00:00:40,65 --> 00:00:40,89
Okay,

21
00:00:40,89 --> 00:00:41,68
where is Filmer?

22
00:00:41,68 --> 00:00:42,4
And it happens

23
00:00:42,4 --> 00:00:43,9
The filmer is everywhere

24
00:00:43,91 --> 00:00:45,4
So we have microcode,

25
00:00:45,4 --> 00:00:48,43
which is some kind of type of humor which,

26
00:00:48,44 --> 00:00:51,68
which is inside CPU and inside,

27
00:00:51,69 --> 00:00:52,49
uh,

28
00:00:52,5 --> 00:00:53,33
CPU chip

29
00:00:53,34 --> 00:00:55,69
There may be more other,

30
00:00:55,7 --> 00:00:57,38
like internal chips,

31
00:00:57,38 --> 00:00:59,06
which runs some humor

32
00:00:59,31 --> 00:01:01,35
Then we have all these PC devices,

33
00:01:01,35 --> 00:01:04,66
which by itself also can contain humor

34
00:01:04,67 --> 00:01:11,5
We have network controllers which also contain some humor

35
00:01:11,51 --> 00:01:14,74
Then we have PCH where we have something like management

36
00:01:14,74 --> 00:01:15,22
energy,

37
00:01:15,23 --> 00:01:17,98
which also contains some form of humor

38
00:01:17,98 --> 00:01:20,15
Some artists which is running there

39
00:01:20,84 --> 00:01:25,45
We have hard disks which contains some controllers,

40
00:01:25,45 --> 00:01:27,07
which contains some firmer

41
00:01:27,08 --> 00:01:29,56
We have USB devices which by itself,

42
00:01:29,56 --> 00:01:32,26
to serve some functions contain former

43
00:01:32,27 --> 00:01:34,04
We have TPM here,

44
00:01:34,05 --> 00:01:39,02
which also contains some former two provides it provides its

45
00:01:39,02 --> 00:01:39,76
functions

46
00:01:39,84 --> 00:01:41,12
We have bias,

47
00:01:41,13 --> 00:01:43,3
We just former itself,

48
00:01:43,3 --> 00:01:45,15
which we will discuss in this training

49
00:01:45,34 --> 00:01:47,9
We have open BMC the same stuff

50
00:01:47,91 --> 00:01:49,41
This is some form of former

51
00:01:49,42 --> 00:01:52,43
So these days filmer is everywhere,

52
00:01:52,44 --> 00:01:53,19
and,

53
00:01:53,2 --> 00:01:54,03
uh,

54
00:01:54,04 --> 00:01:57,44
it's good to know what we're dealing with and learn

55
00:01:57,44 --> 00:01:57,96
about it

