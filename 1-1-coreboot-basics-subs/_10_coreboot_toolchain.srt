1
00:00:00,44 --> 00:00:05,97
So let's talk about corporate torturing so called project use

2
00:00:05,97 --> 00:00:12,2
very specific toolchain with exact version of software of GCC

3
00:00:12,2 --> 00:00:15,06
of libraries of DDB,

4
00:00:15,94 --> 00:00:19,53
and this tool chain is built as a part of

5
00:00:19,54 --> 00:00:20,86
um of the project

6
00:00:21,74 --> 00:00:26,4
A corporate project maintains couple patches on top of toolchain

7
00:00:26,4 --> 00:00:27,16
components

8
00:00:28,14 --> 00:00:29,62
But to be honest,

9
00:00:29,62 --> 00:00:30,66
like you may ask,

10
00:00:30,67 --> 00:00:31,19
okay,

11
00:00:31,2 --> 00:00:33,08
but on in our distribution,

12
00:00:33,08 --> 00:00:34,41
I can install GCC,

13
00:00:34,41 --> 00:00:34,56
g,

14
00:00:34,56 --> 00:00:34,71
D

15
00:00:34,71 --> 00:00:35,32
B,

16
00:00:35,33 --> 00:00:36,9
and I have Lipsey

17
00:00:36,91 --> 00:00:39,66
So why I need another tool chain

18
00:00:40,87 --> 00:00:42,47
This is because first of all,

19
00:00:42,47 --> 00:00:44,19
corporate project want to be independent,

20
00:00:44,19 --> 00:00:44,96
independent,

21
00:00:45,34 --> 00:00:47,09
and it happened in the past

22
00:00:47,1 --> 00:00:48,45
Many times there are values

23
00:00:48,45 --> 00:00:56,29
Distributions provide broken tool chains or incompatible two chainz people

24
00:00:56,29 --> 00:00:57,75
that create tool chains

25
00:00:57,76 --> 00:00:59,03
In general,

26
00:00:59,03 --> 00:01:03,11
proposed distributions make a lot of assumptions about how to

27
00:01:03,11 --> 00:01:04,45
change will be used,

28
00:01:04,46 --> 00:01:08,46
and typically they do not consider open source firmware development

29
00:01:08,46 --> 00:01:10,26
As a main use case,

30
00:01:10,34 --> 00:01:16,06
they typically creator change for developing under the their distribution

31
00:01:16,94 --> 00:01:17,82
Also,

32
00:01:17,83 --> 00:01:18,88
um,

33
00:01:18,89 --> 00:01:23,43
Corbett products want to give ability to be built on

34
00:01:23,43 --> 00:01:25,27
values or essence,

35
00:01:25,28 --> 00:01:26,87
or SS and distrust,

36
00:01:26,88 --> 00:01:30,05
including BSD or or sequins

37
00:01:30,64 --> 00:01:32,08
Also,

38
00:01:32,09 --> 00:01:35,19
Corbyn supports multiple CPU architectures

39
00:01:35,19 --> 00:01:41,6
Watch what may be different in comparison to approach of

40
00:01:41,61 --> 00:01:46,91
torching the developed and distributed by Lennox distrust or values

41
00:01:46,92 --> 00:01:48,36
other providers

42
00:01:49,14 --> 00:01:54,09
And because of that car but gives ability to build

43
00:01:54,09 --> 00:01:56,88
toolchain It's mandatory by the by the way,

44
00:01:56,89 --> 00:01:57,37
um,

45
00:01:57,38 --> 00:02:00,11
it requires quite a lot of time

46
00:02:00,12 --> 00:02:01,65
And because of that,

47
00:02:01,66 --> 00:02:04,14
we use Docker to deliver the,

48
00:02:04,15 --> 00:02:07,67
uh we to deliver this toolchain,

49
00:02:07,68 --> 00:02:08,69
uh,

50
00:02:08,7 --> 00:02:12,76
and not wait hours for building the Trojan on our

51
00:02:13,04 --> 00:02:14,06
A computer

52
00:02:15,04 --> 00:02:17,6
So if you will run,

53
00:02:17,6 --> 00:02:19,99
make with help,

54
00:02:20,0 --> 00:02:21,5
then you will see,

55
00:02:21,51 --> 00:02:22,1
um,

56
00:02:22,11 --> 00:02:25,05
a couple ways of building tool train you might build

57
00:02:25,05 --> 00:02:27,16
for all CPU architectures

58
00:02:27,16 --> 00:02:30,43
You might be built only for 32 bits for 64

59
00:02:30,43 --> 00:02:32,06
bits for power,

60
00:02:32,06 --> 00:02:33,46
for for risk

61
00:02:34,15 --> 00:02:35,15
It depends

62
00:02:35,15 --> 00:02:36,66
What is your target?

63
00:02:37,04 --> 00:02:38,46
This procedure,

64
00:02:38,46 --> 00:02:38,94
of course,

65
00:02:38,94 --> 00:02:40,46
is very time consuming

66
00:02:40,46 --> 00:02:42,85
It may fail because of various reasons

67
00:02:42,86 --> 00:02:43,82
Uh,

68
00:02:43,83 --> 00:02:46,66
those two chains are tested by corporate projects,

69
00:02:46,66 --> 00:02:50,93
but sometimes it happened that there are box because someone

70
00:02:50,94 --> 00:02:52,85
didn't use that for a long time

71
00:02:53,44 --> 00:02:53,74
And,

72
00:02:53,74 --> 00:02:54,18
of course,

73
00:02:54,18 --> 00:03:00,59
to proceed with that you best best would be to

74
00:03:00,59 --> 00:03:04,16
use docker container that we deliver deliver,

75
00:03:04,16 --> 00:03:07,54
which will be presented on further slides

76
00:03:07,55 --> 00:03:08,9
But first of all,

77
00:03:08,9 --> 00:03:12,45
you should have docker install it and ready to use

78
00:03:12,46 --> 00:03:13,22
of course,

79
00:03:13,23 --> 00:03:14,56
in previous practice,

80
00:03:14,74 --> 00:03:15,66
um,

81
00:03:16,74 --> 00:03:17,48
exercise

82
00:03:17,48 --> 00:03:18,73
We already use doctors,

83
00:03:18,73 --> 00:03:22,56
so I believe everything is in the place and should

84
00:03:22,56 --> 00:03:23,06
work

