1
00:00:00,399 --> 00:00:04,160
so let's try to answer some very very

2
00:00:03,199 --> 00:00:07,359
basic question

3
00:00:04,160 --> 00:00:08,480
what is core boot so core boot uh by

4
00:00:07,359 --> 00:00:10,639
website definition

5
00:00:08,480 --> 00:00:11,599
is a extended firmware platform that

6
00:00:10,639 --> 00:00:13,360
delivers

7
00:00:11,599 --> 00:00:15,519
lightning fast and secure boot

8
00:00:13,360 --> 00:00:16,880
experience on motor computers and

9
00:00:15,519 --> 00:00:19,680
embedded systems

10
00:00:16,880 --> 00:00:20,720
um it's an open source project uh it

11
00:00:19,680 --> 00:00:23,680
provides audible

12
00:00:20,720 --> 00:00:24,880
auditability and control over the

13
00:00:23,680 --> 00:00:29,039
technology

14
00:00:24,880 --> 00:00:32,079
so uh in general it is not

15
00:00:29,039 --> 00:00:34,800
a firmware per se it's more like a

16
00:00:32,079 --> 00:00:36,480
firmware framework or extensible

17
00:00:34,800 --> 00:00:40,320
firmware platform

18
00:00:36,480 --> 00:00:43,440
which can give you some um some

19
00:00:40,320 --> 00:00:46,399
features like very fast boots some

20
00:00:43,440 --> 00:00:47,039
auditability because it is open source

21
00:00:46,399 --> 00:00:50,480
it has

22
00:00:47,039 --> 00:00:51,280
it has many secure feature features

23
00:00:50,480 --> 00:00:54,239
which were

24
00:00:51,280 --> 00:00:56,879
um liked by many vendors which i will

25
00:00:54,239 --> 00:01:00,480
explain on next slides

26
00:00:56,879 --> 00:01:01,440
so core boot was created in 1999 by ron

27
00:01:00,480 --> 00:01:03,840
minich

28
00:01:01,440 --> 00:01:05,840
uh in the same year by the way ubud was

29
00:01:03,840 --> 00:01:10,240
created so it's interesting

30
00:01:05,840 --> 00:01:12,720
coincident uh motivation of ron

31
00:01:10,240 --> 00:01:13,520
with whose picture we have here on the

32
00:01:12,720 --> 00:01:16,799
slide

33
00:01:13,520 --> 00:01:17,759
was um why i have to press f1 on all

34
00:01:16,799 --> 00:01:20,880
these pcs

35
00:01:17,759 --> 00:01:24,000
in cluster that i'm trying to build when

36
00:01:20,880 --> 00:01:26,240
ron worked in los alamos laboratory

37
00:01:24,000 --> 00:01:28,320
he just have to run with the keyboard

38
00:01:26,240 --> 00:01:31,520
and push f1 to be

39
00:01:28,320 --> 00:01:34,560
put the cluster so he decide he will

40
00:01:31,520 --> 00:01:37,920
just hack it and get rid of this f1

41
00:01:34,560 --> 00:01:40,479
but the project quickly grow up and

42
00:01:37,920 --> 00:01:42,159
it happened it's he started to create

43
00:01:40,479 --> 00:01:44,880
something called linux boot and

44
00:01:42,159 --> 00:01:45,280
which was then converted to core boot or

45
00:01:44,880 --> 00:01:48,320
this

46
00:01:45,280 --> 00:01:49,040
key story i will explain to you corbett

47
00:01:48,320 --> 00:01:52,159
supports

48
00:01:49,040 --> 00:01:56,000
many architecture architectures

49
00:01:52,159 --> 00:01:58,079
arm arm64 up to version 411

50
00:01:56,000 --> 00:01:59,840
there was support for meeps but

51
00:01:58,079 --> 00:02:01,920
unfortunately um there are no

52
00:01:59,840 --> 00:02:04,159
maintainers for mips that's why

53
00:02:01,920 --> 00:02:05,200
the platform was removed there is power

54
00:02:04,159 --> 00:02:08,959
pc support

55
00:02:05,200 --> 00:02:12,080
which is very like small risk five

56
00:02:08,959 --> 00:02:14,000
uh corbett was one of the first open

57
00:02:12,080 --> 00:02:17,040
source firmware projects to support

58
00:02:14,000 --> 00:02:19,840
risk five and of course x86

59
00:02:17,040 --> 00:02:22,480
recently 3m dev started work on

60
00:02:19,840 --> 00:02:24,879
supporting power nine which

61
00:02:22,480 --> 00:02:27,040
which will add even more architectures

62
00:02:24,879 --> 00:02:29,440
more modern architectures to

63
00:02:27,040 --> 00:02:31,200
um to the bucket uh please note that

64
00:02:29,440 --> 00:02:32,959
core boot is always written

65
00:02:31,200 --> 00:02:34,840
with small letters starting with small

66
00:02:32,959 --> 00:02:36,959
letter even at the beginning of the

67
00:02:34,840 --> 00:02:40,239
sentence

68
00:02:36,959 --> 00:02:42,319
so the the idea behind core boot is

69
00:02:40,239 --> 00:02:45,440
single responsibility principle

70
00:02:42,319 --> 00:02:46,239
so it tries to do one small thing just

71
00:02:45,440 --> 00:02:48,400
the hardware

72
00:02:46,239 --> 00:02:49,280
initialization for given hardware

73
00:02:48,400 --> 00:02:52,160
platform

74
00:02:49,280 --> 00:02:52,800
um but of course this is uh this is

75
00:02:52,160 --> 00:02:56,080
complex

76
00:02:52,800 --> 00:02:58,560
and this is hard to keep um but

77
00:02:56,080 --> 00:03:00,959
after this initialization is done core

78
00:02:58,560 --> 00:03:04,080
boot assumes it's uh it's over

79
00:03:00,959 --> 00:03:05,360
and it can take like move responsibility

80
00:03:04,080 --> 00:03:08,560
to someone else which

81
00:03:05,360 --> 00:03:10,159
typically should be in payload um

82
00:03:08,560 --> 00:03:13,040
and of course we'll explain what that

83
00:03:10,159 --> 00:03:15,519
means in in further slides

84
00:03:13,040 --> 00:03:17,040
so there is some many people ask what's

85
00:03:15,519 --> 00:03:19,280
the difference with ufi

86
00:03:17,040 --> 00:03:21,040
um so you first of all ufi is just the

87
00:03:19,280 --> 00:03:24,640
specification which i will explain

88
00:03:21,040 --> 00:03:27,040
um but many people call ufi

89
00:03:24,640 --> 00:03:28,319
some implementation or some bios which

90
00:03:27,040 --> 00:03:32,080
is already on on some

91
00:03:28,319 --> 00:03:36,319
on on some platform or they mean edk2

92
00:03:32,080 --> 00:03:40,000
reference implementation so first of all

93
00:03:36,319 --> 00:03:43,360
core boot builds ready to boot

94
00:03:40,000 --> 00:03:46,400
image ready to boot bios

95
00:03:43,360 --> 00:03:48,879
which you can flash into your spy and

96
00:03:46,400 --> 00:03:51,120
boot the platform this is not the case

97
00:03:48,879 --> 00:03:52,480
of ufi ufi it's just a framework even

98
00:03:51,120 --> 00:03:54,720
edk2

99
00:03:52,480 --> 00:03:55,840
miss many co many components to build

100
00:03:54,720 --> 00:03:58,159
the platform

101
00:03:55,840 --> 00:03:59,760
second thing uh coreboot right now

102
00:03:58,159 --> 00:04:03,120
supports around like

103
00:03:59,760 --> 00:04:03,920
300 boards uh from from more than 70

104
00:04:03,120 --> 00:04:06,959
vendors

105
00:04:03,920 --> 00:04:08,560
so um we're just building flashing and

106
00:04:06,959 --> 00:04:10,239
and booting core boot

107
00:04:08,560 --> 00:04:12,480
on those platforms and hopefully we will

108
00:04:10,239 --> 00:04:15,120
see more and more platforms there

109
00:04:12,480 --> 00:04:16,639
um coreboot has different target than

110
00:04:15,120 --> 00:04:19,840
for example ubud

111
00:04:16,639 --> 00:04:22,720
um it is interested more in in server

112
00:04:19,840 --> 00:04:24,240
that desktops and of course sometimes

113
00:04:22,720 --> 00:04:26,720
embedded systems but

114
00:04:24,240 --> 00:04:28,320
uh this is not the main main goal i

115
00:04:26,720 --> 00:04:30,960
believe of course like of course

116
00:04:28,320 --> 00:04:31,360
of course embedded is supported but you

117
00:04:30,960 --> 00:04:34,479
boot

118
00:04:31,360 --> 00:04:36,639
is more in small arm

119
00:04:34,479 --> 00:04:38,479
environments more popular more major

120
00:04:36,639 --> 00:04:40,240
there

121
00:04:38,479 --> 00:04:43,199
and of course we will explain

122
00:04:40,240 --> 00:04:46,560
differences between various

123
00:04:43,199 --> 00:04:49,360
firmware open source firmware projects

124
00:04:46,560 --> 00:04:49,360
in this presentation

