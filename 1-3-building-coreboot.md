# Makefile brief overview

* utilized by GNU make tool
* which output files (targets) to create
* which input files to take
* how to process them (set of rules)

```bash
target: input_file1 input_file2
        comm1
        comm2
        comm3
```

> TAB as indent !

For example `make help` in coreboot would execute this target:
https://github.com/coreboot/coreboot/blob/master/Makefile#L76



???

* Makefile is a file utilized by GNU `make` tool. In general it contains
  information how to produce output files from several input files. It can be
  used for a variety of tasks, but of course we will focus on source code
  compiling.

* Makefile basic syntax may already be familiar for you, but let's remind it for
  completeness anyway. On the left side we have output file (AKA target), after a
  colon we have list of input files (dependency list). Below there are set of
  rules describing how to produce target file out of input files. The important
  part to remember is to use tabulation as indent (not spaces).

---

# Makefile brief overview

* only one main Makefile
* multiple Makefile.inc
* other Makefiles for tools / libs / payloads

```{.bash}
./Makefile
./util/intelmetool/Makefile
./payloads/external/SeaBIOS/Makefile
./payloads/external/iPXE/Makefile
./payloads/external/U-Boot/Makefile
```

???
* There is only one main Makefile that lies within coreboot root directory.
* Apart from that, there are many include Makefile.inc files that lie in nearly
  each source tree subdirectory.
* There are also other separate Makefiles for building some utility tools,
  libs, payloads etc.

---

# Kconfig brief overview

* configuration tool developed for Linux kernel configuration
* configuration database
* used to select build-time configuration
* collection of options in tree structure
* adapted by other open source projects

???
* Kconfig is a configuration tool that was developed with Linux kernel project.
  It is configuration database used used to select build-time configuration for
  Linux kernel until now.

* Kconfig can be perceived as a collection of options, organized in a tree
  structure.  It has been adopted by some of the open source project to help
  with build configuration maintenance.

---

# Kconfig in coreboot

* built as a part of coreboot build process
* lies in `coreboot/util/kconfig`
* access through `make menuconfig`
* enable / disable configuration option
* assign a value (string) to configuration option
* select configuration option value (string) from list

Example options:

* mainboard, chipset, RAM / ROM size
* debugging options
* include specific payloads / binary blobs

???

* coreboot is one of the projects that takes advantage of Kconfig utility
* It is located at `coreboot/utils/kconfig`
* In coreboot it is built by coreboot build process
* Text user interface can be accessed by `make menuconfig` command. Here we can
  enable or disable configuration options, assign values to configuration
  options or select a string value from a list.
* In practice those option are used to select appropriate mainboard, chipset,
  RAM / ROM size, enable some debugging options etc.
  There are also options for determining which payloads and binaries to include
  in final ROM image.

---

# coreboot build system

Let's try out some basic commands related to Makefiles and Kconfig. As a quick
example let's invoke `make help` from coreboot directory.

> If you haven't cloned the coreboot repository yet, do it now.

```shell
git clone --recurse-submodules https://review.coreboot.org/coreboot.git
cd coreboot
make help
```

Example output of the command:

```
*** coreboot platform targets ***
  Use "make [target] V=1" for extra build debug information
  all                   - Build coreboot
  clean                 - Remove coreboot build artifacts
  distclean             - Remove build artifacts and config files
  doxygen               - Build doxygen documentation for coreboot
  doxyplatform          - Build doxygen documentation for the current platform
  sphinx                - Build sphinx documentation for coreboot
  sphinx-lint           - Build sphinx documenttion for coreboot with warnings as errors
...
```

???

Let's try out some basic commands related to Makefiles and Kconfig. As a quick
example let's invoke `make help` from coreboot directory. It will give us an
overview what parameter make may take in coreboot. In the output we may notice
the platform, Kconfig, toolchain and coreboot test targets.

---

# coreboot build system

Let's review the most important commands from building coreboot perspective:

* `make all` - Build coreboot
* `make clean` - Remove coreboot build artifacts
* `make distclean` - Remove build artifacts, config files and cloned payloads
* `make menuconfig` - Updates current config utilising a menu based program
* `make oldconfig` - Update current config utilising a provided .config as base
* `make olddefconfig` - Same as oldconfig, but with default answer to all new
  options
* `make defconfig` - New config with default answer to all options
* `make savedefconfig` - Save current config as ./defconfig (minimal config)
* `make crossgcc-ARCH` - Build cross-compiler for specific architecture
* `make crosstools-ARCH` - Build cross-compiler with GDB for specific
  architecture

ARCH can be "i386", "x64", "arm", "aarch64", "riscv", "ppc64", "nds32le"<br>
Use `make <target> CPUS=#` to build toolchain using multiple cores

---

# coreboot build system

Using docker to build coreboot (assuming you have cloned the coreboot
repository and you are in coreboot directory):

```
docker run -w /home/coreboot/coreboot/ -u root -it \
    -v $PWD:/home/coreboot/coreboot --rm  \
    3mdeb/coreboot-training-sdk:4.12.1 /bin/bash
```

> The original container is coreboot/coreboot-sdk

It will switch to docker container into bash where we can start with building
process.

???

Building cross toolchain is time consuming and may fail. It is recommended to
use docker container for such case. If you have an exotic environment you may
try to build the cross toolchain yourself or enable the `ANY_TOOLCHAIN` Kconfig
option if you're feeling lucky (no support in this case).

In order to avoid building the cross toolchain and save some time (and nerves)
a docker container is a recommended choice. We will not dig into why use
docker, because it was already explained in previous courses. Just execute the
command to switch into bash inside the container.

---

# coreboot menuconfig overview

Let's execute `make menuconfig` to open configuration menu program.
The coreboot menuconfig consist of few main categories:

* General setup
* Mainboard
* Chipset
* Devices
* Generic drivers
* Security
* Console
* System tables
* Payload
* Debugging

???

- General setup - mainly toolchain and blob settings, parsers.
- Mainboard - selection of the platform, ROM size and system power state
  (sometimes also some mainboard specific options).
- Chipset - here you may find all chipset and CPU configuration for your
  platform. CAR mode, microcode updates, silicon initialization blobs settings,
  etc.
- Devices - contains setting of the graphics and display initialization as well
  as PCI/PCI Express devices.
- Generic drivers - various coreboot drivers are located here, like EHCI debug
  dongle, event log, PS/2 keyboard initialization, VPD and certain Intel FSP
  blob integration settings.
- Security - this submenu contains all security related features available for
  platform and processors. Here you will find setting related to vboot, TPM,
  Intel TXT, STM, etc.
- Console - this submenu allows to configure the console for debugging
  coreboot, i.e. serial port configuration, cbmem and SPI flash console, post
  codes, debug level.
- System tables - here you may control generation of MP table, PIRQ table and
  SMBIOS tables.
- Payload - as name suggest this submenu is dedicated to payload selection and
  their corresponding settings.
- Debugging - contains various debugging options. Note soem will be available
  only when reasonably high debug level in console submenu will be enabled.

---

# configuring coreboot for QEMU

In order to build some base image (in this example for QEMU) one has to always
set those with each build to match target platform:

* Mainboard -> Mainboard vendor
* Mainboard -> Mainboard model
* Mainboard -> ROM chip size

For QEMU:

* Mainboard -> Mainboard vendor (Emulation)
* Mainboard -> Mainboard model (QEMU x86 q35/ich9)
* Mainboard -> ROM chip size (2048 KB (2MB))

We will build an image that will be executed on an emulated Intel Q35 chipset
(ICH9).

> NOTE: Always run `make distclean` before changing mainboards!

???

* We can point one the most important section in Kconfig menu which is
  `Mainboard` section. Usually, for standard builds, all we need to do is to
  select our mainboard vendor and model from menu in this section.

* Most of the time we can stick which default ROM chip size selection. However,
  some of the boards come with different flavors and selecting correct ROM chip
  size might be necessary.

* Below we can present mainboard settings for QEMU which will be used
  later in the next course.

* Now we do not have to do anything more, just navigate to the save button with
  TAB to save configuration file and the not the exit button to leave the menu.

---

# Kconfig output files

Two output files containing set of selected options: `.config` and `config.h`

`.config`

* located in root directory
* gets included by Makefile
* exposes CONFIG values to Makefile
* sets CONFIG symbols to be available by Makefiles during build

Sample content:

```{.bash}
CONFIG_COREBOOT_BUILD=y
CONFIG_LOCALVERSION=""
CONFIG_CBFS_PREFIX="fallback"
CONFIG_COMPILER_GCC=y
# CONFIG_COMPILER_LLVM_CLANG is not set
```

???

* Kconfig produces two important output files containing full set of options
  selected during configuration phase.

* The first file is `.config`. It is created and saved within coreboot root
  directory upon exiting from `menuconfig` text interface.

* `.config` file gets included by coreboot Makefile.

* It's purpose is to expose CONFIG options selected during configuration phase
  to Makefile, so the build system is aware what is the target configuration.
  Based on this information build system knows which source files should
  be built, which features should be enabled etc.

* As we can see in the example, boolean settings are set to 'y' or commented
  out with "is not set" string at the end. String settings can be set to any
  string of left empty ("")

---

# Kconfig output files

`config.h`

* `C` header file
* located in `build/config.h`
* included by all coreboot `C` files through `Makefile.inc`:
* exposes CONFIG values to source files
* created after executing `make` to build an image

Sample content:

```{.c}
#define CONFIG_SOC_AMD_COMMON 0
#define CONFIG_IOAPIC_INTERRUPTS_ON_FSB 1
#define CONFIG_BOOTBLOCK_SOURCE "bootblock_simple.c"
#define CONFIG_SMM_RESERVED_SIZE 0x100000
```

???

* Second file produced by the Kconfig utility is `config.h`.
* It is different from `.config` in it's format - it is a header C file.
* It serves similar role as `.config` file, but it exposes CONFIG selection to
  all source C files instead.
* In `config.h` boolean setting can be set either to 0 (when disabled) or 1
  (when enabled). There can be also some strings defined as well as addresses,
  defined as unsigned integers.

---

# Kconfig output files

* enable / configure some interfaces in the code

```{.c}
/* Initialize the RXD and TXD paths for UART0 */
if (CONFIG(ENABLE_BUILTIN_HSUART0)) {
    /* some GPIO mux code */
}
```

* check if some feature is available

```{.c}
#if CONFIG(HAVE_ACPI_RESUME)
  printk(BIOS_DEBUG,"Failed to recover CBMEM in S3\n");
  /* Failed S3 resume, reset to come up cleanly */
  reset_system();
#endif
```

???

* It can be used for build-time configuration of some features. On the first
  snippet we can see block of GPIO mux code that will be executed only if
  CONFIG_ENABLE_BUILTIN_HSUART0 was set in `menuconfig`. In this way we can we
  can decide whether we want some interface (e.g. UART) to be enabled.

* There can also be some code blocks that are compiled only if given platform
  has some feature available. In this case we check with preprocessor if
  `ACPI_RESUME` is supported.

---

# Makefile / Kconfig connection

Kconfig configuration options can decide:

* which is the board target
* board can come with different flavors (RAM, ROM)
* which source files are being build
* which features are enabled
* which binaries are included

???

* As presented earlier, Kconfig exposes CONFIG options via `.config` file to
  Makefile, which makes build system aware of those settings.
* Based on them it can decide: which is the target board, what's the size of
  RAM / ROM, which source files should be build and assembled, which features
  should be built and which binaries to include in final ROM image.

---

# Main Makefile

* includes Makefile.inc from paths in `subdirs` variable
* at first it only includes main Makefile.inc from root directory

```{.makefile}
TOPLEVEL := .
subdirs:=$(TOPLEVEL)
```

???

* How are those all Makefile.inc files included? The main file is looking for
  them in paths that are written into `subdirs` variable.
* At first this variable is defined as `subdirs:=.` which points to the root
  directory. There is only one Makefile.inc file (we can call it main
  Makefile.inc file) in root directory and it gets included by Makefile.

---

# subdirs-y class

* `-y` suffixed variables are called classes
* `subidirs-y` first declared in main Makefile.inc
* further appended in others Makefile.inc files
* extends `subdirs` content in Makefile

```{.makefile}
# root source directories of coreboot
subdirs-y := src/lib src/commonlib/ src/console src/device src/acpi
subdirs-y += src/ec/acpi $(wildcard src/ec/*/*) $(wildcard src/southbridge/*/*)
subdirs-y += $(wildcard src/soc/*/*) $(wildcard src/northbridge/*/*)
subdirs-y += src/superio
subdirs-y += $(wildcard src/drivers/*) $(wildcard src/drivers/*/*)
subdirs-y += src/cpu src/vendorcode
subdirs-y += util/cbfstool util/sconfig util/nvramtool util/broadcom
subdirs-y += util/futility util/marvell util/blobtool
subdirs-y += $(wildcard src/arch/*)
subdirs-y += src/mainboard/$(MAINBOARDDIR)
subdirs-y += src/vboot
subdirs-y += payloads payloads/external

subdirs-y += site-local
subdirs-y += util/checklist util/testing
```

???

* In coreboot Makefile system, `-y` suffixed variables are called classes.
* Main Makefile.inc declares a variable `subdirs-y`. Some of the paths present
  in `subdirs-y` class defined in main Makefile.inc are presented on the slide.
* The others Makefile.inc files use `+=` syntax to append paths to `subdirs-y`
  classes
* When Makefile.inc gets included, Makefile extends `subdirs` variable by
  `subdirs-y` content from included Makefile.inc. This means that path to look
  for Makefile.inc extends as well.
* `wildcard` is a function which takes a string with a wildcard as an argument.
  The output of this function is space separated list with names of existing
  files that match to the pattern which was passed as an argument.
  For example, the result of `$(wildcard src/arch/*)` is space separated list
  of all files found in `src/arch/` directory.

---

# Incremental include Makefile.inc

.image[.center[![Image](images/makefile.png)]]

???

* Other Makefiles.inc, laying deeper in the source tree, may extend `subdirs`
  variable as well. This leads to recursive process of including Makefile.inc
  file, extending search path and including another file, until `subdirs` list
  becomes empty.
* So to sum it up:
  - We start with including main Makefile.inc from root directory
  - `subdirs` is extended by `subdirs-y` from main Makefile.inc. It contains paths
    to other core directories which should be searched for Makefile.inc files
  - One of the paths is subtracted from `subdirs` variable and we search for
    Makefile.inc there. If it exists, it gets included. If it contains
    `subdirs-y`, it further extends `subdirs` variable.
  - The process finishes when `subdirs` variable becomes empty

---

# Source classes

Example of other important source classes: ramstage, romstage, bootblock. From
main Makefile.inc:

```{.makefile}
# Add source classes and their build options
classes-y := ramstage romstage bootblock postcar smm smmstub cpu_microcode verstage
```

Adding files to a class within other Makefile.inc:

```{.makefile}
ramstage-y += gpio.c
ramstage-y += irqroute.c
```

After build:

```{.bash}
build/ramstage
build/romstage
build/bootblock
```

???

* There are also other classes defined in Makefile.inc files. The most
  important could be: ramstage, romstage, bootblock. Their names correspond to
  coreboot boot stages to which they are related to.

coreboot Boot Stages:

  - bootblock - The reset vector and pre cache-as-RAM setup
  - romstage - Cache-as-RAM setup, early silicon initialization, memory setup
  - ramstage - Normal device setup and mainboard configuration
  - payload - The OS or application bootloader

* Makefile.inc files laying deeper in the source tree (for example in mainboard
  directory), usually adds some files that should be built to those classes. In
  this example we instruct build system to build gpio and irqroute as a part
  of ramstage block.

* After the build process finishes, we can see that output files are also split
  by a class. There are directories such as:

  - build/ramstage
  - build/romstage
  - build/bootblock

which contain compiled object files that were previously appended to `*-y`
class

---

# Makefile / Kconfig connection

One menuconfig selection can select many CONFIG options

`src/mainboard/emulation/qemu-q35/Kconfig`:

```{.bash}
if BOARD_EMULATION_QEMU_X86_Q35

config BOARD_SPECIFIC_OPTIONS
	def_bool y
	select CPU_QEMU_X86
	select SOUTHBRIDGE_INTEL_I82801IX
	select IOAPIC_INTERRUPTS_ON_APIC_SERIAL_BUS
	select HAVE_CMOS_DEFAULT
	select HAVE_OPTION_TABLE
...
```

???

When picking our mainboard in `menuconfig`, it doesn't end up with just one
CONFIG selection being changed. If we take a look at corresponding Kconfig file
(mainboard/emulation/qemu-q35/Kconfig) we can see a set of configuration option
and their values that are selected after this particular mainboard has been
picked. And those selected by the mainboard Kconfig select other dependent
options throughout whole coreboot source.

---

# Makefile / Kconfig connection

Makefile / Kconfig connection example:

Reminder from main Makefile.inc:

```{.makefile}
subdirs-y += src/mainboard/$(MAINBOARDDIR)
```

Only specific mainboard directory will be added to `subdirs-y` in main
Makefile.inc:

```{.makefile}
MAINBOARDDIR=$(call strip_quotes,$(CONFIG_MAINBOARD_DIR))
```

mainboard/emulation/qemu-q35/Kconfig:

```{.makefile}
config MAINBOARD_DIR
	string
	default "emulation/qemu-q35"
```

???

* This could be an example of Makefile / Kconfig connection mechanism.

* On previous slides we have shown `subdirs-y` declaration in main
  `Makefile.inc` file. Makefile.inc adds `src/mainboard/$(MAINBOARDDIR)` to
  `subdirs-y`.

* We can also see that MAINBOARDDIR variable comes from CONFIG_MAINBOARD_DIR
  which is selected by Kconfig when we set QEMU mainboard. This makes that
  only mainboard directory for this particular board is added to `subdirs`
  variable.

---

# Makefile / Kconfig connection

Take a look back at main Makefile.inc:

```
subdirs-y += $(wildcard src/soc/*/*) $(wildcard src/northbridge/*/*)
subdirs-y += $(wildcard src/drivers/*) $(wildcard src/drivers/*/*)
subdirs-y += $(wildcard src/arch/*)
```

Each Makefile.inc in soc / bridge etc. directory starts with `ifeq` check:

src/southbridge/intel/i82801ix/Makefile.inc:

```{.makefile}
ifeq ($(CONFIG_SOUTHBRIDGE_INTEL_I82801IX),y)

bootblock-y += bootblock.c
bootblock-y += early_init.c

romstage-y += dmi_setup.c
romstage-y += early_init.c
        ...
```

`CONFIG_SOUTHBRIDGE_INTEL_I82801IX` was selected when we set mainboard to QEMU
x86 q35/ich9.

???

* If we take a look back, we can see that main Makefile.inc adds all soc,
  northbridge, southbridge etc. directories to `subdirs-y`, regardless of
  chosen platform.

* But if we open one of the Makefile.inc files from those directories, we can
  see that there is appropriate `ifeq` check at the beginning of the file.  So
  it's content is only exposed to main Makefile if corresponding CONFIG option
  has been selected earlier, during build configuration phase.

* In such way only System on Chip/south-/northbridge specific code and options
  are included, i nsuch case it is Intel I82801IX southbridge.

---

# coreboot toolchain

Required to build coreboot binary and consists of:

* compilers (gcc, g++)
* binutils
* libncurses-dev
* iASL (ACPI Source Code Compiler & Disassembler)

???

coreboot toolchain is necessary to build coreboot binary image. This toolchain
consist of compilers, binutils package, ncurses-dev libs and ACPI compiler.

---

# coreboot toolchain

Why do we need it?

* to build for TARGET architecture
* to avoid incompatibility issues
* all coreboot developers use the same toolchain

Supported architectures: `i386`, `x64`, `arm`, `aarch64`, `riscv`, `power8`.

???

Why is toolchain necessary?

* First, we need to build for TARGET architecture, so cross-compilation may be
  often necessary.

* We also would like to avoid incompatibility issues due to some compiler
  setup.

* Also, compilers shipped with many distributions may not be compatible to
  perform a coreboot build. We all use the same build environment then so it
  should be easier to setup and resolve potential issues.

Currently, architectures supported by coreboot toolchain are: `i386`, `x64`,
`arm`, `aarch64`, `riscv`, `power8`

---

# coreboot toolchain building

- build all available toolchains:

```{.bash}
make crossgcc
```

- build toolchain for x86

```{.bash}
make crossgcc-i386
```

Or use docker container.

???

* We can build all available toolchains with one shot using `make crosgcc`
  command
* We can also build one specific toolchain. Currently, for x86 i386 toolchain
  is used, so usually it is enough to build this one.

---

# toolchain location

Typically the freshly built toolchain will be located here:

```{.bash}
ls  -1 util/crossgcc/xgcc/bin/
```

But inside docker you may find it in `/opt/xgcc/bin` (there will be lots of
files for various architectures).

```{.bash}
i386-elf-addr2line
i386-elf-ar
i386-elf-as
i386-elf-c++filt
i386-elf-cpp
i386-elf-dwp
i386-elf-elfedit
i386-elf-gcc
i386-elf-gcc-6.3.0
i386-elf-gcc-ar
i386-elf-gcc-nm
i386-elf-gcc-ranlib
i386-elf-gcov
...
```

???

After toolchain is build, it can be found at `utils/crossgcc/xgcc/bin`
directory. We can see some of the tools that are part of the toolchain on the
slide, but in fact the list is much longer.

---

# cross-compilation

.xcompile file:

* located in root directory after executing `make`
* contains paths to toolchain binaries (can be relative path if in container)
* sets build flags and variables
* gets included by main Makefile to set variables such as CC, CFLAGS, OBJDUMP
  etc.

```{.bash}
CPP_x86_32:=i386-elf-cpp
AS_x86_32:=i386-elf-as 
LD_x86_32:=i386-elf-ld.bfd 
ifeq ($(CONFIG_COMPILER_GCC)$(CONFIG_LP_COMPILER_GCC),y)
	NM_x86_32:=i386-elf-gcc-nm
	AR_x86_32:=i386-elf-gcc-ar
else
	NM_x86_32:=i386-elf-nm
	AR_x86_32:=i386-elf-ar
endif
OBJCOPY_x86_32:=i386-elf-objcopy
OBJDUMP_x86_32:=i386-elf-objdump
...
```

???
As we have said earlier, cross-compilation via coreboot toolchain is necessary.
Important part of this process is `.xcompile` file which lies in coreboot root
directory. It contains paths to toolchain binaries, as well as compilation
flags which are required in the build process. It is included in Makefile to
set variables like CC, CFLAGS, CPP, OBJCOPY, OBJDUMP etc.

---

# building coreboot for QEMU

We are now quite well prepared to start the build process. Simply type `make`
in the coreboot's root directory and observe how the files are compiled:

```
    CC         bootblock/lib/timestamp.o
    CC         bootblock/lib/version.o
    CC         bootblock/mainboard/emulation/qemu-q35/bootblock.o
    CC         bootblock/southbridge/intel/common/gpio.o
    CC         bootblock/southbridge/intel/common/pmbase.o
    CC         bootblock/southbridge/intel/common/reset.o
    CC         bootblock/southbridge/intel/common/rtc.o
    CC         bootblock/southbridge/intel/common/usb_debug.o
    CC         bootblock/southbridge/intel/i82801ix/bootblock.o
```

???

We are now quite well prepared to start the build process. Simply type `make`
in the coreboot's root directory and observe how the files are compiled. You
will surely notice the pattern of compiled files. THere are many files from
generic coreboot directories like `lib/*` and board specific directories:
`/mainboard/emulation/qemu-q35/*` and `southbridge/intel/i82801ix/*`. You also
probably noticed that each line starts with the name of stage it is compiled
for. In case of above output it is bootblock. Later the message will contain
romstage, postcar and ramstage, as well as messages about cloning and building
SeaBIOS payload.

---

# building coreboot for QEMU

At the end of build process, the build system will print the contents of
coreboot image (CBFS contents).

```
FMAP REGION: COREBOOT
Name                           Offset     Type           Size   Comp
cbfs master header             0x0        cbfs header        32 none
fallback/romstage              0x80       stage           15252 none
fallback/ramstage              0x3c80     stage           56925 none
config                         0x11b40    raw               104 none
revision                       0x11c00    raw               681 none
fallback/dsdt.aml              0x11f00    raw              6946 none
cmos_layout.bin                0x13a80    cmos_layout       676 none
fallback/postcar               0x13d80    stage           15768 none
fallback/payload               0x17b80    simple elf      69499 none
payload_config                 0x28b40    raw              1621 none
payload_revision               0x29200    raw               234 none
(empty)                        0x29340    null          1911384 none
bootblock                      0x1fbdc0   bootblock       16384 none
    HOSTCC     cbfstool/ifwitool.o
    HOSTCC     cbfstool/ifwitool (link)

Built emulation/qemu-q35 (QEMU x86 q35/ich9)
```

???

At the end of build process, the build system will print the contents of
coreboot image (CBFS contents). Here you may see that the stages have been put
inside the image. Depending on configuration and payload there may be other
files like VGA option ROMs, secondary payloads or configuration files like
bootorder for SeaBIOS. Bootblock will always be at the bottom of CBFS for x86
architecture, because of the reset vector being at the top of 4GB of physical
memory. The boot firmware is always mapped at the top of 4GB minus SPI flash
size. But what is CBFS?

---

# CBFS and cbfstool

coreboot File System

* not really a file system
* scheme for managing binary data in ROM image
* named objects in binary ROM image
* read only
* modified off-target by cbfstool
* no directory structure !

???
CBFS stands for coreboot File System. It has similar concepts to file system,
but in fact is not one of them.
It is rather a scheme for managing independent binary data ("files") located
in a single ROM image.

It is meant to be flashed on ROM chips. It's main property is that it is read
only. It shall be assembled off-target from some data chunks into single ROM
image. Then it can be flashed on target ROM chip. Utility that is capable of
assembling of such image is called `cbfstool` and will be discussed later.
Also, there is no such thing as directory structure, even when sometimes it may
look like it is.

---

# CBFS and cbfstool

Management utility for CBFS formatted ROM images

* used in coreboot build process to assemble ROM image structure
* can also be used manually
* built along with coreboot
* located at `build/cbfstool`

???

* cbfstool is utility for managing CBFS formatted ROM images
* It is used in coreboot build process to assemble ROM image structure, but it
  can also be used manually to print image structure, add new file or extract
  existing file.
* It is built as part of the coreboot build process and located at `build/cbfstool`

---

# CBFS and cbfstool

Nice manual built in help command:

help:
```{.bash}
./build/cbfstool -h
```

syntax:
```{.bash}
./build/cbfstool FILE COMMAND [-v] [PARAMETERS]...
```

Important commands: `print`, `extract`, `add`, `add-stage`, `add-payload`

???

* cbfstool is located at `build/cbfstool`. It has really good command line help,
  so all possible commands, parameters and types can be found directly there.
* The syntax is to pass ROM image name as a first argument, then command, and
  parameters at the end.  `-v` flag can of course be used for verbose output.
  The most commonly used commands are probably print, extract, add, add-payload.

---

# CBFS and cbfstool

* Show content of ROM image:

```{.bash}
./cbfstool ./coreboot.rom print
```

* Extract file from ROM image:

```{.bash}
./cbfstool ./coreboot.rom extract -n config -f config
```

* Add file to ROM image:

```{.bash}
./cbfstool ./coreboot.rom add -f config -n config -t raw
```

---

# Quiz

???

* **How many Makefiles are there in coreboot?**

--

 Only 1 in root directory and 1 per payload and tools

--

* **My operating system does not support docker, how can I build toolchain?**

--

 `make crossgcc-ARCH`

--

* **Where can I find the toolchain inside the container?**

--

 `/opt/xgcc/bin`

--


* **How can I invoke coreboot configuration menu?**

--

 `make menuconfig`
