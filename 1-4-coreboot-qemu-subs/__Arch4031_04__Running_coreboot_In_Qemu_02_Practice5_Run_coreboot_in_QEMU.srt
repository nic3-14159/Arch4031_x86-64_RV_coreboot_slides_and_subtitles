1
00:00:00,719 --> 00:00:06,319
so when we have our

2
00:00:03,040 --> 00:00:09,599
core boot rom binary we can try to

3
00:00:06,319 --> 00:00:13,200
run it inside qmo and see what

4
00:00:09,599 --> 00:00:16,560
what will happen uh for that we will use

5
00:00:13,200 --> 00:00:19,359
qmo system um with a minus

6
00:00:16,560 --> 00:00:21,039
m which means machine which in this case

7
00:00:19,359 --> 00:00:24,160
indicates we will have

8
00:00:21,039 --> 00:00:27,359
we will need chipset q35

9
00:00:24,160 --> 00:00:31,760
as a bios we will provide path to

10
00:00:27,359 --> 00:00:35,120
core boot rom binary then we will

11
00:00:31,760 --> 00:00:38,239
have serial as standard

12
00:00:35,120 --> 00:00:41,680
input output and we will

13
00:00:38,239 --> 00:00:44,719
define display as known just to avoid

14
00:00:41,680 --> 00:00:47,840
um uh default

15
00:00:44,719 --> 00:00:49,280
um qr mode display and popping up

16
00:00:47,840 --> 00:00:52,320
windows

17
00:00:49,280 --> 00:00:55,760
which in in case of running it in

18
00:00:52,320 --> 00:00:56,879
a container uh could could cause some

19
00:00:55,760 --> 00:00:59,520
problems

20
00:00:56,879 --> 00:01:00,320
then uh just to leave we of course will

21
00:00:59,520 --> 00:01:04,720
see some

22
00:01:00,320 --> 00:01:06,640
call boot bootlog and to leave that

23
00:01:04,720 --> 00:01:08,720
of course nothing will boot what is

24
00:01:06,640 --> 00:01:11,119
expected because we don't have any disk

25
00:01:08,720 --> 00:01:14,320
and any operating system

26
00:01:11,119 --> 00:01:17,439
but to leave that we just type

27
00:01:14,320 --> 00:01:21,200
ctrl c once and and of course

28
00:01:17,439 --> 00:01:25,439
uh we will create alias just to simplify

29
00:01:21,200 --> 00:01:29,680
further executions of the same command

30
00:01:25,439 --> 00:01:29,680
so um let's go with that

31
00:01:29,920 --> 00:01:34,640
and maybe i will just simply start with

32
00:01:33,280 --> 00:01:41,840
uh

33
00:01:34,640 --> 00:01:41,840
with creating alias

34
00:01:51,119 --> 00:01:56,159
okay let's see if it works

35
00:01:56,560 --> 00:02:00,399
okay it seemed it seemed to work so as

36
00:01:59,439 --> 00:02:04,079
you can see

37
00:02:00,399 --> 00:02:08,959
um we we boot very very fast

38
00:02:04,079 --> 00:02:12,959
but let's try to roll um

39
00:02:08,959 --> 00:02:16,560
to some meaningful

40
00:02:12,959 --> 00:02:17,599
logs um so what we can see for example

41
00:02:16,560 --> 00:02:20,879
here

42
00:02:17,599 --> 00:02:25,520
we see that we're using c bios version

43
00:02:20,879 --> 00:02:25,520
114 we see that

44
00:02:26,319 --> 00:02:33,519
the c bios was built using gcc corbett

45
00:02:29,680 --> 00:02:37,280
toolchain with some particular version

46
00:02:33,519 --> 00:02:40,480
this is maybe not some important kvm

47
00:02:37,280 --> 00:02:43,840
and of course it's it's trying to find

48
00:02:40,480 --> 00:02:46,879
some medium from which it can start

49
00:02:43,840 --> 00:02:46,879
operating system

50
00:02:48,400 --> 00:02:52,800
so we just canceled the execution of it

51
00:02:53,840 --> 00:02:59,840
and that would be all for this practice

