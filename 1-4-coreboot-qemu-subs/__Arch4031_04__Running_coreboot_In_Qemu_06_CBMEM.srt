1
00:00:00,44 --> 00:00:02,73
Let's talk a little bit about CBmm

2
00:00:02,74 --> 00:00:06,56
CBmm is a special region in the Iran reserved for

3
00:00:06,57 --> 00:00:07,0
logs,

4
00:00:07,01 --> 00:00:12,15
performance measuring and values other information that korbut storing during

5
00:00:12,15 --> 00:00:12,86
the boot time

6
00:00:13,44 --> 00:00:16,93
The source code for the tool that extract that information

7
00:00:16,93 --> 00:00:22,33
from the memory is in corporate sorcery and location of

8
00:00:22,33 --> 00:00:23,01
you toes

9
00:00:23,02 --> 00:00:23,76
CB men

10
00:00:24,74 --> 00:00:25,54
Um,

11
00:00:25,55 --> 00:00:30,84
it can be those information which exposed CBM tool can

12
00:00:30,84 --> 00:00:33,68
be also access it through the payload which is used

13
00:00:33,68 --> 00:00:36,28
during the payload development

14
00:00:36,29 --> 00:00:40,28
You usually payload to for some values useful features like

15
00:00:40,29 --> 00:00:43,46
getting those information from memory

16
00:00:44,54 --> 00:00:48,36
You may think that such kind of logging uh has

17
00:00:48,36 --> 00:00:52,11
some overhead but it is developed in a way that

18
00:00:52,11 --> 00:00:53,56
there is almost no overhead

19
00:00:53,94 --> 00:00:59,83
Um unfortunately there is static allocations that statically allocated space

20
00:00:59,83 --> 00:01:01,14
for the logs

21
00:01:01,15 --> 00:01:04,71
So if you have logs which are two variables,

22
00:01:04,72 --> 00:01:08,09
you can easily run out of space uh in that

23
00:01:08,1 --> 00:01:12,25
case some part of logs is truncated and you're just

24
00:01:12,26 --> 00:01:16,18
losing that but typically you don't need everything,

25
00:01:16,19 --> 00:01:18,56
you just need some interesting part

26
00:01:19,44 --> 00:01:22,56
And of course you can decrease the local

27
00:01:24,04 --> 00:01:26,86
In some cases it may be also very hard to

28
00:01:27,24 --> 00:01:28,75
access those information,

29
00:01:28,75 --> 00:01:30,96
especially if you cannot put the platform

30
00:01:31,34 --> 00:01:33,35
This happened a lot during them

31
00:01:34,04 --> 00:01:37,76
Bring up process during the initial porting of car boot

32
00:01:38,34 --> 00:01:40,95
Uh in that case you might uh,

33
00:01:40,96 --> 00:01:42,55
you might use J talk

34
00:01:42,56 --> 00:01:46,07
Um there are also other methods which we probably will

35
00:01:46,08 --> 00:01:50,42
will explain in four the trainings um,

36
00:01:50,43 --> 00:01:51,05
this part,

37
00:01:51,05 --> 00:01:52,91
despite its limitation,

38
00:01:52,92 --> 00:01:58,57
CBM cmm is a standard tool for gathering the information

39
00:01:58,58 --> 00:02:01,61
especially if you don't have serial output,

40
00:02:01,61 --> 00:02:06,15
which can give you the same information or similar information

41
00:02:06,84 --> 00:02:07,12
So,

42
00:02:07,12 --> 00:02:08,37
to compile the tool,

43
00:02:08,38 --> 00:02:15,81
you're just going to utilize CBM CBM unmake and let's

44
00:02:15,81 --> 00:02:16,85
try to do that

45
00:02:18,54 --> 00:02:19,16
Mm hmm

46
00:02:22,54 --> 00:02:33,45
There's a few details CB mom to make and and

47
00:02:33,45 --> 00:02:35,18
you can see we have cBmm,

48
00:02:35,19 --> 00:02:36,29
it is very,

49
00:02:36,29 --> 00:02:37,24
very small

50
00:02:37,25 --> 00:02:40,36
Uh and you can use it

