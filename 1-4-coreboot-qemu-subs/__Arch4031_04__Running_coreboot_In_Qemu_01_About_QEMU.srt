1
00:00:01,44 --> 00:00:02,17
as I said,

2
00:00:02,18 --> 00:00:06,62
Kumo is open source emulator for various platforms

3
00:00:06,63 --> 00:00:09,39
It supports multiple architectures

4
00:00:09,4 --> 00:00:12,85
Um It follows the next philosophy

5
00:00:12,86 --> 00:00:18,41
It is it fully it's fully software implementation of various

6
00:00:18,41 --> 00:00:19,26
environments

7
00:00:19,64 --> 00:00:21,9
Um What is very important,

8
00:00:21,91 --> 00:00:28,2
it can leverage hardware virtualization features to speed up our

9
00:00:28,2 --> 00:00:33,93
boot process or our uh emulation process or this is

10
00:00:33,93 --> 00:00:38,62
like a para para virtualization in this case it supports

11
00:00:38,62 --> 00:00:45,19
both KV M like as well as um age A

12
00:00:45,2 --> 00:00:47,86
X M on windows,

13
00:00:47,97 --> 00:00:50,39
but in this case this is not so important for

14
00:00:50,39 --> 00:00:50,76
us

15
00:00:51,24 --> 00:00:54,2
Um it supports many architectures,

16
00:00:54,21 --> 00:00:55,34
for example,

17
00:00:55,34 --> 00:00:56,21
for for Intel,

18
00:00:56,21 --> 00:01:01,09
it supports VTD but what is also important,

19
00:01:01,1 --> 00:01:04,0
it supports risk five and open power

20
00:01:04,01 --> 00:01:08,0
Uh this helped importing core boot on top of this

21
00:01:08,01 --> 00:01:11,17
emulation platforms and of course,

22
00:01:11,18 --> 00:01:17,2
bring I called that project closer to real hardware implementation

23
00:01:17,21 --> 00:01:18,56
of these architectures

24
00:01:19,64 --> 00:01:20,44
It's it's very,

25
00:01:20,44 --> 00:01:22,11
very subtle tool,

26
00:01:22,12 --> 00:01:22,87
has many,

27
00:01:22,87 --> 00:01:23,65
many options,

28
00:01:23,65 --> 00:01:28,3
like you can probably do training only about um Oh

29
00:01:28,31 --> 00:01:33,5
um it is used widely by cloud providers and various

30
00:01:33,5 --> 00:01:35,66
important vendors

31
00:01:36,14 --> 00:01:37,76
But from our perspective,

32
00:01:37,77 --> 00:01:42,09
it most important features are for formal booting

33
00:01:42,1 --> 00:01:43,2
Luckily

34
00:01:43,21 --> 00:01:44,01
QM

35
00:01:44,01 --> 00:01:48,34
I can give you ability to support whatever form and

36
00:01:48,34 --> 00:01:49,32
you want to hook

37
00:01:49,33 --> 00:01:52,04
It also support you if I indicate too,

38
00:01:52,05 --> 00:01:52,86
about car,

39
00:01:52,86 --> 00:01:54,41
but is always also supported,

40
00:01:54,42 --> 00:01:55,16
and that's great

41
00:01:55,34 --> 00:01:58,16
We will use that to learn more about corporate

