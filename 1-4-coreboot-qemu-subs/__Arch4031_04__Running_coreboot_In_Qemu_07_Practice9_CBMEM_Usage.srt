1
00:00:00,14 --> 00:00:01,96
So let's practice with C b man

2
00:00:02,64 --> 00:00:05,16
First step we can skip because we already did that

3
00:00:05,74 --> 00:00:11,72
In second step we look at the debian image partition

4
00:00:11,72 --> 00:00:17,26
partition layout to be able to calculate the offset of

5
00:00:17,26 --> 00:00:20,86
the love bug device where the look back device will

6
00:00:20,86 --> 00:00:24,96
begin inside the inside our image

7
00:00:25,44 --> 00:00:30,49
In that way we can despite the our image contained

8
00:00:30,49 --> 00:00:31,53
multiple partitions

9
00:00:31,53 --> 00:00:36,49
We can just Moment 1 of the partitions and we

10
00:00:36,49 --> 00:00:38,69
create some directory

11
00:00:38,7 --> 00:00:43,65
Then we mount the loop device into a group directory

12
00:00:44,04 --> 00:00:47,14
and we can copy inside the image

13
00:00:47,15 --> 00:00:50,06
The cbs the compile at sea Beeman binary

14
00:00:50,54 --> 00:00:54,16
We're just amount and they touch all the look back

15
00:00:54,16 --> 00:01:00,28
devices then we can boot and actually see how City

16
00:01:00,28 --> 00:01:03,65
man works by gathering logs and like how we can

17
00:01:03,66 --> 00:01:05,45
extract those logs from the memorial

18
00:01:05,84 --> 00:01:10,26
So let's try to use those instructions

19
00:01:12,24 --> 00:01:13,7
First of all,

20
00:01:13,78 --> 00:01:20,15
let's use our dr container

21
00:01:21,64 --> 00:01:28,16
Then we can simply check partition table,

22
00:01:34,04 --> 00:01:38,79
you can see that Um our first partition,

23
00:01:38,79 --> 00:01:45,86
which is our system starts at the block uh 2048

24
00:01:46,24 --> 00:01:51,1
And size of the side of the sector is 5

25
00:01:51,1 --> 00:01:52,19
12 bytes

26
00:01:52,23 --> 00:01:56,0
So just to calculate this 4th,

27
00:01:56,01 --> 00:02:02,56
5 12 gives us this number

28
00:02:03,34 --> 00:02:08,17
of course we will use that number 2 um create

29
00:02:08,17 --> 00:02:19,96
low back device and then we can create loop victoria

30
00:02:19,96 --> 00:02:21,28
It's already exists

31
00:02:22,04 --> 00:02:23,75
Check if nothing inside

32
00:02:24,44 --> 00:02:30,26
That's more the develop zero in character

33
00:02:31,54 --> 00:02:32,02
Yeah

34
00:02:32,03 --> 00:02:33,76
So I have type of here

35
00:02:35,44 --> 00:02:35,9
Okay

36
00:02:35,9 --> 00:02:36,38
In look,

37
00:02:36,38 --> 00:02:40,7
we have all the information of all the root file

38
00:02:40,7 --> 00:02:42,27
system of our Debian

39
00:02:42,28 --> 00:02:44,86
Let's copy the mm hmm

40
00:02:45,24 --> 00:02:56,81
Should be man binary contract that is there

41
00:02:56,89 --> 00:02:58,06
We can see here

42
00:02:59,04 --> 00:03:01,05
So let's amount a loop

43
00:03:04,34 --> 00:03:06,46
Let's detach all the devices

44
00:03:07,04 --> 00:03:13,28
And now we can use our old Aaliyah's to boot

45
00:03:13,29 --> 00:03:32,26
the system and around CB member and all these that

46
00:03:32,26 --> 00:03:39,86
we just modified in the same way

47
00:03:39,86 --> 00:03:45,46
You can use method for transferring all the files

48
00:03:46,24 --> 00:03:46,56
Uh huh

49
00:03:47,84 --> 00:03:52,12
So we can see we have CB mom here so

50
00:03:52,12 --> 00:03:55,89
we can create some fires inside this system and then

51
00:03:55,89 --> 00:04:00,31
moan the day beyond image and extract those either dumps

52
00:04:00,31 --> 00:04:04,82
either some locks or of course you can use networking

53
00:04:04,82 --> 00:04:05,25
for that

54
00:04:06,34 --> 00:04:09,04
So let's check minus C

55
00:04:09,04 --> 00:04:12,19
And you can see that we already so minus C

56
00:04:12,2 --> 00:04:21,23
Um so minus C

57
00:04:21,24 --> 00:04:24,88
Uh means that it will show us console

58
00:04:24,89 --> 00:04:28,35
Um It's council look which was which was gathered at

59
00:04:28,84 --> 00:04:30,52
during the boot time

60
00:04:30,53 --> 00:04:35,15
Of course we we unless we have suffered in the

61
00:04:35,15 --> 00:04:39,7
memorial and this buffer is controlled on the build time

62
00:04:39,7 --> 00:04:40,76
of the car boot

63
00:04:41,44 --> 00:04:46,09
We may have multiple boot so that's why we have

64
00:04:46,09 --> 00:04:50,26
this minus one option which gives us just last boot

65
00:04:50,94 --> 00:04:57,1
Um And there is coverage information maybe there's no coverage

66
00:04:57,1 --> 00:04:58,25
information included

67
00:04:58,64 --> 00:05:02,8
Um There there are there is list of tables which

68
00:05:02,8 --> 00:05:03,87
are included,

69
00:05:03,88 --> 00:05:07,14
there is quite a lot of them um So all

70
00:05:07,14 --> 00:05:11,51
this table can be um can be presented

71
00:05:11,52 --> 00:05:16,0
There are some TPM locks if um if there were

72
00:05:16,0 --> 00:05:21,41
gathered um there are some time stops unfortunately in case

73
00:05:21,41 --> 00:05:24,77
of um oh we will have problem of calculating time

74
00:05:24,78 --> 00:05:31,26
because there is not enough correct CPU frequency information in

75
00:05:31,26 --> 00:05:36,52
the system but we can use console and look what's

76
00:05:36,52 --> 00:05:38,89
going on here and as you can see we see

77
00:05:38,89 --> 00:05:44,45
boot block starting like in the classical um boot boot

78
00:05:44,45 --> 00:05:47,49
lock from car boot then if there is from stage

79
00:05:47,59 --> 00:05:55,64
and it continues booting with all the stages and that's

80
00:05:55,64 --> 00:05:55,86
it

