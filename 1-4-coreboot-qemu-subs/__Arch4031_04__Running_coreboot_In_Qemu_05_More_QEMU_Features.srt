1
00:00:00,24 --> 00:00:01,5
as I said previously,

2
00:00:01,5 --> 00:00:05,01
QM provides quite a quite a rich ecosystem

3
00:00:05,01 --> 00:00:09,3
It has many features uh definitely you can easily find

4
00:00:09,31 --> 00:00:12,86
documentation which describes all these interesting features

5
00:00:13,24 --> 00:00:18,12
One of the the most important one that that it's

6
00:00:18,13 --> 00:00:22,03
good to use during the former development is G d

7
00:00:22,03 --> 00:00:26,0
be hooking with G D B um to um oh

8
00:00:26,0 --> 00:00:32,45
and debug whatever is running inside um some execution locks

9
00:00:32,46 --> 00:00:34,29
some dumping of the,

10
00:00:34,3 --> 00:00:37,16
what's going on inside the emulated environment

11
00:00:37,54 --> 00:00:39,23
We can precisely see,

12
00:00:39,24 --> 00:00:40,23
for example,

13
00:00:40,23 --> 00:00:44,87
from what locations uh code that is running inside

14
00:00:44,87 --> 00:00:47,47
Chemo reads where it writes

15
00:00:47,48 --> 00:00:51,46
Um I know there are there is some work related

16
00:00:51,46 --> 00:00:56,29
to management engine uh emulation and then you can of

17
00:00:56,29 --> 00:00:59,05
course they do some exploration of the systems

18
00:00:59,14 --> 00:01:01,26
If you are interested in that part,

19
00:01:01,78 --> 00:01:03,59
there are many other ways,

20
00:01:03,59 --> 00:01:06,87
there are some extensions to cuba mo,

21
00:01:06,88 --> 00:01:07,53
for example,

22
00:01:07,53 --> 00:01:11,26
to support the RTM to support SgX

23
00:01:11,34 --> 00:01:13,06
So,

24
00:01:13,54 --> 00:01:14,43
chemo is very,

25
00:01:14,43 --> 00:01:16,88
very rich and can be very helpful during the few

26
00:01:16,88 --> 00:01:17,65
more development

27
00:01:18,44 --> 00:01:20,45
Um of course,

28
00:01:20,45 --> 00:01:22,7
like how typical you will use that

29
00:01:22,71 --> 00:01:26,91
Uh it has good potential to do shift left strategy

30
00:01:26,91 --> 00:01:29,57
so you don't have hardware or you are limited with

31
00:01:29,57 --> 00:01:33,45
supporting capabilities or you simply just developing some payload and

32
00:01:33,45 --> 00:01:37,47
you don't have to do that on the uh on

33
00:01:37,47 --> 00:01:38,25
the hardware

34
00:01:38,26 --> 00:01:42,91
And that's good because like you can speed up developed

35
00:01:42,91 --> 00:01:43,37
test,

36
00:01:43,37 --> 00:01:47,72
fix cycle and just basing on um of course this

37
00:01:47,72 --> 00:01:52,97
means some level of abstraction and then we you have

38
00:01:52,97 --> 00:01:55,08
to understand what's the difference between what you have in

39
00:01:55,08 --> 00:01:58,65
kuusamo and what you have in real hardware,

40
00:01:59,14 --> 00:02:01,35
Other things which are very important

41
00:02:01,36 --> 00:02:05,97
Our implementations of um support for risk five and open

42
00:02:05,97 --> 00:02:09,93
power in Cuba mo thanks to that port in korbut

43
00:02:09,93 --> 00:02:12,56
was uh was quite easy,

44
00:02:12,57 --> 00:02:13,7
quite easy,

45
00:02:13,71 --> 00:02:16,96
at least there was there were tools for that and

46
00:02:16,96 --> 00:02:21,6
that that definitely speed up growth of ecosystem of alternative

47
00:02:21,6 --> 00:02:22,22
architectures,

48
00:02:22,22 --> 00:02:24,4
which would bring additional value,

49
00:02:24,41 --> 00:02:25,16
um,

50
00:02:25,24 --> 00:02:26,66
in area of openness,

51
00:02:26,67 --> 00:02:28,06
in area of security,

52
00:02:28,44 --> 00:02:32,68
which should be of interest to any core but fun

53
00:02:32,69 --> 00:02:33,96
corvette enthusiast

54
00:02:35,24 --> 00:02:39,05
So let's go to the quizzes and assignment

