1
00:00:00,14 --> 00:00:04,55
sometimes when you're doing development with um oh you might

2
00:00:04,56 --> 00:00:09,82
need to lock some output to um to file as

3
00:00:09,82 --> 00:00:13,84
you saw during the booting a car boot in kumo

4
00:00:13,84 --> 00:00:17,96
everything running very very fast and sometimes it is hard

5
00:00:17,97 --> 00:00:21,46
to catch all the things

6
00:00:21,54 --> 00:00:25,45
Sometimes your terminal do not have enough scroll back,

7
00:00:25,45 --> 00:00:27,57
we do not have enough history

8
00:00:27,58 --> 00:00:31,46
So it is also a good idea to have some

9
00:00:31,46 --> 00:00:37,35
additional options for um logging to um to fight

10
00:00:37,94 --> 00:00:42,56
There is such option inside core boot built,

11
00:00:43,04 --> 00:00:47,22
you can see there is kumo debug console output and

12
00:00:47,22 --> 00:00:49,32
there is I o address of this debate,

13
00:00:49,32 --> 00:00:53,36
Council provided us parameter here

14
00:00:54,94 --> 00:00:59,39
Um It should be already enabled by default but still

15
00:00:59,39 --> 00:01:02,13
I would like to show you where it is in

16
00:01:02,14 --> 00:01:05,66
council money so you can remember how to,

17
00:01:05,67 --> 00:01:07,96
how to find how to find it

18
00:01:08,74 --> 00:01:13,14
Um and then we will extend our C BQ are

19
00:01:13,14 --> 00:01:22,96
more command with additional parameters which which I will explain

20
00:01:23,74 --> 00:01:25,05
in a second

21
00:01:25,06 --> 00:01:29,05
So first of all let's switch to the council,

22
00:01:29,64 --> 00:01:33,77
we want to verify,

23
00:01:33,77 --> 00:01:38,76
where is the kumo Debug console set up

24
00:01:39,14 --> 00:01:44,25
So we're going to console and we're looking for you

25
00:01:44,25 --> 00:01:48,71
are more the bar council as you can see here

26
00:01:48,71 --> 00:01:49,39
it is,

27
00:01:49,4 --> 00:01:54,4
you can change its and are you address but it's

28
00:01:54,4 --> 00:01:56,9
already said and should be compiled,

29
00:01:56,91 --> 00:02:02,85
compiled in um our core but of course everything is

30
00:02:02,85 --> 00:02:05,84
already compiled so no change

31
00:02:05,85 --> 00:02:08,26
That's why we didn't rebuild anything

32
00:02:08,74 --> 00:02:11,46
So let's get back to our slides

33
00:02:13,94 --> 00:02:14,52
Okay

34
00:02:14,52 --> 00:02:18,4
So you can see there is extension of the uh

35
00:02:18,41 --> 00:02:21,8
of the areas that we created C BQ amount with

36
00:02:21,81 --> 00:02:26,67
character device which will be attached to cuba more emulated

37
00:02:26,67 --> 00:02:27,36
system

38
00:02:27,74 --> 00:02:36,76
This character device connects file um and it will connect

39
00:02:37,24 --> 00:02:40,96
other device here identified by Deborah Kahn

40
00:02:41,34 --> 00:02:44,84
The name is also here so it's good to remember

41
00:02:44,85 --> 00:02:47,29
And the path to that file will be just local

42
00:02:47,29 --> 00:02:49,16
file with the name lock

43
00:02:49,16 --> 00:02:52,2
You can provide any name you want and of course

44
00:02:52,2 --> 00:02:53,92
we need to connect,

45
00:02:53,93 --> 00:02:55,93
connect our ISA

46
00:02:55,94 --> 00:03:00,86
The back on device which based on the configuration of

47
00:03:00,86 --> 00:03:06,09
car boot will be associated with I O address O

48
00:03:06,09 --> 00:03:09,93
X 402 This was defined in the car but previously

49
00:03:09,94 --> 00:03:12,77
and of course we just hook this address to is

50
00:03:12,77 --> 00:03:17,55
a debug con and and then exposed that device to

51
00:03:17,56 --> 00:03:22,15
file so this file can be recorded to our disk

52
00:03:22,54 --> 00:03:26,3
So let's check how good this comet works

53
00:03:29,14 --> 00:03:37,63
So just let's use our failures and now it should

54
00:03:37,63 --> 00:03:38,03
work

55
00:03:38,04 --> 00:03:38,35
Yeah

56
00:03:39,04 --> 00:03:39,87
Okay

57
00:03:39,88 --> 00:03:43,91
So we already booted with see bios,

58
00:03:43,91 --> 00:03:46,9
the core but with the bios in kumo the outward

59
00:03:46,9 --> 00:03:48,1
going to standard,

60
00:03:48,11 --> 00:03:53,45
uh when we leave we should find the log file

61
00:03:54,14 --> 00:03:55,69
Of course there it is

62
00:03:55,7 --> 00:03:59,6
And if we will just show you top of that

63
00:03:59,6 --> 00:04:00,5
lock file,

64
00:04:00,51 --> 00:04:06,24
you can see we have corporate booting here for 12

65
00:04:06,25 --> 00:04:07,37
the boot block,

66
00:04:07,38 --> 00:04:10,96
just showing that everything is local level seven,

67
00:04:11,34 --> 00:04:14,25
and we're just processing the boot

68
00:04:14,64 --> 00:04:16,5
So if we cut log,

69
00:04:16,51 --> 00:04:21,61
we will have to call log file of the uh

70
00:04:21,62 --> 00:04:22,25
of the system

