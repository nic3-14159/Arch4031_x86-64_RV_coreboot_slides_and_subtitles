1
00:00:00,34 --> 00:00:05,46
other very important feature of komo is cuba monitor

2
00:00:05,84 --> 00:00:09,08
It is very part of a powerful piece of cuba

3
00:00:09,08 --> 00:00:13,15
mo it gives direct control over the emulated hardware

4
00:00:13,16 --> 00:00:17,77
We can like art processors and memory uh to be

5
00:00:17,77 --> 00:00:19,29
honest with Cuban,

6
00:00:19,3 --> 00:00:24,27
Cuban monitor has all the features that any a professional

7
00:00:24,27 --> 00:00:26,25
call provider would need

8
00:00:26,64 --> 00:00:31,14
And even more so it's very very sophisticated

9
00:00:31,14 --> 00:00:34,41
It gives ability to look into memory directly

10
00:00:34,42 --> 00:00:37,97
But in this uh in this training we will not

11
00:00:37,97 --> 00:00:40,34
export it very exploded very deeply

12
00:00:40,34 --> 00:00:43,38
It it is very easy to find documentation about all

13
00:00:43,38 --> 00:00:44,16
the features

14
00:00:44,54 --> 00:00:51,04
Um let's just show like typically what former developers use

15
00:00:51,05 --> 00:00:55,19
Uh they typically check for example if um oh uh

16
00:00:55,2 --> 00:00:59,09
with core boot hanging at some point we check what

17
00:00:59,09 --> 00:00:59,97
kind of registers

18
00:00:59,97 --> 00:01:01,41
What's the state of the registers?

19
00:01:01,41 --> 00:01:02,49
Where is the stock pointer?

20
00:01:02,49 --> 00:01:06,15
Where is the uh instruction pointer pointer

21
00:01:06,24 --> 00:01:09,5
So I will show you how to dump registers

22
00:01:09,51 --> 00:01:13,86
Um There are some additional features like enabling GDP server

23
00:01:13,86 --> 00:01:17,0
to hook um into um Oh,

24
00:01:17,01 --> 00:01:19,65
but this will be discussed further

25
00:01:19,66 --> 00:01:23,96
Um But at this point let's let's discuss how to

26
00:01:24,14 --> 00:01:26,45
connect cumin monitor

27
00:01:26,74 --> 00:01:29,1
So to to enable cumin monitor,

28
00:01:29,11 --> 00:01:32,75
we can use very values different methods

29
00:01:32,75 --> 00:01:34,59
We can use standard I

30
00:01:34,59 --> 00:01:34,82
O

31
00:01:34,82 --> 00:01:39,37
We can use like multiple different methods but in our

32
00:01:39,37 --> 00:01:41,49
case we will use unique sockets

33
00:01:41,5 --> 00:01:45,34
Um And then we'll use socket to connect to that

34
00:01:45,34 --> 00:01:46,28
unique socket

35
00:01:46,29 --> 00:01:51,15
Uh and have some fun using cumin monitor

36
00:01:52,04 --> 00:01:53,8
Um Yeah,

37
00:01:53,81 --> 00:01:56,65
so the process is not not very trivial

38
00:01:56,66 --> 00:02:01,85
We're running CUBA more with monitor parameters defining where is

39
00:02:01,85 --> 00:02:03,35
the socket location?

40
00:02:04,44 --> 00:02:06,86
Some parameters for socket

41
00:02:07,54 --> 00:02:10,98
And then we have because we in that way we

42
00:02:10,98 --> 00:02:18,25
already um use default output of KUMo which is saturated

43
00:02:18,25 --> 00:02:20,46
with carbon locks

44
00:02:21,04 --> 00:02:23,86
We somehow have to hook into the system

45
00:02:24,34 --> 00:02:27,82
That's why we just opening another terminal

46
00:02:27,83 --> 00:02:29,96
We're looking for our container

47
00:02:29,97 --> 00:02:33,51
We because CUBA is already in container

48
00:02:33,52 --> 00:02:38,88
We have to enter the container and find uh find

49
00:02:38,88 --> 00:02:40,71
the socket that connect to the socket

50
00:02:40,72 --> 00:02:45,17
Then we can explore um Cure monitor comments

51
00:02:45,39 --> 00:02:46,66
So let's try that

52
00:02:48,24 --> 00:02:50,76
So will it just execute the monitor?

53
00:02:51,34 --> 00:02:52,79
Of course us

54
00:02:52,8 --> 00:02:56,86
It was shot on the slide which is adding to

55
00:02:56,86 --> 00:02:58,55
the earliest monitor,

56
00:02:59,04 --> 00:03:04,99
unique with the path to the unique socket

57
00:03:05,28 --> 00:03:10,19
Of course this would be server and and some parameters

58
00:03:10,19 --> 00:03:11,25
for the socket

59
00:03:11,94 --> 00:03:17,56
So we're running um let's switch to second terminal

60
00:03:18,44 --> 00:03:23,09
Um We need asking them are here also

61
00:03:23,1 --> 00:03:27,06
And what we have to do here is identify the

62
00:03:27,06 --> 00:03:30,86
docker container in which we run this Q

63
00:03:30,86 --> 00:03:31,66
And a session

64
00:03:32,14 --> 00:03:36,67
Um So far so that's at least what kind of

65
00:03:36,67 --> 00:03:41,86
doctors I'm running This is the idea of the container

66
00:03:42,74 --> 00:03:50,95
So let's execute some interactive command in this container

67
00:03:51,74 --> 00:03:55,51
Um Yeah,

68
00:03:55,51 --> 00:03:56,76
we are inside

69
00:03:57,14 --> 00:04:01,51
So now we should be able to use so called

70
00:04:01,52 --> 00:04:10,15
command for hooking into the a socket exposed by the

71
00:04:10,16 --> 00:04:10,86
Qm

72
00:04:13,22 --> 00:04:14,19
As you can see,

73
00:04:14,19 --> 00:04:15,96
we are in cuba monitor

74
00:04:16,54 --> 00:04:20,35
Uh there are there are multiple commands here

75
00:04:20,94 --> 00:04:25,16
You can see how reach this environment is

76
00:04:25,74 --> 00:04:28,95
Um I even don't know all the possibilities here,

77
00:04:29,34 --> 00:04:37,39
but what's important for for basic exploration of the possibilities

78
00:04:37,4 --> 00:04:41,75
of human winter is info command

79
00:04:42,24 --> 00:04:45,93
Info command is very important and show quite a lot

80
00:04:45,93 --> 00:04:47,66
of information about the system

81
00:04:48,34 --> 00:04:49,91
Um first of all,

82
00:04:49,91 --> 00:04:53,27
it can show information about the processor

83
00:04:53,28 --> 00:04:56,32
Um It can do the dumps

84
00:04:56,33 --> 00:05:02,17
Um It can do perform some interesting reports about uh

85
00:05:02,18 --> 00:05:06,66
interrupt a peaks about virtualization,

86
00:05:06,66 --> 00:05:07,75
about memory

87
00:05:08,24 --> 00:05:11,46
But what I want to show you here is for

88
00:05:11,46 --> 00:05:14,36
example registers

89
00:05:17,44 --> 00:05:18,86
And as you can see,

90
00:05:19,02 --> 00:05:20,4
we're probably risk,

91
00:05:20,41 --> 00:05:25,42
we are probably in Um a 16 bit mold um

92
00:05:25,43 --> 00:05:34,26
where our see bios trying to handle some some um

93
00:05:34,27 --> 00:05:35,25
disks

94
00:05:36,24 --> 00:05:39,78
But you can see that it also change uh during

95
00:05:39,78 --> 00:05:44,36
the execution so you can find the stack pointer,

96
00:05:44,36 --> 00:05:47,45
you can find in the instruction pointer and a bunch

97
00:05:47,45 --> 00:05:49,56
of other interesting things

98
00:05:49,94 --> 00:05:54,47
Um There's also a TPM information if you have any

99
00:05:54,47 --> 00:05:55,85
TPM hooked

100
00:05:56,24 --> 00:06:01,86
Um You can also look at the structure of the

101
00:06:01,87 --> 00:06:08,41
emulated device emulated system using Q three Q three shows

102
00:06:08,42 --> 00:06:12,48
in a kind of 33 level what kind of devices

103
00:06:12,48 --> 00:06:16,06
with which parameters are emulated in this system

104
00:06:16,44 --> 00:06:17,48
And thanks to that,

105
00:06:17,48 --> 00:06:20,24
you can see like where is the mm I offer

106
00:06:20,24 --> 00:06:23,81
this device uh a bunch of like how the uh

107
00:06:23,82 --> 00:06:28,2
based address registers are configured um and a bunch of

108
00:06:28,21 --> 00:06:32,26
other important information when you're doing development

109
00:06:32,74 --> 00:06:35,91
Feel free to explore this command even more

110
00:06:35,92 --> 00:06:40,23
It's very interesting and definitely it is worth to learn

111
00:06:40,23 --> 00:06:41,26
more about it

