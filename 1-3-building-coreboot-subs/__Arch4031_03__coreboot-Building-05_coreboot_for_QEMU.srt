1
00:00:00,160 --> 00:00:03,120
okay so in order to build some base

2
00:00:02,320 --> 00:00:06,160
images

3
00:00:03,120 --> 00:00:08,000
uh of core boot one has to always set

4
00:00:06,160 --> 00:00:09,360
the main board configuration to match

5
00:00:08,000 --> 00:00:11,920
the target platform

6
00:00:09,360 --> 00:00:12,799
so basically uh all we need to do is uh

7
00:00:11,920 --> 00:00:15,440
to go to the

8
00:00:12,799 --> 00:00:16,720
main board sub menu and select the main

9
00:00:15,440 --> 00:00:19,279
vendor model

10
00:00:16,720 --> 00:00:20,720
and sometimes their own chip size most

11
00:00:19,279 --> 00:00:22,720
of the time we can stick with the

12
00:00:20,720 --> 00:00:25,119
default round chip size selection

13
00:00:22,720 --> 00:00:26,080
however some of the boards may come with

14
00:00:25,119 --> 00:00:28,320
different flavors

15
00:00:26,080 --> 00:00:30,640
and selecting current rom chip size

16
00:00:28,320 --> 00:00:32,800
might be necessary

17
00:00:30,640 --> 00:00:34,640
below i can i will present you the main

18
00:00:32,800 --> 00:00:35,760
board settings for the cameo which will

19
00:00:34,640 --> 00:00:38,879
be used later

20
00:00:35,760 --> 00:00:42,480
in the next course so basically

21
00:00:38,879 --> 00:00:45,680
for the camera we select the

22
00:00:42,480 --> 00:00:49,840
emulation vendor and um select the main

23
00:00:45,680 --> 00:00:51,520
water sq x86 q35 ic h9

24
00:00:49,840 --> 00:00:53,760
and the default round chip size will be

25
00:00:51,520 --> 00:00:56,000
two megabytes

26
00:00:53,760 --> 00:00:57,280
so now we don't need to do anything more

27
00:00:56,000 --> 00:01:00,480
we just navigate

28
00:00:57,280 --> 00:01:01,920
to the save button with the tab to save

29
00:01:00,480 --> 00:01:05,119
the configuration file

30
00:01:01,920 --> 00:01:08,640
and basically exit to leave the menu

31
00:01:05,119 --> 00:01:09,280
remember before you switch any main

32
00:01:08,640 --> 00:01:12,960
board

33
00:01:09,280 --> 00:01:18,400
always run the make disk click so um

34
00:01:12,960 --> 00:01:18,400
let's quickly see how it goes

35
00:01:23,920 --> 00:01:27,840
we go to the main board menu

36
00:01:29,280 --> 00:01:33,600
and it seems i have already selected the

37
00:01:31,840 --> 00:01:36,720
right chip

38
00:01:33,600 --> 00:01:41,680
um it's the human crew 35

39
00:01:36,720 --> 00:01:44,799
and the round size of two megabytes

40
00:01:41,680 --> 00:01:48,640
and here i can navigate with the tab

41
00:01:44,799 --> 00:01:50,479
and the arrows to save

42
00:01:48,640 --> 00:01:52,479
to the default file that config in the

43
00:01:50,479 --> 00:01:55,280
root carbon directory ok

44
00:01:52,479 --> 00:01:56,560
configuration threaten nice and then

45
00:01:55,280 --> 00:02:00,000
exit

46
00:01:56,560 --> 00:02:02,799
and again exit and that's

47
00:02:00,000 --> 00:02:02,799
basically it

