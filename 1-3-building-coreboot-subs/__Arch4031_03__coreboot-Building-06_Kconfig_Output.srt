1
00:00:00,04 --> 00:00:00,35
Okay

2
00:00:00,36 --> 00:00:03,81
Now that you know how to configure some basic bills

3
00:00:03,81 --> 00:00:06,49
for QM You let's go back for a while today

4
00:00:06,5 --> 00:00:07,75
A conflict

5
00:00:08,14 --> 00:00:11,33
So when the bills is configured,

6
00:00:11,34 --> 00:00:15,36
the King Conflict produces two important output files containing the

7
00:00:15,37 --> 00:00:18,25
full set of options selected during the configuration phase

8
00:00:18,74 --> 00:00:20,96
The first files is a conflict fire

9
00:00:21,34 --> 00:00:23,34
It is created and set within the car,

10
00:00:23,34 --> 00:00:26,26
but through directory up on exciting from a new conflict

11
00:00:26,27 --> 00:00:27,76
text interface

12
00:00:29,04 --> 00:00:33,06
The dot Conflict file gets included by the Corbett main

13
00:00:33,06 --> 00:00:33,56
make file

14
00:00:34,34 --> 00:00:38,29
Its purpose is to expose the conflict options selected during

15
00:00:38,29 --> 00:00:40,94
the configuration face to the make file so that the

16
00:00:40,94 --> 00:00:43,76
build system is aware of what is the target configuration

17
00:00:44,34 --> 00:00:45,54
based on this information,

18
00:00:45,54 --> 00:00:49,42
The build system knows which source files should be built

19
00:00:49,48 --> 00:00:51,96
which features should be enabled etcetera

20
00:00:52,94 --> 00:00:55,15
As we can see in this short example,

21
00:00:55,16 --> 00:00:58,6
the billing settings are set to yes in short,

22
00:00:58,61 --> 00:01:02,7
white or commented out with is not said string at

23
00:01:02,7 --> 00:01:03,16
the end

24
00:01:04,04 --> 00:01:06,55
Strict settings can be set up and a string or

25
00:01:06,56 --> 00:01:07,96
basically left empty

26
00:01:08,94 --> 00:01:14,32
The conflict also allows you to set various other options

27
00:01:14,32 --> 00:01:16,43
like various other types of options,

28
00:01:16,43 --> 00:01:18,86
like integer or heads of this small

29
00:01:20,04 --> 00:01:20,29
Okay

30
00:01:20,29 --> 00:01:23,58
So now let's see quickly how it looks like for

31
00:01:23,58 --> 00:01:32,09
our community built and let's use the last comment as

32
00:01:32,09 --> 00:01:32,8
you can see,

33
00:01:32,81 --> 00:01:36,81
the header says that it is automatically generated file and

34
00:01:36,82 --> 00:01:38,64
it shouldn't be edited manually

35
00:01:38,65 --> 00:01:41,7
So if we want to make some modifications to do

36
00:01:41,7 --> 00:01:46,61
it for the money conflict and you can see the

37
00:01:46,62 --> 00:01:51,81
headers of various sections Are named by the 70s for

38
00:01:51,81 --> 00:01:52,17
example,

39
00:01:52,17 --> 00:01:55,19
we have the China set up and before we have

40
00:01:55,19 --> 00:01:59,81
the main board some money which indicated is indicated by

41
00:01:59,81 --> 00:02:00,96
the main board header here

42
00:02:02,24 --> 00:02:03,05
All right here

43
00:02:03,84 --> 00:02:09,0
Yes we have the general set up here And two

44
00:02:09,01 --> 00:02:11,76
Let's study the few options we see

45
00:02:12,14 --> 00:02:17,61
So basically each kick olympic option which is defined by

46
00:02:17,61 --> 00:02:19,45
astra by given name

47
00:02:19,94 --> 00:02:25,78
It's always a punitive conflict underscore here and the value

48
00:02:25,78 --> 00:02:30,37
sector in the many configuration is assigned after the equal

49
00:02:30,37 --> 00:02:30,75
sign

50
00:02:31,44 --> 00:02:34,78
So basically this is a Corvette built boolean value which

51
00:02:34,78 --> 00:02:38,91
is set to Yes we have a strength local question

52
00:02:38,91 --> 00:02:42,13
which I showed you were and it is contribute as

53
00:02:42,13 --> 00:02:44,69
empty strength but here we have the CPF

54
00:02:44,69 --> 00:02:49,42
S Perfect which indicates how what kind of perfect should

55
00:02:49,43 --> 00:02:51,66
uh stages her?

56
00:02:52,95 --> 00:02:54,86
It is set to fall back string

57
00:02:55,44 --> 00:03:00,19
You can also see other options like uh and it'll

58
00:03:00,19 --> 00:03:02,94
chain which also has to use any other to change

59
00:03:02,94 --> 00:03:06,15
and Corbett one but it is marked as not set

60
00:03:07,04 --> 00:03:10,29
Um let's try to find some other options like hex

61
00:03:10,29 --> 00:03:12,46
and decimal options or interior options

62
00:03:13,44 --> 00:03:16,62
So for example here we have the cbf I size

63
00:03:16,62 --> 00:03:22,45
which is an extra decimal value or we can have

64
00:03:23,22 --> 00:03:24,16
an interruption

65
00:03:24,17 --> 00:03:24,7
For example,

66
00:03:24,7 --> 00:03:27,46
the conflict for you are to console is an integer

67
00:03:28,74 --> 00:03:29,0
Here,

68
00:03:29,0 --> 00:03:32,85
we can also mix recipe suitable size in kilobytes is

69
00:03:33,44 --> 00:03:40,06
2000 224 basically

70
00:03:41,84 --> 00:03:45,52
That's probably all options that are all types of options

71
00:03:45,52 --> 00:03:48,05
that accord with implements and take on

