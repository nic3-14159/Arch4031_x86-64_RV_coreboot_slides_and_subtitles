1
00:00:00,44 --> 00:00:03,05
so to sum up what we have learned so far

2
00:00:03,54 --> 00:00:08,41
Um Kay conflict exposes the conflict options dot conflict file

3
00:00:08,42 --> 00:00:11,5
to make files which makes the build system our those

4
00:00:11,5 --> 00:00:16,17
settings based on the dot confident conflict at eight file

5
00:00:16,6 --> 00:00:19,04
the make file system can decide which is the target

6
00:00:19,04 --> 00:00:21,97
board was the size of the RAM and drum,

7
00:00:22,01 --> 00:00:25,5
which source files should be built and assembled and in

8
00:00:25,5 --> 00:00:28,51
the court which nurtures should be built and which binaries

9
00:00:28,51 --> 00:00:32,05
doing plot in the final wrong image and the previous

10
00:00:32,05 --> 00:00:32,58
lessons,

11
00:00:32,59 --> 00:00:35,83
I have mentioned that we have only one make file

12
00:00:35,84 --> 00:00:39,07
and together with the K conflict decides what sources are

13
00:00:39,07 --> 00:00:44,17
compiled in Corbyn has quite a vast source tree

14
00:00:44,18 --> 00:00:48,6
So how are those old other make files dot implies

15
00:00:48,61 --> 00:00:49,16
clouded

16
00:00:49,84 --> 00:00:52,94
So the main make file is looking for them in

17
00:00:52,94 --> 00:00:57,42
past that are written into the Sanders were able at

18
00:00:57,42 --> 00:01:00,43
first this variable is defined as sub

19
00:01:00,43 --> 00:01:03,96
There's equal to dot which points to the root directory

20
00:01:04,44 --> 00:01:06,66
There's only one make on fire

21
00:01:07,14 --> 00:01:09,85
We can call it main make falling file in the

22
00:01:09,85 --> 00:01:13,16
root directory at guests included by the make file

23
00:01:15,14 --> 00:01:17,56
So in the corporate make fire system

24
00:01:17,88 --> 00:01:21,55
The dash why Suffolk's very are called classes

25
00:01:22,04 --> 00:01:25,64
The main make fighting declares a variable subjects,

26
00:01:25,65 --> 00:01:26,43
dash Y

27
00:01:26,53 --> 00:01:28,65
Some of the past present in sub

28
00:01:28,65 --> 00:01:30,09
There's dash Y

29
00:01:30,09 --> 00:01:34,06
Class defined in maine make funding represented on the slide

30
00:01:34,73 --> 00:01:35,71
The other make holdings

31
00:01:35,88 --> 00:01:40,18
Inc files use the plus equal syntax to upend

32
00:01:40,19 --> 00:01:41,62
Pass to the subjects

33
00:01:41,63 --> 00:01:45,48
Dachau wire classes when make I don't think it's included

34
00:01:45,84 --> 00:01:49,77
the make fire extend the subjects variable by subjects,

35
00:01:49,77 --> 00:01:51,45
dash by content from the cloud

36
00:01:51,46 --> 00:01:52,55
Make from that link

37
00:01:53,04 --> 00:01:55,78
This means that path to look for make found that

38
00:01:55,78 --> 00:01:56,96
in existence as well

39
00:01:57,66 --> 00:02:01,07
Wild card is a function which takes a strength with

40
00:02:01,08 --> 00:02:02,55
wealth cut as an argument

41
00:02:03,0 --> 00:02:07,52
The output of dysfunction is space separated list with names

42
00:02:07,52 --> 00:02:11,38
of existing files that matched to the pattern which was

43
00:02:11,38 --> 00:02:12,66
passed as an argument

44
00:02:13,14 --> 00:02:13,97
For example,

45
00:02:14,29 --> 00:02:20,07
the result of dollar wildcard sores that slash art slash

46
00:02:20,08 --> 00:02:20,66
as there is,

47
00:02:21,14 --> 00:02:24,89
it's a space separated list of all files found in

48
00:02:24,9 --> 00:02:28,55
source slash arc slash directory

49
00:02:30,14 --> 00:02:33,86
So let's see how does it look like and code

50
00:02:38,34 --> 00:02:38,55
Mhm

51
00:02:46,94 --> 00:02:47,15
Mhm

52
00:02:47,74 --> 00:02:47,95
Well

53
00:02:49,24 --> 00:02:49,45
Mhm

54
00:03:14,94 --> 00:03:15,15
Mhm

55
00:03:16,14 --> 00:03:16,35
Mhm

56
00:03:24,64 --> 00:03:24,96
Mhm

57
00:03:42,14 --> 00:03:42,35
Mhm

58
00:03:44,54 --> 00:03:44,75
Mhm

59
00:03:45,34 --> 00:03:45,55
Mhm

60
00:03:48,04 --> 00:03:48,24
Yeah

61
00:03:49,34 --> 00:03:49,55
Okay

62
00:03:50,04 --> 00:03:50,54
Yeah

63
00:03:54,74 --> 00:03:55,06
Mhm

64
00:04:09,34 --> 00:04:09,54
Yeah

65
00:04:10,94 --> 00:04:11,26
Mhm

66
00:04:12,84 --> 00:04:13,05
Mhm

67
00:04:13,44 --> 00:04:13,64
Yeah

68
00:04:15,04 --> 00:04:15,54
Yeah

69
00:04:21,74 --> 00:04:21,95
Mhm

70
00:04:25,54 --> 00:04:25,75
Mhm

71
00:04:28,34 --> 00:04:28,55
Mhm

72
00:04:30,54 --> 00:04:30,75
Mhm

73
00:04:32,84 --> 00:04:33,04
Yeah

74
00:04:38,24 --> 00:04:38,45
Mhm

75
00:04:43,94 --> 00:04:46,03
So let's have a look at the main main file

76
00:04:46,03 --> 00:04:47,75
in Garwood directory

77
00:04:48,24 --> 00:04:50,95
So we have a very book called the top level

78
00:04:50,96 --> 00:04:53,55
Which part the root directory of the registry

79
00:04:54,24 --> 00:04:58,0
Then this total we were able is used to include

80
00:04:58,0 --> 00:05:04,58
various make fall dot care or to extend the subjects

81
00:05:04,58 --> 00:05:05,15
class

82
00:05:06,44 --> 00:05:12,17
Then all the sub their classes evaluated to include other

83
00:05:12,17 --> 00:05:13,35
make files as well

84
00:05:15,54 --> 00:05:17,36
And let's go to the main quad dot inc

85
00:05:18,34 --> 00:05:21,17
The fact that I think we have another sub directories

86
00:05:21,17 --> 00:05:23,47
which are common for car boot,

87
00:05:23,48 --> 00:05:25,21
they are automatically included

88
00:05:25,22 --> 00:05:25,78
For example,

89
00:05:25,78 --> 00:05:29,28
like source led common web console device and a C

90
00:05:29,28 --> 00:05:29,76
P I

91
00:05:30,74 --> 00:05:30,94
Yeah

92
00:05:32,24 --> 00:05:32,88
And for example,

93
00:05:32,88 --> 00:05:36,34
if we go to one of the victories mentioned here

94
00:05:36,35 --> 00:05:38,26
let's say socially

95
00:05:38,74 --> 00:05:38,94
Yeah,

96
00:05:39,74 --> 00:05:43,16
we'll find a corresponding my friend got in there too

97
00:05:44,24 --> 00:05:45,81
Let's see we are a source lip

98
00:05:45,82 --> 00:05:51,15
Make one think now and currently we shall not

99
00:05:51,64 --> 00:05:58,46
And the subdirectories except some Jeanette subdirectory

100
00:05:59,04 --> 00:06:03,67
So if there is no other subdirectories that my father

101
00:06:03,67 --> 00:06:07,92
thinking in the case which source fights should be compiled

102
00:06:07,92 --> 00:06:10,06
into given uh program

103
00:06:10,54 --> 00:06:14,31
So we have also another classes like home station,

104
00:06:14,31 --> 00:06:15,97
wrong stage compressor,

105
00:06:16,13 --> 00:06:16,76
pulley block

106
00:06:17,24 --> 00:06:20,42
And we see that offends some source files to these

107
00:06:20,42 --> 00:06:20,86
classes

108
00:06:21,34 --> 00:06:21,55
Mhm

109
00:06:25,84 --> 00:06:28,55
So as I have mentioned earlier under make file that

110
00:06:28,56 --> 00:06:31,71
link files and deeper in the sauce tree may extend

111
00:06:31,71 --> 00:06:32,77
the subjects variables

112
00:06:32,77 --> 00:06:33,39
Well,

113
00:06:33,4 --> 00:06:36,55
this is the recursive process of including make file dot

114
00:06:36,56 --> 00:06:40,68
inc file extending search path including another file

115
00:06:40,69 --> 00:06:42,46
Anti subjects please become empty

116
00:06:43,04 --> 00:06:44,45
So to sum it up,

117
00:06:45,04 --> 00:06:47,46
we start with including the main make fall in from

118
00:06:47,46 --> 00:06:51,41
root directory and then the subject is extended by subs

119
00:06:51,41 --> 00:06:52,53
dash y from maine

120
00:06:52,53 --> 00:06:53,46
Make fall dot inc

121
00:06:54,04 --> 00:06:54,76
It contains

122
00:06:54,77 --> 00:06:58,63
As to other core directories which should be research for

123
00:06:58,63 --> 00:07:00,16
make file dot files

124
00:07:00,64 --> 00:07:04,65
One of the paths is subtracted from subjects variable and

125
00:07:04,65 --> 00:07:08,79
we search for medical dot center if it exists it

126
00:07:08,79 --> 00:07:12,92
gets included if it contains subjects dashing why it's further

127
00:07:12,92 --> 00:07:14,86
extends the subjects variable

128
00:07:15,84 --> 00:07:19,76
The process finishes when the subjects variable becomes empty

129
00:07:21,04 --> 00:07:24,02
So as I have mentioned the previous lessons,

130
00:07:24,1 --> 00:07:27,46
there are also other classes defined the make financing files

131
00:07:27,91 --> 00:07:30,79
The most important could be the ram stage from stage

132
00:07:30,79 --> 00:07:34,78
and will block their names correspond to corporate boot stages

133
00:07:34,78 --> 00:07:36,15
to which they are related

134
00:07:37,04 --> 00:07:40,09
So a short reminder about the current status from previous

135
00:07:40,09 --> 00:07:40,56
chapter

136
00:07:41,14 --> 00:07:44,59
So the book block class uh contains the service files

137
00:07:44,6 --> 00:07:48,36
that uh consists of the stage

138
00:07:48,83 --> 00:07:52,33
The bulldog stages contains the recent victor and the caches

139
00:07:52,33 --> 00:07:53,15
round set up

140
00:07:53,84 --> 00:07:57,79
The rem stage is responsible for at least reconciliation at

141
00:07:57,79 --> 00:07:58,66
memorial set up,

142
00:07:59,44 --> 00:08:00,84
postcard tears,

143
00:08:00,84 --> 00:08:05,67
dedication some environment down from stage performs normal device setup

144
00:08:05,77 --> 00:08:09,26
and main board configuration and the last stages payment,

145
00:08:09,64 --> 00:08:12,95
which is typically the operating system or application loader

146
00:08:14,35 --> 00:08:16,94
They may founded in files laying deeper in this industry

147
00:08:17,36 --> 00:08:17,92
For example,

148
00:08:17,92 --> 00:08:20,9
in the main world directory usually add some files that

149
00:08:20,9 --> 00:08:22,72
should be built to those classes

150
00:08:23,21 --> 00:08:26,04
In this example we instruct the build system to build

151
00:08:26,04 --> 00:08:29,22
educator and Iraqi wrote as a part of our um

152
00:08:29,22 --> 00:08:29,95
stick block

153
00:08:31,24 --> 00:08:32,99
After the build process finishes

154
00:08:33,0 --> 00:08:35,78
You can see the output files are also split by

155
00:08:35,78 --> 00:08:36,56
a glass

156
00:08:36,57 --> 00:08:39,5
There are also directories such as Bill from stage,

157
00:08:39,5 --> 00:08:41,95
bit rum stage with good luck etcetera,

158
00:08:42,44 --> 00:08:45,87
which contained the compiled object files that were privacy appended

159
00:08:46,31 --> 00:08:47,45
to the given class

160
00:08:49,14 --> 00:08:51,26
So let's have a look at an example

161
00:08:51,84 --> 00:08:52,25
Um,

162
00:08:52,26 --> 00:08:59,15
make follow in 13 files that include the source files

163
00:08:59,74 --> 00:09:01,04
So for example,

164
00:09:01,05 --> 00:09:04,96
as a purposely showing you the sources make floating file

165
00:09:05,34 --> 00:09:08,77
you can see that there are certain libraries and clubs

166
00:09:08,77 --> 00:09:09,76
and boot block

167
00:09:10,92 --> 00:09:11,65
For example,

168
00:09:11,65 --> 00:09:12,66
for the first stage,

169
00:09:12,67 --> 00:09:16,35
which is used to we would you have a separate

170
00:09:16,6 --> 00:09:21,36
in class for a long stage class and for the

171
00:09:21,36 --> 00:09:22,39
rem stage as well,

172
00:09:22,4 --> 00:09:27,6
we have we see some specific sources that are compiled

173
00:09:27,6 --> 00:09:28,36
to the round stage

174
00:09:29,34 --> 00:09:30,25
Additionally,

175
00:09:30,25 --> 00:09:34,2
you can see that certain source files can be compiled

176
00:09:34,2 --> 00:09:36,15
dependent on the K conflict options

177
00:09:36,54 --> 00:09:40,78
So this very able is included from the K conflict

178
00:09:40,78 --> 00:09:45,69
filed result file and I've always to Why?

179
00:09:45,7 --> 00:09:48,85
Which expands the around ST dash to the ground state

180
00:09:48,85 --> 00:09:49,46
dash y

181
00:09:50,04 --> 00:09:53,75
That means the Semen console is compared to germs,

182
00:09:53,75 --> 00:09:59,3
which only if the council cBMM is selected to a

183
00:09:59,3 --> 00:10:01,46
positive value because it is a bullying

184
00:10:06,84 --> 00:10:10,3
So to show another point of connection between the micro

185
00:10:10,3 --> 00:10:11,25
I can conflict

186
00:10:11,74 --> 00:10:13,93
Let's have a look at example,

187
00:10:13,94 --> 00:10:18,56
K coughing from uh emulation board for um you and

188
00:10:18,56 --> 00:10:19,75
chips and you're 35

189
00:10:20,34 --> 00:10:23,06
So I'm picking our mind works in many conflict

190
00:10:23,64 --> 00:10:26,82
It doesn't end up with just one conflict selection being

191
00:10:26,83 --> 00:10:27,36
changed

192
00:10:27,74 --> 00:10:29,92
If you take a look at the corresponding K conflict

193
00:10:29,92 --> 00:10:30,6
foul,

194
00:10:30,61 --> 00:10:35,55
you can see a set of configuration options and the

195
00:10:35,56 --> 00:10:38,44
values that are selected after this particular main work has

196
00:10:38,44 --> 00:10:41,69
been paid and those selected by the main working conflict

197
00:10:41,69 --> 00:10:45,34
select other dependent options throughout the whole horrible source

198
00:10:46,44 --> 00:10:46,95
Um,

199
00:10:48,04 --> 00:10:52,71
it is simplified because one does not simply remember what

200
00:10:52,71 --> 00:10:55,61
options should be set for a given member

201
00:10:56,14 --> 00:10:59,12
So the relation chain others to simplify things on the

202
00:10:59,12 --> 00:11:02,7
main board level while keep the complex thing things in

203
00:11:02,7 --> 00:11:04,64
the Okay convicts,

204
00:11:04,65 --> 00:11:05,29
for example,

205
00:11:05,29 --> 00:11:08,32
for the associate for the north bridge or the salvage

206
00:11:08,32 --> 00:11:09,45
for Cuban platform

207
00:11:11,54 --> 00:11:15,56
So on another example of make a concussion connection,

208
00:11:15,57 --> 00:11:20,36
maybe the inclusion of the member specific make for nothing

209
00:11:21,34 --> 00:11:22,48
The previous slides,

210
00:11:22,49 --> 00:11:24,3
I have shown the subject

211
00:11:24,3 --> 00:11:27,61
That's why declaration and make in the main make file

212
00:11:27,61 --> 00:11:31,58
dot in fire make fire dot inc at source

213
00:11:31,58 --> 00:11:35,86
Main board and variable main board duties subsidies dash y

214
00:11:36,54 --> 00:11:39,5
You can also see that the main party available comes

215
00:11:39,5 --> 00:11:43,17
from the conflict Member do which is selected by K

216
00:11:43,17 --> 00:11:47,12
conflict when we sent them The given main board

217
00:11:47,27 --> 00:11:47,5
Now,

218
00:11:47,5 --> 00:11:48,12
for example,

219
00:11:48,12 --> 00:11:49,36
to some relation um you

220
00:11:49,36 --> 00:11:50,46
Q 35

221
00:11:51,04 --> 00:11:54,29
This this makes that only member direction for this particular

222
00:11:54,29 --> 00:11:57,55
board is subject to the subdirectory variable

223
00:11:59,34 --> 00:12:01,55
So let's see how it looks in the code

224
00:12:05,84 --> 00:12:11,49
Let's go to the main board directory in relation And

225
00:12:11,49 --> 00:12:12,96
come over to 35

226
00:12:14,64 --> 00:12:20,85
So you can say that there are various k conflict

227
00:12:22,04 --> 00:12:26,75
conditions like have both emulation pmU X 86 55

228
00:12:27,14 --> 00:12:31,79
This option comes from k coughing dot name,

229
00:12:31,8 --> 00:12:32,36
for example,

230
00:12:33,14 --> 00:12:38,44
which is automatically included by the k coughing on the

231
00:12:38,44 --> 00:12:39,26
higher level

232
00:12:39,27 --> 00:12:44,57
So if we have the emulation vendor we have and

233
00:12:44,63 --> 00:12:48,53
a coffee and take off dot name for each for

234
00:12:48,54 --> 00:12:50,76
each vendor in corporate

235
00:12:51,54 --> 00:12:56,62
So we define the vendor and main board model and

236
00:12:56,62 --> 00:12:59,8
as well we source the conflict of name and that

237
00:12:59,81 --> 00:13:02,85
conflict files from each main board model

238
00:13:05,03 --> 00:13:08,97
This way we only have more specific options for a

239
00:13:08,97 --> 00:13:10,96
single main board we select

240
00:13:12,34 --> 00:13:13,5
So for example,

241
00:13:14,0 --> 00:13:19,2
let's see our emulation boards for ke emu sex coverage

242
00:13:19,2 --> 00:13:23,35
intel um and this mother daughter so rich

243
00:13:23,94 --> 00:13:27,54
So it already tells us where to look for the

244
00:13:27,54 --> 00:13:30,26
K conflict which is responsible for defining this option

245
00:13:31,04 --> 00:13:32,95
We may look for the source

246
00:13:32,96 --> 00:13:40,16
So bridge until and here we have it's this one

247
00:13:42,61 --> 00:13:47,06
And as we can see the more complex options which

248
00:13:47,44 --> 00:13:50,57
should be a main board diagnostic are included here

249
00:13:50,73 --> 00:13:52,96
There are checks related to the soul bridge

250
00:13:53,64 --> 00:13:58,48
So we don't have to know which options are specific

251
00:13:58,48 --> 00:13:59,18
to solve bridge

252
00:13:59,18 --> 00:14:02,29
We just said the so bridge the main body is

253
00:14:02,29 --> 00:14:02,76
using

254
00:14:05,14 --> 00:14:07,02
And so we have other options,

255
00:14:07,02 --> 00:14:14,16
like some symbols over configuration have some bartram size defined

256
00:14:15,29 --> 00:14:23,16
and other stuff besides the uh the heart selection of

257
00:14:23,54 --> 00:14:25,66
the specific options,

258
00:14:26,04 --> 00:14:31,98
we can define certain defaults which are selected when given

259
00:14:31,99 --> 00:14:32,92
option is enabled

260
00:14:32,92 --> 00:14:35,87
So if you enable we put for example you can

261
00:14:35,87 --> 00:14:38,93
select the options that are specific for this port as

262
00:14:38,93 --> 00:14:39,36
well

263
00:14:44,46 --> 00:14:47,58
And here we also can see that the main border

264
00:14:47,58 --> 00:14:48,56
is set here

265
00:14:49,84 --> 00:14:56,45
This main but they're contains the path or rather the

266
00:14:56,84 --> 00:14:59,95
part of the pen that is used to construct various

267
00:14:59,95 --> 00:15:01,76
other paths in the immune system

268
00:15:02,92 --> 00:15:05,68
As we can see there are many references references across

269
00:15:05,68 --> 00:15:08,54
the across the street

270
00:15:10,14 --> 00:15:11,76
Going back to the mic files,

271
00:15:12,14 --> 00:15:15,06
we can see that the main border is avoided here

272
00:15:19,11 --> 00:15:24,98
and it's just all over the the south street and

273
00:15:24,98 --> 00:15:27,65
this is including of course as a subdirectory as well

274
00:15:28,24 --> 00:15:28,45
Mhm

275
00:15:31,14 --> 00:15:33,27
So if we take a look back,

276
00:15:33,28 --> 00:15:36,41
we will see that the main menu to add all

277
00:15:36,42 --> 00:15:38,06
associates nor bridges,

278
00:15:38,06 --> 00:15:39,03
salt bridges,

279
00:15:39,04 --> 00:15:41,72
etcetera directories to the sun,

280
00:15:41,73 --> 00:15:42,9
their subjects

281
00:15:42,9 --> 00:15:43,5
That's right

282
00:15:43,55 --> 00:15:45,56
Regardless of the chosen platform

283
00:15:46,04 --> 00:15:49,22
But if you open one of these make fight dot

284
00:15:49,23 --> 00:15:51,13
x files from those directories,

285
00:15:51,15 --> 00:15:54,27
we will see that there is an appropriate if equal

286
00:15:54,27 --> 00:15:56,05
check at the beginning of the file

287
00:15:56,84 --> 00:16:01,25
This equal check ensures that only uh this content,

288
00:16:01,25 --> 00:16:03,91
its content is only exposed to the main make file

289
00:16:03,92 --> 00:16:07,37
if the corresponding conflict option has been selected area during

290
00:16:07,37 --> 00:16:08,86
the build configuration phase,

291
00:16:10,04 --> 00:16:10,79
in such way,

292
00:16:10,79 --> 00:16:13,95
only the system on chip coverage or nor bridge specific

293
00:16:13,95 --> 00:16:16,14
code and options are included

294
00:16:16,36 --> 00:16:17,36
In our case,

295
00:16:17,36 --> 00:16:18,85
it will be the intel server,

296
00:16:18,85 --> 00:16:20,96
which I have mentioned in the previous lesson

297
00:16:21,74 --> 00:16:24,6
So let's have a look how they make fun that

298
00:16:24,6 --> 00:16:27,05
think fire looks in for this sort bridge

299
00:16:29,24 --> 00:16:29,45
Mhm

300
00:16:30,24 --> 00:16:34,89
So um opening the makeup in from the south bridge

301
00:16:34,92 --> 00:16:40,07
and we see this is the f equal conditions that

302
00:16:40,08 --> 00:16:44,77
ensures only the content of this make part is included

303
00:16:45,54 --> 00:16:47,07
when the sub bridges enabled

304
00:16:47,94 --> 00:16:51,98
So we have the corresponding sources uh here,

305
00:16:51,99 --> 00:16:54,36
which are included for the specific salvage

