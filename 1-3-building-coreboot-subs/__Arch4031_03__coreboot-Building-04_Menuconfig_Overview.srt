1
00:00:00,14 --> 00:00:03,01
Okay now I will show you how to execute uh

2
00:00:03,02 --> 00:00:05,36
menu conflict to open the conflagration in the program

3
00:00:05,94 --> 00:00:10,32
I will also describe what the corporate many conflict consists

4
00:00:10,32 --> 00:00:13,83
of and what can be configured in each of these

5
00:00:13,83 --> 00:00:16,49
many categories as I will be going through the menu

6
00:00:16,49 --> 00:00:16,87
conflict

7
00:00:16,87 --> 00:00:18,76
I will explain all these options

8
00:00:19,24 --> 00:00:19,5
Okay,

9
00:00:19,5 --> 00:00:24,56
so let's execute make menu conflict and to pull display

10
00:00:24,94 --> 00:00:30,74
the configuration money which is used to set up our

11
00:00:30,74 --> 00:00:33,56
coral built and starting from general set up

12
00:00:34,14 --> 00:00:40,46
So here we can see some basic settings for compiler

13
00:00:40,94 --> 00:00:46,55
uh and other uh secondary parcels computer stuff

14
00:00:47,14 --> 00:00:48,3
I have some symbols,

15
00:00:48,3 --> 00:00:49,41
option tables,

16
00:00:49,42 --> 00:00:50,93
compression settings,

17
00:00:50,94 --> 00:00:54,42
uh whether it's included that conflict file into the final

18
00:00:54,42 --> 00:00:55,15
round image

19
00:00:55,74 --> 00:01:00,96
Uh some blocks configuration uh Just science tizer and stuff

20
00:01:01,44 --> 00:01:04,14
You can also add a boot splash image here,

21
00:01:04,15 --> 00:01:08,56
uh or where the code should be stashed

22
00:01:08,56 --> 00:01:09,24
For for example,

23
00:01:09,24 --> 00:01:12,45
for the uh resume from memory

24
00:01:13,14 --> 00:01:16,79
Also you can set a local version string here which

25
00:01:16,79 --> 00:01:20,14
will um finally appear in the S and bios,

26
00:01:20,15 --> 00:01:21,19
for example,

27
00:01:21,2 --> 00:01:23,06
50 in the bios string,

28
00:01:23,94 --> 00:01:25,86
version and stuff

29
00:01:27,94 --> 00:01:28,21
Okay,

30
00:01:28,22 --> 00:01:31,48
um let's go to the next one which is a

31
00:01:31,49 --> 00:01:32,25
main board

32
00:01:32,26 --> 00:01:36,62
Here is the probably the most often used and most

33
00:01:36,62 --> 00:01:41,64
useful said many because it's always us to set the

34
00:01:41,64 --> 00:01:42,34
main board

35
00:01:42,35 --> 00:01:46,57
Um you want to build their the core,

36
00:01:46,57 --> 00:01:49,96
but for for example,

37
00:01:49,96 --> 00:01:51,28
may choose,

38
00:01:51,29 --> 00:01:57,4
let's see directly of sex and as we can see

39
00:01:57,41 --> 00:02:00,46
uh we have just selected inhumane board

40
00:02:01,14 --> 00:02:03,48
Um can select some room size for it,

41
00:02:03,49 --> 00:02:05,27
let's say eight MB

42
00:02:05,84 --> 00:02:10,59
You can select some system power state after the uh

43
00:02:10,6 --> 00:02:12,6
power has failed

44
00:02:12,61 --> 00:02:16,25
We can also provide some flash map description file,

45
00:02:16,64 --> 00:02:20,26
which I also described you in previous lessons

46
00:02:21,04 --> 00:02:24,54
So here past the path to the FMD file with

47
00:02:24,55 --> 00:02:27,16
describes religions and their sizes and stuff

48
00:02:28,14 --> 00:02:31,93
Um here we may define the size of cbs classified

49
00:02:31,93 --> 00:02:32,71
system

50
00:02:32,72 --> 00:02:35,84
This typically depends on the size of the bias region

51
00:02:35,84 --> 00:02:37,16
on their platforms

52
00:02:37,54 --> 00:02:41,2
So here it will be let's say six MB

53
00:02:42,64 --> 00:02:44,95
Um and let's move to the next category which is

54
00:02:44,95 --> 00:02:51,38
chip set trips that contains mainly uh the uh associate

55
00:02:51,38 --> 00:02:54,16
specific stuff like the micro architectural stuff

56
00:02:54,64 --> 00:02:57,27
And here we can see you can enable some options

57
00:02:57,27 --> 00:02:58,48
like hyper threading

58
00:02:58,49 --> 00:03:03,37
Um these options like indicate whether the platform can have

59
00:03:03,38 --> 00:03:06,1
a replaceable CPU with Skylink,

60
00:03:06,1 --> 00:03:06,73
CPU cable,

61
00:03:06,74 --> 00:03:08,2
dual core or quad core

62
00:03:08,21 --> 00:03:11,78
This option is just indicate whether additional micro code files

63
00:03:11,78 --> 00:03:12,46
should be included

64
00:03:13,94 --> 00:03:18,26
There's also assumptions like for the for the M

65
00:03:18,26 --> 00:03:18,8
E

66
00:03:18,81 --> 00:03:26,26
Regions for the read write updates of the army um

67
00:03:26,64 --> 00:03:29,96
whether to perform on mps sensation by FSP and stuff

68
00:03:30,74 --> 00:03:34,55
There's uh the other options like we can university ex

69
00:03:34,56 --> 00:03:35,9
uh the A C

70
00:03:35,9 --> 00:03:36,08
P

71
00:03:36,08 --> 00:03:36,16
I

72
00:03:36,16 --> 00:03:37,0
Timer,

73
00:03:37,01 --> 00:03:41,66
uh legacy pete timer and other stuff

74
00:03:44,24 --> 00:03:49,45
Also Hero may set uh main future control register,

75
00:03:49,45 --> 00:03:51,95
log bit and virtualization

76
00:03:52,84 --> 00:03:56,55
He recommends also select which Microsoft file sharing protocol,

77
00:03:56,55 --> 00:04:01,63
whether we generated from the available tree or actually called

78
00:04:01,63 --> 00:04:02,76
external binaries

79
00:04:02,81 --> 00:04:05,42
We can also add some five descriptor

80
00:04:05,42 --> 00:04:07,46
If we have one image,

81
00:04:07,84 --> 00:04:11,46
you can also apply and make cleaner if you want

82
00:04:12,14 --> 00:04:19,13
Um We can also select which put back to load

83
00:04:19,14 --> 00:04:22,33
which other stage actually to load depending on

84
00:04:22,33 --> 00:04:24,96
The symbols are always we can load the fallback

85
00:04:26,34 --> 00:04:31,47
Um The fallback means the prefix which is appended uh

86
00:04:31,48 --> 00:04:35,76
to the the stage name in the CPF s can

87
00:04:35,76 --> 00:04:37,21
also protect some flash regions

88
00:04:37,21 --> 00:04:40,53
Like we can look at me antics omitted the region

89
00:04:40,54 --> 00:04:42,36
or use the preset values

90
00:04:42,37 --> 00:04:46,91
So basically it says whether the FD tool which is

91
00:04:46,91 --> 00:04:50,58
available in car boot should log the flash descriptor which

92
00:04:50,58 --> 00:04:52,26
you provide here in the file

93
00:04:54,14 --> 00:04:57,75
So next section of the devices section and here we

94
00:04:57,75 --> 00:05:02,25
mainly have options for the graphics device and piece of

95
00:05:02,25 --> 00:05:03,23
extra devices

96
00:05:03,24 --> 00:05:07,46
So basically uh we select here which kind of uh

97
00:05:07,84 --> 00:05:11,68
operation we can performed to encourage the graphics,

98
00:05:11,68 --> 00:05:13,26
like we can use the open source sleep,

99
00:05:13,84 --> 00:05:14,33
jfx,

100
00:05:14,33 --> 00:05:15,85
innit library,

101
00:05:16,34 --> 00:05:20,07
which is right in spark ada language or we can

102
00:05:20,07 --> 00:05:25,72
alternatively around vega optionals around the GOP driver from fsB

103
00:05:25,97 --> 00:05:27,46
or I didn't do nothing

104
00:05:29,14 --> 00:05:33,91
You can also set what the frame buffer should be

105
00:05:33,91 --> 00:05:36,64
like whether we want to show the graphical book,

106
00:05:36,64 --> 00:05:37,46
splash and stuff

107
00:05:38,74 --> 00:05:39,36
Also,

108
00:05:39,36 --> 00:05:42,6
there is a new option which allows us to configure

109
00:05:42,6 --> 00:05:47,29
the bassmaster pits um incorporate for the devices

110
00:05:47,3 --> 00:05:51,24
Can also enable some sex is power management features like

111
00:05:51,24 --> 00:05:53,57
10 o'clock spm sub states

112
00:05:53,57 --> 00:05:57,14
We can also enable Hot black and then we see

113
00:05:57,15 --> 00:05:59,19
how many buses we want to reserve and how much

114
00:05:59,19 --> 00:06:01,46
memory you want to reserve for these buses and stuff

115
00:06:02,84 --> 00:06:06,19
We can also override the pC subsystem under the device

116
00:06:06,19 --> 00:06:09,05
ID and include BJ bias images

117
00:06:10,14 --> 00:06:12,11
You can include LGBT binary,

118
00:06:12,12 --> 00:06:14,55
cannot additional um,

119
00:06:15,44 --> 00:06:17,02
B J images as well

120
00:06:17,09 --> 00:06:20,04
If we have more than one graphics controller in the

121
00:06:20,04 --> 00:06:20,66
system,

122
00:06:21,74 --> 00:06:22,98
generic drivers,

123
00:06:22,99 --> 00:06:28,29
generic drivers described the options that are available for for

124
00:06:28,29 --> 00:06:33,05
the platform from the generic corporate drivers which are um

125
00:06:33,06 --> 00:06:36,16
common on across many platforms

126
00:06:36,54 --> 00:06:42,15
You cannot enable some uh event lock um in the

127
00:06:42,16 --> 00:06:47,53
flash you can have some limited data store,

128
00:06:47,54 --> 00:06:52,53
which is a data stored in the spi flash which

129
00:06:52,54 --> 00:06:55,15
uses sME interacts too,

130
00:06:55,64 --> 00:07:01,72
communicate to this start can also enable some other additional

131
00:07:01,72 --> 00:07:04,26
models like Oxford X P C E

132
00:07:04,64 --> 00:07:08,06
You can support for V P D

133
00:07:08,65 --> 00:07:14,38
BPD is structure defined by their christmas project,

134
00:07:14,39 --> 00:07:19,24
which is just widely to start some uh configuration for

135
00:07:19,24 --> 00:07:20,06
the platforms,

136
00:07:20,07 --> 00:07:20,87
chromebooks

137
00:07:21,19 --> 00:07:26,46
We can include some Syrian Lebanese cFS and understand here

138
00:07:26,47 --> 00:07:33,45
We also may configure how we should include that fsp

139
00:07:33,45 --> 00:07:34,01
binaries

140
00:07:34,01 --> 00:07:37,53
You can either use the have sp repo or attend

141
00:07:37,54 --> 00:07:40,08
some external physically binaries,

142
00:07:40,08 --> 00:07:42,26
which we provide the path to

143
00:07:43,34 --> 00:07:45,64
We can also enable the logo,

144
00:07:45,65 --> 00:07:48,56
which is a BMP file past to the FsB to

145
00:07:48,56 --> 00:07:50,96
be displayed during the synchronization,

146
00:07:51,44 --> 00:07:54,82
May initialize the PS two keyboard and other stuff as

147
00:07:54,82 --> 00:07:55,06
well

148
00:07:56,84 --> 00:07:58,46
Some security

149
00:07:58,84 --> 00:08:01,68
This is probably the most interesting uh,

150
00:08:01,69 --> 00:08:02,26
menu

151
00:08:03,24 --> 00:08:08,25
So it consists now for a few options here,

152
00:08:08,74 --> 00:08:12,35
like verified boot in short of a boot

153
00:08:12,74 --> 00:08:17,05
This is a library developed by the chromium promo?

154
00:08:17,05 --> 00:08:23,23
S project for the sake of secure boot of the

155
00:08:23,23 --> 00:08:28,7
chromebook devices and it just uh just does the cryptographic

156
00:08:28,71 --> 00:08:31,15
verification of various components

157
00:08:31,74 --> 00:08:36,4
I'm going to go into much details because it's very

158
00:08:36,41 --> 00:08:37,27
very complex

159
00:08:37,27 --> 00:08:41,36
Many and has a lot of various options which most

160
00:08:41,36 --> 00:08:44,5
of them is not using our regular platforms that are

161
00:08:44,5 --> 00:08:45,36
not chromebooks

162
00:08:46,24 --> 00:08:49,36
We can also enable trusted platform module here huck

163
00:08:49,94 --> 00:08:53,15
Example of able we have available option for TPM 2.0

164
00:08:53,74 --> 00:08:56,96
can enable mirrored boot and and stuff

165
00:08:58,54 --> 00:09:01,91
The emergency physician is quite simple because the only option

166
00:09:01,91 --> 00:09:06,16
right now here is adoption to clear all freed Iran

167
00:09:06,16 --> 00:09:10,56
on regular would it is very useful for example if

168
00:09:10,56 --> 00:09:16,07
we have some um secrets in the memory especially when

169
00:09:16,07 --> 00:09:19,49
we have for example the trusted execution technology which enforces

170
00:09:19,49 --> 00:09:22,72
us to clear the memory um if it detects any

171
00:09:22,72 --> 00:09:29,06
secrets may be leaked bent one they wanted just for

172
00:09:29,84 --> 00:09:30,56
security

173
00:09:30,94 --> 00:09:34,24
We have also added directly support many which allows us

174
00:09:34,24 --> 00:09:39,45
to include the uh A CMS in the image enables

175
00:09:39,45 --> 00:09:40,07
verbose logging

176
00:09:40,07 --> 00:09:43,71
Can basically execute the 60 driver and implemented,

177
00:09:44,84 --> 00:09:45,78
correct?

178
00:09:45,79 --> 00:09:49,13
Because also as a might transform inter integrated to car

179
00:09:49,13 --> 00:09:54,25
boot so you may select this option and basically mm

180
00:09:54,64 --> 00:09:58,05
provide the path to sDM binary which would be integrated

181
00:09:58,05 --> 00:10:01,16
and loaded into S and M

182
00:10:02,84 --> 00:10:07,69
We have also the boot protection mhm many which allows

183
00:10:07,69 --> 00:10:10,5
us to set the spi flash log beats which are

184
00:10:10,5 --> 00:10:16,86
specific to given controller of or spy class ship council

185
00:10:17,34 --> 00:10:22,09
It's basically menu for various the banking options are like

186
00:10:22,1 --> 00:10:25,26
where the core but consult should be out with like

187
00:10:25,94 --> 00:10:27,75
from which stages,

188
00:10:28,43 --> 00:10:30,95
what kind of serial port to?

189
00:10:31,34 --> 00:10:31,66
Uh huh

190
00:10:32,14 --> 00:10:34,2
Serial port or other consults

191
00:10:34,21 --> 00:10:37,12
They want to enable like we can have a speaker

192
00:10:37,12 --> 00:10:41,56
modem or network console over compatible adapter

193
00:10:41,94 --> 00:10:44,74
Or you can have spi flash consultant which just write

194
00:10:44,75 --> 00:10:45,96
the spi flash there,

195
00:10:46,71 --> 00:10:48,09
the output of the commands,

196
00:10:48,41 --> 00:10:52,53
the output of the corporate locks

197
00:10:52,54 --> 00:10:54,85
We can also configure the back level

198
00:10:55,24 --> 00:10:59,18
You can compute the post codes which can be for

199
00:10:59,18 --> 00:11:03,76
example regional LPC interface for us with a daybook postcard

200
00:11:05,44 --> 00:11:11,29
system tables are practice practically Emini for generating cause bios

201
00:11:11,29 --> 00:11:12,04
tables

202
00:11:12,05 --> 00:11:14,64
Some platforms may also have the I

203
00:11:14,64 --> 00:11:14,82
R

204
00:11:14,82 --> 00:11:15,12
Q

205
00:11:15,12 --> 00:11:19,4
Table or mp table for generation and other stuff

206
00:11:19,41 --> 00:11:23,6
You basically can sit here what the sm bios should

207
00:11:23,6 --> 00:11:27,38
be populated with find out

208
00:11:27,39 --> 00:11:32,4
Um I'm not very important many

209
00:11:32,54 --> 00:11:36,23
Many because here may choose the final application which is

210
00:11:36,23 --> 00:11:40,85
loved by car boat and as I have previously mhm

211
00:11:41,24 --> 00:11:44,94
described we have various pillows to to choose like Tana

212
00:11:44,94 --> 00:11:45,53
corpulent,

213
00:11:45,54 --> 00:11:45,97
Lennox,

214
00:11:45,97 --> 00:11:46,92
Lewis boot,

215
00:11:46,93 --> 00:11:47,85
Sebelius,

216
00:11:47,86 --> 00:11:49,12
uh average

217
00:11:49,13 --> 00:11:55,56
Uh you could grab to follow or another executable payload

218
00:11:56,94 --> 00:12:01,91
Um can also have various other pillow specific options here

219
00:12:01,91 --> 00:12:03,96
like some suppliers configuration

220
00:12:04,64 --> 00:12:08,37
Um but if we choose for example the diana core

221
00:12:08,38 --> 00:12:12,26
we will have time across specific options like which kind

222
00:12:12,26 --> 00:12:13,11
of Tana,

223
00:12:13,11 --> 00:12:14,66
corpulent flavor we want,

224
00:12:15,44 --> 00:12:18,95
which target architecture or really step like build and stop

225
00:12:20,24 --> 00:12:20,77
also

226
00:12:20,77 --> 00:12:25,6
We may select to build a pixie Rome

227
00:12:25,61 --> 00:12:29,67
You can use a pixie open source implementation to build

228
00:12:29,67 --> 00:12:29,96
it

229
00:12:30,74 --> 00:12:34,78
Or we may add some secondary payloads which also described

230
00:12:34,79 --> 00:12:38,61
earlier core implement as tent here

231
00:12:38,61 --> 00:12:41,85
We may even select the arab vision for the longest

232
00:12:43,44 --> 00:12:43,69
Okay

233
00:12:43,69 --> 00:12:46,19
And the last one is the banking manual,

234
00:12:46,2 --> 00:12:52,15
which allows us to enable even more messages to be

235
00:12:52,15 --> 00:13:00,1
displayed besides those which are which are printed by default

236
00:13:00,11 --> 00:13:01,06
in the car boot

237
00:13:01,07 --> 00:13:04,35
The councilman which is configured by the debacle and the

238
00:13:04,35 --> 00:13:05,05
council me

239
00:13:05,74 --> 00:13:08,47
And it allows us to offer some verbal messages

240
00:13:08,48 --> 00:13:09,23
For example,

241
00:13:09,23 --> 00:13:11,87
we may have a Samantha banging when we select this

242
00:13:11,87 --> 00:13:12,46
option

243
00:13:12,84 --> 00:13:15,84
Ah you can have some verbose city fast,

244
00:13:15,85 --> 00:13:17,56
the debug messages

245
00:13:18,04 --> 00:13:21,55
So whenever and access to cfs is done

246
00:13:22,24 --> 00:13:25,75
The council will print and additional uh the bugging messages

247
00:13:25,76 --> 00:13:30,28
Uh when for example scanning the Cfs for the requested

248
00:13:30,28 --> 00:13:30,67
file,

249
00:13:30,68 --> 00:13:35,86
you can also display more information about sp like here

250
00:13:36,24 --> 00:13:41,08
Uh You can also uh display promoting the great expression

251
00:13:41,08 --> 00:13:43,46
information from the fsp and stuff

252
00:13:44,44 --> 00:13:45,38
For example,

253
00:13:45,39 --> 00:13:50,6
we may also configured image for The different um 100

254
00:13:50,6 --> 00:13:55,67
emulator or for example compatible daybook coating ada sources,

255
00:13:55,67 --> 00:13:59,31
which is especially useful for dependent to leave the Fx

256
00:13:59,31 --> 00:13:59,76
in it

257
00:14:00,74 --> 00:14:02,57
And we have for example,

258
00:14:02,57 --> 00:14:05,08
spi flash debug messages when we want to do back

259
00:14:05,09 --> 00:14:10,35
um spi flash access for the chip set

260
00:14:12,64 --> 00:14:13,26
Okay

261
00:14:13,27 --> 00:14:19,36
Uh I will leave different investigation of these options too

262
00:14:19,84 --> 00:14:20,56
to you

263
00:14:21,14 --> 00:14:26,87
Just a quick um it's a quick reminder

264
00:14:26,88 --> 00:14:29,88
You may use the slash button to to search for

265
00:14:29,88 --> 00:14:35,11
a configuration parameter or press the question mark to for

266
00:14:35,11 --> 00:14:35,53
example,

267
00:14:35,53 --> 00:14:38,36
display the help of give an option to give you

268
00:14:38,94 --> 00:14:42,68
moreover viewing details what to give an option really does

269
00:14:42,69 --> 00:14:43,76
well selected

270
00:14:44,34 --> 00:14:44,68
No,

271
00:14:44,68 --> 00:14:46,11
that's not always not

272
00:14:46,12 --> 00:14:52,48
All options may have may have some help like um

273
00:14:52,49 --> 00:14:53,25
let's see

274
00:14:54,24 --> 00:14:54,44
Yeah,

275
00:14:54,94 --> 00:14:56,11
for example,

276
00:14:56,94 --> 00:14:58,16
Superior anti pie

277
00:14:58,64 --> 00:15:02,13
And as you can see this is uh an internal

278
00:15:02,13 --> 00:15:06,56
option which is selected by some given Cpus and micro

279
00:15:06,56 --> 00:15:11,38
architectures and this is basically are not mean to to

280
00:15:11,38 --> 00:15:13,84
be selected by by many conflicts

281
00:15:13,84 --> 00:15:15,95
So they have no uh,

282
00:15:16,84 --> 00:15:17,46
section

283
00:15:20,64 --> 00:15:21,1
Okay,

284
00:15:21,11 --> 00:15:25,55
so that would conclude the many conflict um,

285
00:15:28,23 --> 00:15:34,9
the man you can't speak section so please try it

286
00:15:34,9 --> 00:15:35,67
yourself,

287
00:15:35,68 --> 00:15:38,08
go through the menu and try to explore as much

288
00:15:38,08 --> 00:15:41,46
as you can to learn how it looks like

