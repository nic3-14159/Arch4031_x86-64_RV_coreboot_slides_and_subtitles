1
00:00:00,84 --> 00:00:04,09
So what the CP first really is the CBF s

2
00:00:04,1 --> 00:00:06,36
stands for car boot file system

3
00:00:06,84 --> 00:00:09,86
It has a similar concept to fight system,

4
00:00:10,24 --> 00:00:12,66
but in fact it is not one of them

5
00:00:13,04 --> 00:00:17,11
It's rather a scheme for managing independent binary data,

6
00:00:17,12 --> 00:00:20,15
so called files located in a single room image

7
00:00:21,03 --> 00:00:23,28
It is meant to be flashed on RAM chips

8
00:00:23,54 --> 00:00:27,5
Its main property that is that it's really only it

9
00:00:27,5 --> 00:00:30,89
should be assembled off target from some data trunks into

10
00:00:30,89 --> 00:00:32,16
a single wrong image

11
00:00:32,74 --> 00:00:35,56
Then it can be flashed on a target from chip

12
00:00:36,24 --> 00:00:40,11
utility that is capable of assembling such image is called

13
00:00:40,11 --> 00:00:43,56
the cbf esto and will be discussed in next lesson

14
00:00:44,24 --> 00:00:44,98
Also,

15
00:00:44,99 --> 00:00:48,02
there is no such thing as directory structure even when

16
00:00:48,02 --> 00:00:50,66
sometimes it may look like this

17
00:00:52,24 --> 00:00:56,35
So the CPF estelle is a management utility for managing

18
00:00:56,35 --> 00:00:58,45
the CPF S for multi fromage images

19
00:00:58,94 --> 00:01:01,46
It is used in corporal build process to assemble the

20
00:01:01,46 --> 00:01:02,56
wrong image structure,

21
00:01:03,04 --> 00:01:05,58
but it can also be used minimally to print image

22
00:01:05,58 --> 00:01:08,76
structure and new fly or extract existing file

23
00:01:09,54 --> 00:01:11,57
It is built as a part of the Corvette build

24
00:01:11,57 --> 00:01:14,46
process and located and build cBF festival

25
00:01:15,24 --> 00:01:19,22
So let's look how how it looks like um after

26
00:01:19,22 --> 00:01:19,86
the build process

27
00:01:22,44 --> 00:01:24,94
So as we have seen earlier,

28
00:01:25,44 --> 00:01:30,22
the CBF has to print the carbons image contents at

29
00:01:30,23 --> 00:01:31,56
the end of the build process

30
00:01:31,94 --> 00:01:35,22
So we see a certain files name which are named

31
00:01:35,22 --> 00:01:38,04
in the most left call

32
00:01:38,53 --> 00:01:42,46
We have this stage record like rem stage round stage

33
00:01:42,84 --> 00:01:45,26
post car bailout and to pull block

34
00:01:46,14 --> 00:01:47,26
Besides these fights,

35
00:01:47,26 --> 00:01:50,8
we have also some necessary files like DS

36
00:01:50,8 --> 00:01:53,16
DT which is an empty table

37
00:01:53,54 --> 00:01:54,86
We have the symbols layout,

38
00:01:55,34 --> 00:02:00,46
binary father which contains runtime options who have some basic

39
00:02:00,47 --> 00:02:02,18
corporate folks like the conflict,

40
00:02:02,19 --> 00:02:03,61
minimal conflict,

41
00:02:03,62 --> 00:02:08,09
the revision of the corporate industry and the payload confident

42
00:02:08,09 --> 00:02:09,16
revision files

43
00:02:10,85 --> 00:02:13,69
The offset column explains the offset from the start of

44
00:02:13,7 --> 00:02:16,42
the image to the beginning of the file

45
00:02:18,04 --> 00:02:20,46
Each of the files can have different types

46
00:02:20,84 --> 00:02:23,61
We have various types like stages profiles

47
00:02:23,62 --> 00:02:24,8
She must layout,

48
00:02:24,81 --> 00:02:25,58
simple,

49
00:02:25,59 --> 00:02:29,36
executive and linking file boot block

50
00:02:30,04 --> 00:02:34,47
And there are also many other file types like SPD

51
00:02:34,48 --> 00:02:35,52
for the serial presence,

52
00:02:35,52 --> 00:02:37,5
detect on the uh,

53
00:02:37,51 --> 00:02:39,31
the memory modules

54
00:02:39,32 --> 00:02:40,48
And for example,

55
00:02:40,49 --> 00:02:43,72
also there is a separate file type for intel,

56
00:02:43,72 --> 00:02:44,18
fsp,

57
00:02:44,18 --> 00:02:45,01
binaries,

58
00:02:45,02 --> 00:02:45,76
etcetera

59
00:02:46,74 --> 00:02:50,18
Of course each file has it on the size which

60
00:02:50,18 --> 00:02:52,66
is presented in the science column

61
00:02:53,24 --> 00:02:57,08
In the last column indicates the compression applied to given

62
00:02:57,08 --> 00:02:57,45
file

63
00:02:57,88 --> 00:03:04,52
Currently there is nothing compressed although it the the compressed

64
00:03:04,52 --> 00:03:05,66
stages are for example,

65
00:03:05,66 --> 00:03:07,19
round stage and payload

66
00:03:07,36 --> 00:03:11,55
They are typically compressed but it is not indicated

67
00:03:23,14 --> 00:03:26,54
So now let's review the CPF estelle and it's a

68
00:03:26,55 --> 00:03:27,25
common line

69
00:03:27,84 --> 00:03:31,12
So the cp festo after the build process is located

70
00:03:31,12 --> 00:03:32,85
at the build cBf's tool

71
00:03:33,44 --> 00:03:35,77
It has quite really good common line help

72
00:03:35,78 --> 00:03:37,59
So all possible comments,

73
00:03:37,59 --> 00:03:40,46
parameters and types can be found directly there

74
00:03:41,13 --> 00:03:44,99
The same taxes simple simply pass the Rome image name

75
00:03:44,99 --> 00:03:46,38
as the first argument,

76
00:03:46,52 --> 00:03:50,53
then the command Parham and parameters at the end the

77
00:03:50,53 --> 00:03:50,81
dutch

78
00:03:50,81 --> 00:03:53,47
The flag can of course be used to enable some

79
00:03:53,47 --> 00:03:54,45
fair boss output

80
00:03:55,77 --> 00:03:59,93
The most commonly used comments are probably the print extract

81
00:04:00,14 --> 00:04:03,05
added at stage and at bailout

82
00:04:03,94 --> 00:04:06,96
So let's have a look how the hell looks like

83
00:04:09,24 --> 00:04:11,93
I would not just execute the CBO festival from the

84
00:04:11,94 --> 00:04:14,22
build sacrificed it actually age,

85
00:04:14,22 --> 00:04:22,16
flag command and so we have some usage examples with

86
00:04:22,17 --> 00:04:26,66
options explanatory and Yeah,

87
00:04:26,67 --> 00:04:31,72
the first comment is which simply as a given component

88
00:04:31,73 --> 00:04:36,21
Given regions the region can be an F flash map

89
00:04:36,21 --> 00:04:40,15
region indicates the file and name and T attack

90
00:04:40,94 --> 00:04:45,28
We can also optionally append hash apply compression,

91
00:04:45,45 --> 00:04:49,74
defines some base urges or alignment but yeah,

92
00:04:50,24 --> 00:04:51,8
these are mutual excluding,

93
00:04:51,8 --> 00:04:53,26
so you have to define only one

94
00:04:53,94 --> 00:04:56,56
We can also pass some padding size

95
00:04:56,94 --> 00:04:59,74
Um and for example,

96
00:04:59,74 --> 00:05:03,89
we can specify whether uh dysfunction to execute in place

97
00:05:03,9 --> 00:05:09,55
Uh in case we have some fsp or program stage

98
00:05:09,94 --> 00:05:12,48
and we can define,

99
00:05:12,48 --> 00:05:13,09
for example,

100
00:05:13,09 --> 00:05:16,26
some other inter specific options

101
00:05:18,64 --> 00:05:21,2
We have also a special common lines to add the

102
00:05:21,2 --> 00:05:23,46
payload stage or a flat binary

103
00:05:24,44 --> 00:05:30,38
These are almost the same but take different arguments because

104
00:05:30,39 --> 00:05:36,41
the different 5 5 times with different arguments,

105
00:05:36,42 --> 00:05:38,02
for example,

106
00:05:38,63 --> 00:05:41,1
has some specific arguments

107
00:05:41,1 --> 00:05:41,69
For example,

108
00:05:41,69 --> 00:05:44,05
we can add some common lines and around this,

109
00:05:44,94 --> 00:05:47,28
we can also apply compression,

110
00:05:47,28 --> 00:05:49,65
define a base address but not alignment

111
00:05:52,14 --> 00:05:53,15
For example,

112
00:05:53,16 --> 00:05:58,25
the stage also can have some compression

113
00:05:58,25 --> 00:05:59,19
The base address

114
00:05:59,74 --> 00:06:01,63
We can ignore certain sections,

115
00:06:01,64 --> 00:06:07,06
for example around stages at the disappearance without certain sections

116
00:06:07,54 --> 00:06:11,4
You can define the alignment and some page size also

117
00:06:11,41 --> 00:06:16,7
executive place etcetera at integer

118
00:06:17,03 --> 00:06:20,45
This is a simple comment that as a 64 bit

119
00:06:20,45 --> 00:06:22,46
integral to the cbf s

120
00:06:22,84 --> 00:06:27,04
It is often used to add some spice runtime configuration

121
00:06:27,05 --> 00:06:30,51
We just passed the integer after the dash

122
00:06:30,52 --> 00:06:35,66
I reflect and give the value

123
00:06:38,34 --> 00:06:42,33
There is also some add master header which is a

124
00:06:42,34 --> 00:06:49,36
legacy severe festivals or whether instance you can remove certain

125
00:06:49,74 --> 00:06:54,92
certain five strong given regions can compact the cbf s

126
00:06:54,92 --> 00:06:58,97
so if we have some holes after extracted or removed

127
00:06:58,98 --> 00:06:59,78
files,

128
00:06:59,79 --> 00:07:03,86
we can compact damage again to save some space

129
00:07:05,54 --> 00:07:08,65
We can create and duplicate some copy of severe fence

130
00:07:08,65 --> 00:07:10,55
instance in the flesh map

131
00:07:11,04 --> 00:07:14,51
We can create some ladies your own file with CPF

132
00:07:14,51 --> 00:07:15,54
S monster hydra

133
00:07:16,34 --> 00:07:19,85
You can create some new style also partitioned humor images

134
00:07:19,86 --> 00:07:21,26
with Flashman

135
00:07:22,44 --> 00:07:23,36
Uh,

136
00:07:23,74 --> 00:07:28,57
we can also locate a file in the city affairs

137
00:07:28,57 --> 00:07:31,45
or find a place for a file of given size

138
00:07:31,84 --> 00:07:35,25
We're gonna bring the layout of this of the wrong

139
00:07:35,94 --> 00:07:40,16
It will print us at the flush mount partitions and

140
00:07:40,17 --> 00:07:44,31
print commenters for printing the cBf s content

141
00:07:44,31 --> 00:07:47,56
So we have to define their freshman region

142
00:07:48,04 --> 00:07:51,46
Well with with containing the CPF s,

143
00:07:52,89 --> 00:07:59,07
we can extract certain files um and certain five times

144
00:07:59,07 --> 00:08:03,11
in cfs require also passing the architecture in order to

145
00:08:03,12 --> 00:08:05,26
correctly extract the fire

146
00:08:05,64 --> 00:08:06,63
For example,

147
00:08:06,74 --> 00:08:09,31
if we want to extract a payload or a stage

148
00:08:09,32 --> 00:08:12,15
we have to pass the architecture in order to correctly

149
00:08:12,94 --> 00:08:15,06
extract the executable

150
00:08:15,84 --> 00:08:19,12
It can also simply write and read a certain uh

151
00:08:19,12 --> 00:08:21,07
huh flash mob regions

152
00:08:21,08 --> 00:08:25,54
Uh we can truncate certain regions and expand them as

153
00:08:25,54 --> 00:08:25,96
well

154
00:08:27,34 --> 00:08:30,2
And at the end of the heart comment we have

155
00:08:30,2 --> 00:08:35,08
some explanations about about the values that have to be

156
00:08:35,08 --> 00:08:36,05
passed to certain flags,

157
00:08:36,05 --> 00:08:39,29
like offset architectures and types here,

158
00:08:39,29 --> 00:08:43,01
you can see the types of all cFS fights which

159
00:08:43,01 --> 00:08:46,06
are which are interpreted

160
00:08:49,64 --> 00:08:49,87
Okay,

161
00:08:49,87 --> 00:08:55,25
now let's uh let's exercise certain basic cbf estelle comments

162
00:08:55,85 --> 00:08:56,65
like Brent,

163
00:08:56,65 --> 00:08:57,31
extract,

164
00:08:57,32 --> 00:08:58,01
add files,

165
00:08:58,01 --> 00:08:59,46
remove files etcetera

166
00:09:02,54 --> 00:09:05,96
Now I will record everything for you so that's very

167
00:09:06,48 --> 00:09:08,18
if you're not keeping up,

168
00:09:08,18 --> 00:09:11,61
you will have a chance to replay everything

169
00:09:11,61 --> 00:09:17,08
Hand Oh exercise or so let's change to the build

170
00:09:17,08 --> 00:09:25,52
directory and execute the first most important most important comment

171
00:09:25,52 --> 00:09:26,65
which is print

172
00:09:27,54 --> 00:09:30,2
We passed the corporate room which is also a place

173
00:09:30,2 --> 00:09:33,96
in the build directory that execute breath

174
00:09:34,54 --> 00:09:38,69
As we can see it contains all our files has

175
00:09:38,69 --> 00:09:40,96
presented area after the build process

176
00:09:42,14 --> 00:09:42,34
Yeah

177
00:09:42,84 --> 00:09:43,61
And yes,

178
00:09:43,8 --> 00:09:47,36
let's try some basic comments like extract

179
00:09:49,24 --> 00:09:49,45
Mhm

180
00:09:52,34 --> 00:09:54,23
And maybe this extract,

181
00:09:54,24 --> 00:09:55,36
conflict file

182
00:09:56,94 --> 00:09:57,56
Yeah

183
00:10:02,14 --> 00:10:02,34
Yeah

184
00:10:02,84 --> 00:10:03,05
Mhm

185
00:10:03,84 --> 00:10:04,05
Mhm

186
00:10:05,74 --> 00:10:11,23
It has been extracted successfully and let's just dump the

187
00:10:11,24 --> 00:10:11,66
fire

188
00:10:12,14 --> 00:10:12,35
Mhm

189
00:10:13,94 --> 00:10:14,98
And as you can see,

190
00:10:15,18 --> 00:10:16,76
the only option that was selected

191
00:10:17,34 --> 00:10:21,15
Non default option was our main words actions

192
00:10:22,94 --> 00:10:23,14
Yeah

193
00:10:24,5 --> 00:10:28,16
And now let's try for example to remove certain

194
00:10:30,14 --> 00:10:30,35
Okay

195
00:10:30,84 --> 00:10:36,81
Mhm files like um let's remove the coffee actually

196
00:10:38,64 --> 00:10:40,65
Mhm Okay

197
00:10:41,04 --> 00:10:44,21
And now bring the country's again to ensure it has

198
00:10:44,21 --> 00:10:45,56
really been removed

199
00:10:46,3 --> 00:10:47,05
Yes,

200
00:10:47,44 --> 00:10:49,96
we see and empathy in space,

201
00:10:50,44 --> 00:10:52,75
in the place where the conflict I was

202
00:10:53,54 --> 00:10:56,16
You may not say that the size is different right

203
00:10:56,16 --> 00:11:01,93
now because each file contains a header that specifies the

204
00:11:01,93 --> 00:11:03,46
start and the end of the file

205
00:11:04,24 --> 00:11:09,04
So it is some overhead when we place a file

206
00:11:09,22 --> 00:11:11,8
but when the place is empty,

207
00:11:11,81 --> 00:11:12,36
the size,

208
00:11:12,37 --> 00:11:15,16
the size of emptiness is wider

209
00:11:15,94 --> 00:11:16,45
Mm hmm

210
00:11:17,34 --> 00:11:17,93
Okay

211
00:11:17,93 --> 00:11:21,55
And now we can add some far right,

212
00:11:22,84 --> 00:11:23,65
maybe

213
00:11:25,94 --> 00:11:26,26
Okay,

214
00:11:26,94 --> 00:11:29,76
let's write some custom strength

215
00:11:31,94 --> 00:11:34,08
Uh let's say hello Bert

216
00:11:35,47 --> 00:11:38,23
It depended to my fire

217
00:11:40,54 --> 00:11:40,75
Mhm

218
00:11:42,07 --> 00:11:43,37
And then lets you see

219
00:11:43,37 --> 00:11:47,16
Professed to corporate Ron to add the file

220
00:11:48,14 --> 00:11:51,37
So first we need to specify which file we want

221
00:11:51,37 --> 00:11:53,86
to add its name

222
00:11:54,44 --> 00:11:57,56
Let's say it will be my file

223
00:11:59,14 --> 00:12:03,66
Um And that's probably not enough

224
00:12:04,24 --> 00:12:04,77
Yes,

225
00:12:04,78 --> 00:12:09,39
because we have to type the type of the so

226
00:12:09,44 --> 00:12:13,05
it should be a row file and now everything is

227
00:12:13,05 --> 00:12:17,36
correct and slow and it's desperate

228
00:12:18,74 --> 00:12:21,95
And to have the mind file and the cbf s

229
00:12:23,94 --> 00:12:26,89
we can also apply compression to define

230
00:12:26,9 --> 00:12:28,75
So let's say that

231
00:12:29,34 --> 00:12:29,66
Mhm

232
00:12:30,44 --> 00:12:38,34
My father be compressed and the compression is indicated by

233
00:12:38,34 --> 00:12:39,74
flag darcy

234
00:12:40,09 --> 00:12:41,93
And we can use a D

235
00:12:41,93 --> 00:12:42,1
M

236
00:12:42,1 --> 00:12:42,86
A compression

237
00:12:45,84 --> 00:12:46,05
Mhm

238
00:12:46,94 --> 00:12:52,06
And it felt because The five sizes too small

239
00:12:53,04 --> 00:12:53,45
Yeah,

240
00:12:55,54 --> 00:12:58,66
we need to have a larger file

241
00:13:01,74 --> 00:13:02,25
Well,

242
00:13:03,34 --> 00:13:06,49
where my little club disappear fast of actually let's try

243
00:13:06,49 --> 00:13:06,96
this out

244
00:13:09,24 --> 00:13:09,95
Yeah

245
00:13:12,34 --> 00:13:12,55
Mhm

246
00:13:14,14 --> 00:13:19,51
So let's include the CPF stole inside Corbett

247
00:13:20,34 --> 00:13:20,66
Okay

248
00:13:21,04 --> 00:13:21,45
Yeah,

249
00:13:22,74 --> 00:13:23,66
yeah,

250
00:13:24,13 --> 00:13:24,96
it succeeded

251
00:13:25,84 --> 00:13:26,95
And let's print

252
00:13:29,84 --> 00:13:30,75
And yes,

253
00:13:30,76 --> 00:13:36,68
we see now that the CPF Festival compressed is compressed

254
00:13:36,69 --> 00:13:38,96
as indicated by the last column

255
00:13:39,34 --> 00:13:41,69
And it shows that the compressed size as well

256
00:13:42,24 --> 00:13:46,55
So a country after compression that is There is a

257
00:13:46,55 --> 00:13:48,76
bit more than 400 kg

258
00:13:50,14 --> 00:13:54,47
And the real uh find sources over one and the

259
00:13:54,47 --> 00:13:55,62
heart of mega bucks

260
00:13:55,63 --> 00:13:58,26
So let's see if it is really true

261
00:14:02,04 --> 00:14:02,41
Yes,

262
00:14:02,42 --> 00:14:03,49
it's actually true

263
00:14:03,69 --> 00:14:08,69
The find size is the same so it works

264
00:14:08,7 --> 00:14:14,2
Um So we already know how to remove and add

265
00:14:14,2 --> 00:14:14,9
new fires,

266
00:14:14,91 --> 00:14:15,99
extract them,

267
00:14:16,0 --> 00:14:17,27
print its contents

268
00:14:17,94 --> 00:14:21,57
You may also play with the C the festival uh

269
00:14:21,59 --> 00:14:28,97
more uh for your own exercise um Like uh I

270
00:14:28,97 --> 00:14:30,72
recommend at some integers,

271
00:14:30,73 --> 00:14:33,25
play with at stages at bay logs

272
00:14:33,84 --> 00:14:39,17
You can also try to uh bring to save the

273
00:14:39,17 --> 00:14:41,76
layoffs or locates on files etcetera

274
00:14:43,34 --> 00:14:44,06
Good luck

