1
00:00:00,54 --> 00:00:03,47
So we already know how the court would build system

2
00:00:03,47 --> 00:00:07,84
looks like and what is built during the global beef

3
00:00:07,84 --> 00:00:08,46
process

4
00:00:09,04 --> 00:00:10,0
However,

5
00:00:10,03 --> 00:00:13,96
we don't know yet what exactly is building our corporate

6
00:00:14,64 --> 00:00:18,65
So Corbyn toolchain is necessary to build to build the

7
00:00:18,65 --> 00:00:19,95
Corbett binary image

8
00:00:20,44 --> 00:00:25,51
These two chain consists of compilers been utilities package and

9
00:00:25,51 --> 00:00:28,66
curses death lips and a C P I compiler

10
00:00:29,04 --> 00:00:34,35
So basically the compiler is compiling capital cold and building

11
00:00:34,35 --> 00:00:40,76
materials contains the linker which links the whole compiled objects

12
00:00:40,77 --> 00:00:46,68
by gcc into a single executable leave and courses are

13
00:00:46,68 --> 00:00:53,69
mainly used to display the configuration many which is which

14
00:00:53,69 --> 00:00:55,31
is used to create the text

15
00:00:55,31 --> 00:00:58,46
You right and is also used in the payload

16
00:00:59,44 --> 00:01:04,31
The SCP I source code compiler is used to compile

17
00:01:04,32 --> 00:01:08,61
a C P I tables and code which is included

18
00:01:08,61 --> 00:01:10,06
in the corpus South Tree

19
00:01:11,34 --> 00:01:13,46
Why do we even need the toolchain?

20
00:01:14,33 --> 00:01:14,78
Well,

21
00:01:14,79 --> 00:01:17,26
building corporate image is one thing,

22
00:01:17,84 --> 00:01:23,26
but one should note that carbon supports multiple architectures so

23
00:01:23,27 --> 00:01:26,66
we need a different tune chain for each architecture

24
00:01:27,04 --> 00:01:30,75
So if you want to build for given target architecture

25
00:01:30,76 --> 00:01:32,95
cost compilation maybe often necessary

26
00:01:33,54 --> 00:01:37,11
We also like to avoid incompatibility issues due to some

27
00:01:37,11 --> 00:01:38,16
compiler set up

28
00:01:39,04 --> 00:01:41,99
Also compare it ships with many distributions may not be

29
00:01:41,99 --> 00:01:44,05
compatible to perform a corporate bill

30
00:01:44,44 --> 00:01:45,46
We all use the same,

31
00:01:45,46 --> 00:01:47,1
build an environment than some

32
00:01:47,1 --> 00:01:49,36
It should be easier to set up and resolve potential

33
00:01:49,36 --> 00:01:49,96
issues

34
00:01:52,04 --> 00:01:55,57
So how to build the core motel chain in order

35
00:01:55,57 --> 00:01:57,77
to build all available to chains,

36
00:01:57,78 --> 00:01:58,63
we can use them,

37
00:01:58,63 --> 00:02:02,06
make across gcc comment to build all in one shot

38
00:02:02,74 --> 00:02:05,26
You can also build a specific tool chain

39
00:02:05,84 --> 00:02:10,66
We have to just specify the target architecture uh and

40
00:02:10,66 --> 00:02:12,79
make crazy consistency

41
00:02:12,79 --> 00:02:13,96
Dash the ark

42
00:02:14,94 --> 00:02:18,26
So usually it is enough to build this one since

43
00:02:18,74 --> 00:02:22,63
We're mainly going to be building for example for X

44
00:02:22,63 --> 00:02:23,15
86

45
00:02:24,24 --> 00:02:27,78
So I would not recommend building and the corporate to

46
00:02:27,78 --> 00:02:30,98
change because it is very time consuming and this of

47
00:02:30,98 --> 00:02:31,95
an error prone

48
00:02:33,04 --> 00:02:38,27
We commend to use the docker container if you decide

49
00:02:38,28 --> 00:02:41,56
to build the future manually after the build process,

50
00:02:41,57 --> 00:02:44,94
it can be found at the audience cross gcc gcc

51
00:02:44,95 --> 00:02:47,06
slash bin directory

52
00:02:47,74 --> 00:02:50,45
You will see that some of the tools that are

53
00:02:50,45 --> 00:02:51,88
part of the church on the slide

54
00:02:51,88 --> 00:02:55,37
But in fact the list is much longer and send

55
00:02:55,37 --> 00:02:55,94
the doctor,

56
00:02:55,95 --> 00:02:59,37
you might find it in opt gcc being there will

57
00:02:59,37 --> 00:03:01,95
be lots of files for various architectures

58
00:03:03,04 --> 00:03:06,37
So let's check out how does it look like I

59
00:03:06,42 --> 00:03:08,56
am a sighting of the doctor already

60
00:03:09,04 --> 00:03:11,95
So let's check how it looks like

61
00:03:17,25 --> 00:03:21,1
As you can see there is a toolchain for 64

62
00:03:21,1 --> 00:03:23,16
bit architecture on X 86

63
00:03:23,54 --> 00:03:31,32
Have 64 bit risk five 64 bits or appreciate and

64
00:03:31,32 --> 00:03:35,7
yes we are And 32 bit x 86 architecture

65
00:03:37,14 --> 00:03:37,34
Yeah

66
00:03:37,84 --> 00:03:46,54
Also if you're lying are That are 64 bit So

67
00:03:46,55 --> 00:03:48,15
as I have mentioned here,

68
00:03:48,16 --> 00:03:51,35
the cross compilation of Angkor boutique chain is often necessary

69
00:03:52,14 --> 00:03:56,25
but how to tell corporate where our surgeon is and

70
00:03:56,25 --> 00:03:59,36
how to use it an important part

71
00:03:59,94 --> 00:04:03,9
This process takes the Ex compile file which lies in

72
00:04:03,9 --> 00:04:05,36
the corporate directory

73
00:04:05,91 --> 00:04:09,1
It contains paths to touch and boundaries as well as

74
00:04:09,1 --> 00:04:10,1
compilation Flags,

75
00:04:10,1 --> 00:04:11,99
which are required in the build process

76
00:04:12,54 --> 00:04:15,33
It is included in the make fun to set variables

77
00:04:15,33 --> 00:04:16,43
like C,

78
00:04:16,43 --> 00:04:16,85
c,

79
00:04:16,86 --> 00:04:17,1
c,

80
00:04:17,1 --> 00:04:17,76
Flags,

81
00:04:17,77 --> 00:04:18,53
CPP,

82
00:04:18,54 --> 00:04:19,05
object,

83
00:04:19,05 --> 00:04:19,55
copy,

84
00:04:19,56 --> 00:04:20,02
object,

85
00:04:20,03 --> 00:04:20,41
dump,

86
00:04:20,42 --> 00:04:21,15
et cetera

87
00:04:21,94 --> 00:04:23,76
Let's see how it looks in the code

88
00:04:28,34 --> 00:04:30,34
So here we have the target,

89
00:04:30,34 --> 00:04:34,72
which is the dot ex compile file which is built

90
00:04:34,73 --> 00:04:38,4
with you to ex compile Ex compile as we can

91
00:04:38,4 --> 00:04:38,84
see it

92
00:04:38,85 --> 00:04:41,15
Like that excuses ship off,

93
00:04:41,84 --> 00:04:44,15
but how the existence is set

94
00:04:47,34 --> 00:04:51,47
It is used the EX compile file and with the

95
00:04:51,47 --> 00:05:00,76
EX compile activity to produce it and another researches um

96
00:05:01,14 --> 00:05:01,46
mhm

97
00:05:02,84 --> 00:05:05,13
In the executing X compatible,

98
00:05:06,04 --> 00:05:11,76
it takes the default paths for the cross toolchain and

99
00:05:11,76 --> 00:05:13,42
it said the environment variable,

100
00:05:13,55 --> 00:05:15,86
which is then consumed by the make file

