1
00:00:01,280 --> 00:00:05,520
now we are quite well prepared to start

2
00:00:03,439 --> 00:00:07,279
the build process

3
00:00:05,520 --> 00:00:09,360
simply type make in the corbett's

4
00:00:07,279 --> 00:00:11,759
redirectory and observe how

5
00:00:09,360 --> 00:00:14,480
the files are compiled your surely

6
00:00:11,759 --> 00:00:16,160
notice the pattern of the compiled files

7
00:00:14,480 --> 00:00:18,240
there are many files from generic

8
00:00:16,160 --> 00:00:20,240
kerberos directions like lib

9
00:00:18,240 --> 00:00:22,640
and board specific directories like main

10
00:00:20,240 --> 00:00:25,920
board emulation qmu q35

11
00:00:22,640 --> 00:00:28,480
and sawbridge intel and the model

12
00:00:25,920 --> 00:00:30,480
you will also probably notice that each

13
00:00:28,480 --> 00:00:32,079
line starts with the name of the stage

14
00:00:30,480 --> 00:00:35,280
it is compiled for

15
00:00:32,079 --> 00:00:36,239
in case of the below output it is boot

16
00:00:35,280 --> 00:00:38,239
block

17
00:00:36,239 --> 00:00:39,760
later the message will contain rom stage

18
00:00:38,239 --> 00:00:42,079
postcard round stage

19
00:00:39,760 --> 00:00:45,360
as well as messages about cloning and

20
00:00:42,079 --> 00:00:48,320
building c bios payload

21
00:00:45,360 --> 00:00:50,879
so let's go through the process since

22
00:00:48,320 --> 00:00:50,879
the beginning

23
00:00:52,239 --> 00:00:59,840
i will configure once again the

24
00:00:56,239 --> 00:00:59,840
main board

25
00:01:00,879 --> 00:01:07,040
choose the q35

26
00:01:04,720 --> 00:01:09,360
save the configuration file and type

27
00:01:07,040 --> 00:01:09,360
make

28
00:01:09,680 --> 00:01:16,400
it start with some submittals and other

29
00:01:12,320 --> 00:01:16,400
utilities and then proceed with bootlock

30
00:01:16,640 --> 00:01:19,759
in the rom stage

31
00:01:22,640 --> 00:01:30,479
and drum stage

32
00:01:26,640 --> 00:01:34,799
as you can see many lib entries are here

33
00:01:30,479 --> 00:01:38,079
drivers device and now the smart module

34
00:01:34,799 --> 00:01:40,000
and all the ram stage sources it's not

35
00:01:38,079 --> 00:01:42,159
in the right order because

36
00:01:40,000 --> 00:01:44,079
before the round stage begins the

37
00:01:42,159 --> 00:01:46,159
postcard is executed so

38
00:01:44,079 --> 00:01:47,280
the postcard stage is compiled after

39
00:01:46,159 --> 00:01:49,280
amsterdam right now

40
00:01:47,280 --> 00:01:55,840
and then we have cloning of the c bios

41
00:01:49,280 --> 00:01:55,840
payload and it has been compiled

42
00:02:00,719 --> 00:02:09,200
and now after all the parts are ready

43
00:02:03,439 --> 00:02:11,200
the cbs festival is compiled

44
00:02:09,200 --> 00:02:13,840
this compiled with the host tool chain

45
00:02:11,200 --> 00:02:16,000
which is indicated by host cc

46
00:02:13,840 --> 00:02:17,920
and then we have some vibrate utilities

47
00:02:16,000 --> 00:02:22,560
and now whole image is

48
00:02:17,920 --> 00:02:25,599
packed and as indicated by the cpfs

49
00:02:22,560 --> 00:02:26,720
i will explain the cbfs and the soon

50
00:02:25,599 --> 00:02:31,599
lessons

51
00:02:26,720 --> 00:02:35,599
so now the build for the qmuk35

52
00:02:31,599 --> 00:02:36,080
is ready so at the end of the build

53
00:02:35,599 --> 00:02:38,800
process

54
00:02:36,080 --> 00:02:40,080
the build system prints the contents of

55
00:02:38,800 --> 00:02:43,120
the cardboard image

56
00:02:40,080 --> 00:02:44,400
the cbfs contents here you may see that

57
00:02:43,120 --> 00:02:47,120
the stages have been

58
00:02:44,400 --> 00:02:48,879
put inside the image depending on the

59
00:02:47,120 --> 00:02:51,120
configuration and payload

60
00:02:48,879 --> 00:02:52,959
there may be other files like vga option

61
00:02:51,120 --> 00:02:56,239
roms secondary payloads

62
00:02:52,959 --> 00:02:59,360
or configuration files like coreboot for

63
00:02:56,239 --> 00:03:02,159
boot order for uc bias but block will

64
00:02:59,360 --> 00:03:03,440
always be at the bottom of cbfs for x86

65
00:03:02,159 --> 00:03:05,840
architecture

66
00:03:03,440 --> 00:03:06,640
because of the risk vector being on the

67
00:03:05,840 --> 00:03:09,440
top of

68
00:03:06,640 --> 00:03:10,319
the 4 gigabytes of physical memory the

69
00:03:09,440 --> 00:03:12,640
boot firmware

70
00:03:10,319 --> 00:03:14,319
is always mapped at the top of 4

71
00:03:12,640 --> 00:03:18,000
gigabytes memory

72
00:03:14,319 --> 00:03:21,920
minus the spi flash size

73
00:03:18,000 --> 00:03:25,200
but what really is gpfs we can see some

74
00:03:21,920 --> 00:03:28,319
file names offsets types sizes

75
00:03:25,200 --> 00:03:33,840
and compression maybe compression

76
00:03:28,319 --> 00:03:33,840
i will explain that in the next lesson

