1
00:00:00,560 --> 00:00:06,399
let's move on to the k-config overview

2
00:00:03,679 --> 00:00:08,559
k-config is a configuration tool that

3
00:00:06,399 --> 00:00:09,599
was developed with linux kernel project

4
00:00:08,559 --> 00:00:12,000
initially

5
00:00:09,599 --> 00:00:14,080
it is a con configuration database used

6
00:00:12,000 --> 00:00:18,560
to select build time configuration

7
00:00:14,080 --> 00:00:20,640
for linux kernel well it was until now

8
00:00:18,560 --> 00:00:23,680
kconfig can be perceived as a collection

9
00:00:20,640 --> 00:00:25,519
of options organized in a tree structure

10
00:00:23,680 --> 00:00:26,880
it has been adopted by some of the

11
00:00:25,519 --> 00:00:29,039
operate source projects

12
00:00:26,880 --> 00:00:30,240
to help with build configuration

13
00:00:29,039 --> 00:00:32,800
maintenance

14
00:00:30,240 --> 00:00:35,520
one of this open source project is

15
00:00:32,800 --> 00:00:37,760
actually core boot

16
00:00:35,520 --> 00:00:39,760
as i have said in the previous lesson

17
00:00:37,760 --> 00:00:40,960
cardboard is one of the projects that

18
00:00:39,760 --> 00:00:43,760
takes advantage of the

19
00:00:40,960 --> 00:00:45,840
config utility its source code is

20
00:00:43,760 --> 00:00:48,960
located at cardboard slash utical

21
00:00:45,840 --> 00:00:49,920
slash kconfig in record but it is built

22
00:00:48,960 --> 00:00:53,440
automatically

23
00:00:49,920 --> 00:00:54,399
by corboot build process the text user

24
00:00:53,440 --> 00:00:57,039
interface

25
00:00:54,399 --> 00:00:58,399
can be accessed by make menuconfig

26
00:00:57,039 --> 00:01:00,480
command

27
00:00:58,399 --> 00:01:02,559
this interface is created based on the

28
00:01:00,480 --> 00:01:04,239
kconfig database

29
00:01:02,559 --> 00:01:05,600
here we can enable or disable

30
00:01:04,239 --> 00:01:08,080
configuration options

31
00:01:05,600 --> 00:01:11,280
assign values to configuration options

32
00:01:08,080 --> 00:01:13,680
or select a string value from a list

33
00:01:11,280 --> 00:01:15,600
in practice those options are used to

34
00:01:13,680 --> 00:01:18,560
select appropriate memory board

35
00:01:15,600 --> 00:01:19,680
chipset ram and drone size enable some

36
00:01:18,560 --> 00:01:22,320
debugging options

37
00:01:19,680 --> 00:01:24,320
etc there are also options for

38
00:01:22,320 --> 00:01:27,520
determining which payloads and binaries

39
00:01:24,320 --> 00:01:30,640
to include in final rom image

40
00:01:27,520 --> 00:01:33,040
so let's see how example k config files

41
00:01:30,640 --> 00:01:33,040
look like

42
00:01:35,119 --> 00:01:38,799
the first main config file resides in

43
00:01:37,759 --> 00:01:41,840
the

44
00:01:38,799 --> 00:01:41,840
source directory

45
00:01:44,240 --> 00:01:48,640
and for example we can see that it

46
00:01:46,799 --> 00:01:51,600
initiates the main menu

47
00:01:48,640 --> 00:01:53,040
which is called core boot configuration

48
00:01:51,600 --> 00:01:55,200
and the main menu can contain

49
00:01:53,040 --> 00:01:57,360
sub menus which are defined with the

50
00:01:55,200 --> 00:02:00,079
comment menu

51
00:01:57,360 --> 00:02:01,600
single configuration options starts with

52
00:02:00,079 --> 00:02:05,119
config

53
00:02:01,600 --> 00:02:08,399
keyword we can have various

54
00:02:05,119 --> 00:02:11,760
options like booleans strings

55
00:02:08,399 --> 00:02:13,360
there are also hexadecimal options like

56
00:02:11,760 --> 00:02:18,400
integer options

57
00:02:13,360 --> 00:02:21,520
you can have for example certain choices

58
00:02:18,400 --> 00:02:25,200
etc etc also

59
00:02:21,520 --> 00:02:28,720
kconfig allows to source other

60
00:02:25,200 --> 00:02:30,800
files like kconfig files to extend the

61
00:02:28,720 --> 00:02:33,360
mini

62
00:02:30,800 --> 00:02:34,239
here we have an example of the main

63
00:02:33,360 --> 00:02:37,680
vermini

64
00:02:34,239 --> 00:02:38,480
which is extended with the k config file

65
00:02:37,680 --> 00:02:41,440
from source

66
00:02:38,480 --> 00:02:43,120
slash main board slash k config in

67
00:02:41,440 --> 00:02:45,040
question of make con

68
00:02:43,120 --> 00:02:48,400
inclusion of another k config file is

69
00:02:45,040 --> 00:02:48,400
done by source keyword

70
00:02:48,879 --> 00:02:55,440
and that's how it looks like in the core

71
00:02:52,440 --> 00:02:55,440
boot

