# Introduction

### .center[What does one wish to happen when powering up a computer?]

Typically one wants to load an operating system and get their work done on the
machine.

But the operating system cannot be launched "just like that" without any
preparation of the hardware and passing information to the system. Same as in
the Lord of the Rings:

`One does not simply walk into Mordor`

???

That is why we need a piece of persistent software which is executed after
pressing the power button and prepares the hardware to launch an operating
system. This piece of software is called firmware.

---

# Introduction

In this course you will:

* be introduced to the tasks performed by firmware on the example of
  coreboot
* learn the coreboot's boot process in detail
* get to know the coreboot's components, also called stages and their purpose

---

# coreboot

coreboot stages:

* bootblock
* verstage (optional)
* romstage
* postcar
* ramstage
* payload

???

Each of the stages has its purpose and one should not perform tasks of one
stage in another stage. It is enforced by coreboot booting model and to keep
behavioral consistency across various platforms.

---

# coreboot

Each of the stages is a separate executable file which is packed into a CBFS
(coreboot file system).

CBFS allows to pack the firmware components to create a sort of archive.
Features of CBFS:

- can be manipulated with cbfstool (https://github.com/coreboot/coreboot/tree/master/util/cbfstool)
- can integrate various file types (bootsplash images, raw files, 64 bit
  integers, etc.)
- handles compression to the files
- can specify the base address of the file in the target machine memory when it
  is mapped under 4 GB
- can specify alignment of the file in the CBFS 


???

There is a tool that allows manipulating the CBFS contents, called cbfstool.
It puts and removes files from CBFS, applies compression, enforces alignments
and base addresses of the files. It is also capable of manipulating the
flashmap partition contents.

---

# Reset Vector

* the first instruction executed by processor
* like a beginning of the world, universe and everything (but on the silicon
  level)
* located at memory address 0x00000000 for ARM and 0xfffffff0 on x86


???

ARM: The primary core loads the program counter and starts executing from
address 0xSP (Primary core stack pointer , inside ROM at 0xSP location it’s
stack pointer will be loaded) which instructs the core to load its reset
handler (stack pointer, vector table) and read processor Start Address(PSA) to
get application boot address and jump to that location.

x86: This is what it looked like for most of the lifetime of x86 architecture.
Nowadays silicon vendors tend to break this architectural rule by changing the
reset vector address or execute the code before CPUs are released from reset.
Few examples:
- Intel: ACM, ucode update, ME bring up, custom reset vector in Boot Guard
- AMD: PSP bringup and memory training, copying firmware to DRAM before CPUs
  are released from reset, also custom entry point for the first instructions

---

# bootblock

* Overview:

  - quite a lot of assembly code at the beginning
  - execute in place from ROM (RAM not ready yet)

* Main task of bootblock:

  - switch CPU to protected mode (32 bit mode)
  - very early silicon setup (enabling SPI prefetching, IO ports decoding for
    debug interfaces)
  - cache-as-RAM (CAR) setup for heap and stack (enough to run C code)

???

bootblock is the first stage of coreboot. This is where the processor jumps
after reset vector. Bootblock enables 32 bit protected mode, performs basic
initialization of the silicon to enable debug interfaces, sets up the temporary
memory (heap and stack) in the cache, called cache-as-ram (CAR). Why CAR? In
the very first days of BIOS and firmware, it was mainly written in assembly.
This assembly code was responsible for early silicon setup and memory training.
After the memory was ready, only then the higher level programming language
could be used to write code. Over time, with the release of newer
specifications of DDR memory, the memory initialization has become so complex,
that it was too difficult to write purely in assembly. That is why CAR mode has
been introduced which lets firmware developers use higher level programming
language as C in the early boot stages when RAM is not yet ready.

---

# verstage

This is an optional stage executed after the bootblock. It is added to the
coreboot when vboot (verified boot) is enabled. verstage tasks:

* initialize the trusted device, like Trusted Platform Module (TPM),
  coreboot support both TPM 1.2 and TPM 2.0
* initialize the cryptographical library
* locate the read-write firmware partition
* cryptographically verify the selected firmware partition
* execute next stage from the verified partition
* if verification fails, try other partition (read-write or recovery)

???

vboot is a Google's reference implementation of verified boot. vboot is
responsible for cryptographical verification of the firmware that is going to
be executed. It initiates the static root of trust for measurement (SRTM) in
order to boot the firmware securely and prevent unauthorized code from
execution.

TPM device compliant with Trusted Computing Group (TCG) specification is a
secure storage device capable of storing the firmware components measurements
(hashes), securely create and store keys and secrets.

---

# verstage

.center[<img src="images/vboot_diagram.png" width="900px" style="margin-left:-85px"/>]

vboot enforces a specific layout via the flashmap. Flashmap defines partitions
for recovery (read-only) and updatable firmware (read-write). Both contain CBFS
with copies of the stages required to boot.

???

There are various combinations of the layout based on desired protection level:

- RO
- RO+RW
- RO+2RW

Each of the partitions contains a CBFS which contains the copy of the stages
which are executed after verstage. Additionally RW partitions have so called
VBLOCK which stores the public key and signature of the CBFS in the
corresponding partition. vboot analyzes the VBLOCK and verifies the CBFS and
then jumps to the next stage.

---

# verstage

Example flashmap layout descrition file:

```
FLASH@0xff400000 0xc00000 {
	SI_ALL 0x500000 {
		SI_DESC 0x1000
		SI_GBE 0x2000
		SI_ME
	}
	SI_BIOS 0x700000 {
		UNIFIED_MRC_CACHE 0x20000 {
			RECOVERY_MRC_CACHE 0x10000
			RW_MRC_CACHE 0x10000
		}

		WP_RO {
			FMAP 0x800
			RO_FRID 0x40
			RO_PADDING 0x7c0
			GBB 0x1e000
			COREBOOT(CBFS)
		}
	}
}
```

---

# romstage

* Overview:
  - Cache-As-RAM (CAR)
  - mainly uses C code
* Main task of romstage:
  - early devices initialization necessary for RAM training
  - RAM initialization
  - cbmem initialization
  - prepare a handoff and jump to postcar
* 4 variants of execution
  - romstage with Intel FSP
  - romstage with AMD AGESA code
  - romstage with AMD AGESA blob
  - romstage with native silicon support

???

romstage is the next stage executed after bootblock (in case of no vboot) or
verstage. There is no memory yest so it executes from CAR. Mainly written in C
code, except the entry point. The purpose of romstage is simple:

1. Initialize the hardware necessary for RAM operation
2. Train the main memory
3. Initialize cbmem and handoff to postcar stage

Few years back the memory training was open and not so complicated. At that
time platforms used coreboot's native implementation for memory initialization.
However over time the silicon vendor began to close their silicon
initialization inside binary blobs in fear of leaking intellectual property
etc. These blobs are Intel Firmware Support Package and AMD Generic
Encapsulated Software Architecture. At the beginning AMD has been releasing the
AGESA source code to coreboot, but later it began to follow Intel's trend and
started to deliver it in binary form. Still there are some microarchitectures
that have fully open code, like Intel Ivybridge (3rd Gen) and older, AMD family
16h Kabini, 15h Trinity or 14h. Nowadays x86 architecture is full of secrets
and likely will never ever be open again.

---

# postcar

This stage has very few responsibilities and thus takes a very short amount of
time during boot process.

* Overview:
  - Cache-As-RAM (CAR)
  - mainly uses C code
* Main task of postcar:
  - tear the Cache-As-Ram environment down
  - switch to RAM
  - load ramstage

???

postcar stage has been designed to create a clear program boundary between the
romstage and ramstage. Before postcar has been introduced there were so called
CAR globals, which were variables that had to be stored in a special section of
romstage and copied over when CAR has been teared down. It was quite a burden
for developers to maintain that solution, so CAR teardown has been moved to
separate stage - postcar.

---

# ramstage

The ramstage does the main hardware initialization:

* PCI devices
* On-chip devices
* TPM (if not done by verstage)
* Graphics (optional)
* CPU (like set up System Management Mode)
* Super I/O, Embedded Controller

???

Ramstage is the first stage that runs in the main memory. It is responsible for
initialization of CPU cores and other devices like PCI/PCI Express devices,
graphics, TPM, Super I/O and Embedded Controllers. But that's not all.

---

# ramstage

Ramstage does not perform the initialization in a random order. The process is
controlled in an appropriate manner via boot state machine. Boot state machine
substages:

* BS_PRE_DEVICE - before any device tree actions take place
* BS_DEV_INIT_CHIPS - initialize all chips in device tree
* BS_DEV_ENUMERATE - device tree probing
* BS_DEV_RESOURCES - device tree resource allocation and assignment
* BS_DEV_ENABLE - device tree enabling/disabling of devices
* BS_DEV_INIT - device tree device initialization
* BS_POST_DEVICE - all device tree actions performed
* BS_OS_RESUME_CHECK - check for OS resume
* BS_OS_RESUME - resume to OS
* BS_WRITE_TABLES - write coreboot tables
* BS_PAYLOAD_LOAD - Load payload into memory
* BS_PAYLOAD_BOOT - Boot to payload

???

Each of the substages can have particular action to be performed on the entry
to the substage and on the exit. it is tightly coupled with coreboot's device
tree (do not mistake with ARM's device tree for kernel booting), a tree
structure defined for each platform that describes the hardware devices present
on the board and their configuration, address, location etc. First, there are
actions performed before PCI devices initialization (PRE_DEVICE). For example
PCI devices may not be yet ready and they need some special treatment before
standard enumeration procedure, etc. At this point for example Intel FSP or AMD
AGESA is called to prepare the hardware. DEV_INIT_CHIPS initializes various
chips in coreboot's device tree, for example CPUs. DEV_ENUMERATE is responsible
for standard PCI enumeration. DEV_RESOURCES assigns resources to the devices.
PCI devices may require certain memory to map the registers and IO ports.
DEV_ENABLE is responsible for disabling or enabling the devices as defined in
the coreboot's device tree. DEV_INTI is responsible for performing device
specific initialization that is not always common for all devices. POST_DEVICE
is a substage that happens after devices are enumerated and initialized. It
gives some room for customization and adjustments for mainboard specific
settings for devices. OS_RESUME_CHECK OS_RESUME substages check whether the
machine has been put to sleep with suspend to RAM and eventually resumes
directly to the operating system. WRITE_TABLES is responsible for preparing
various structures and tables in RAM for operating system consumption. It is
done after OS resume check, because it is not necessary to do it again.
PAYLOAD_LOAD and PAYLOAD_BOOT prepares the payload (final application, not part
of coreboot) to be launched and executed. At this point ramstage end and
control is handed over to the payload.

---

# ramstage

Initialization of tables:

* ACPI tables
* SMBIOS tables (x86 specific)
* coreboot tables
* devicetree updates (ARM specific)

???

As mentioned in the introduction, the operating system requires certain
information about the hardware it runs on, etc. These information are passed
via standardized structures.

- Advanced Configuration and Power Interface (ACPI) industry-standard was
  developed to enable OS-directed configuration, power and thermal management,
  and RAS (Reliability, Availability and Serviceability) features of mobile,
  desktop, and server platforms. The ACPI specification describes the
  structures and mechanisms necessary to design operating system-directed power
  management and make advanced configuration architectures possible. ACPI
  applies to all classes of computers and is the key element in Operating
  System-directed configuration and Power Management (OSPM). ACPI tables may
  contain informational content or executable code written in ACPI specific
  language.
- System Management BIOS (SMBIOS) is the premier standard for delivering
  management information via system firmware. Since its release in 1995, the
  widely implemented SMBIOS standard has simplified the management of more than
  two billion client and server systems. It exposes information about the
  hardware (details of CPUs, used memory, external devices, etc.)
- coreboot tables are coreboot's specific structures placed in reserved memory,
  they hold various information, like mainboard vendor,model, coreboot version,
  build date, the console, boot performance timestamps etc. They can be viewed
  and parsed with another coreboot tool called cbmem.
- devicetrees are ARM specific and are required to boot Linux kernel

---

# ramstage

It also does hardware and firmware lockdown:

* write-protection of boot media
* lock security related registers
* lock System Management Mode (x86 specific)

???

It is necessary to lock certain registers and access to memory. Not doing will
have a huge security impact on the software that runs on the machine. For
example SMM contains firmware code that handles various action related to sleep
and power transitions, thus must be protected from operating system.

---

# Payload

Some of possible payloads:

* SeaBIOS
* TianoCore
* GRUB2
* Linux
* LinuxBoot
* depthcharge
* U-boot
* FILO
* custom payloads (based on libpayload for example)

Secondary payloads:

* memtest86plus
* iPXE
* coreinfo
* nvramcui

???

Payloads is an external to coreboot's software which responsibility is to load
the target software on the machine, typically operating system. coreboot offers
a wide range of payloads to be launched after ramstage. We will briefly explain
what each of them can do:

- SeaBIOS - provides the old-fashioned, legacy BIOS services which were used
  back in 90s to launch operating systems, like MS-DOS
- TianoCore - provides UEFI compatible interface to launch an operating system
- GRUB2 - popular bootloader in many Linux distributions, can load Linux and
  Windows, as well as other executables
- Linux - Linux kernel and initial ram disk can be integrated as a coreboot
  payload and directly launched after ramstage
- LinuxBoot - Linux kernel with u-root userland (initial ramdisk written in Go
  language)
- depthcharge - Google Chromebook's specific payload designed to launch ChromeOS
- U-Boot - well known bootloader in embedded ecosystem, more adequate for ARM
  rather than x86 architecture
- FILO - a bootloader which loads boot images from a local filesystem, without
  help from legacy BIOS services. Written with libpayload
- custom payloads - coreboot offers also launching custom payloads and it
  offers a library for writing own payloads called libpayload

Secondary payloads are payloads that can be run by the main payload. The idea
of secondary payloads become available with the integration of CBFS executable
images into SeaBIOS. SeaBIOS can load another payload from CBFS and execute it.
Some secondary payloads available in coreboot: 

- memtest86plus - RAM testing application, also available in Ubuntu
  distribution
- iPXE - open source network boot environment, Preboot eXecution Environment
- coreinfo - simple GUI application to parse coreboot tables and display
  information about hardware and coreboot console/timestamps
- nvramcui - simple GUI application for CMOS contents manipulation, integrates
  with option table implemented in coreboot that store runtime configuration in
  CMOS non-volatile memory

---

# Quiz

???

* **How many necessary boot phases coreboot covers?**

 5: bootblock, romstage, postcar, ramstage and payload

* **Why do we use Cache as RAM?**

 It's faster to set up then to initialize RAM and we want to have some kind of
 memory as fast as possible to execute C code.

* **Which stage is responsible for RAM training?**

 romstage

* **Name at least 4 possible payloads or secondary payloads**

 SeaBIOS, TianoCore, GRUB2, LinuxBoot
